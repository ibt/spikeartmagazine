core: 7.x
api: 2
projects:
  drupal:
    version: '7.60'
  spark:
    type: module
    custom_download: true
    version: 1.0-beta1
  views_bulk_operations:
    subdir: contrib
    version: '3.5'
  addressfield:
    subdir: contrib
    version: '1.2'
  admin_menu:
    subdir: contrib
    version: 3.0-rc5
  admin_settings:
    type: module
    custom_download: true
    subdir: custom
    version: '1.1'
  admin_views:
    subdir: contrib
    version: '1.6'
  animgif_support:
    subdir: contrib
    version: '1.6'
  auto_nodetitle:
    subdir: contrib
    version: '1.0'
  backup_migrate:
    subdir: contrib
    version: '3.5'
  service_links:
    subdir: contrib
    version: '2.3'
  better_formats:
    subdir: contrib
    version: 1.0-beta2
  ctools:
    version: '1.14'
  cer:
    subdir: contrib
    version: 3.0-beta1
  ckeditor_insert:
    subdir: contrib
    version: '1.1'
  ckeditor_link:
    subdir: contrib
    version: '2.4'
  colorbox:
    subdir: contrib
    version: '2.13'
  commerce:
    subdir: commerce
    version: '1.13'
  commerce_addressbook:
    subdir: commerce
    version: 2.0-rc9
  commerce_autosku:
    subdir: commerce
    version: '1.2'
  commerce_bank_transfer:
    subdir: commerce
    version: 1.0-alpha3
  commerce_billy:
    subdir: commerce
    version: '1.2'
  commerce_billy_mail:
    subdir: commerce
    version: 1.0-beta7
  commerce_discount:
    subdir: commerce
    version: 1.0-beta5
  commerce_discount_product_category:
    subdir: commerce
    version: '1.2'
  commerce_extra:
    subdir: commerce
    version: 1.0-alpha1
  commerce_extra_panes:
    subdir: commerce
    version: '1.1'
  commerce_features:
    subdir: commerce
    version: '1.3'
  commerce_paypal:
    subdir: commerce
    version: '2.6'
  commerce_product_attributes:
    subdir: commerce
    version: 1.0-beta3
  commerce_remove_tax:
    subdir: commerce
    version: '1.0'
  commerce_stock:
    subdir: commerce
    version: '2.3'
  commerce_shipping:
    subdir: commerce
    version: '2.3'
  conditional_fields:
    subdir: contrib
    version: 3.0-alpha2
  context:
    subdir: contrib
    version: '3.7'
  context_condition_theme:
    subdir: contrib
    version: '1.0'
  custom_shipping:
    type: module
    custom_download: true
    subdir: custom/custom_commerce/spike_shop
    version: '1.0'
  date:
    subdir: contrib
    version: '2.10'
  date_popup_authored:
    subdir: contrib
    version: '1.2'
  devel:
    subdir: contrib
    version: '1.6'
  disable_messages:
    subdir: contrib
    version: '1.2'
  ds:
    subdir: contrib
    version: '2.16'
  elements:
    subdir: contrib
    version: '1.5'
  emogrifier:
    subdir: contrib
    version: 2.0-beta1
  entity:
    subdir: contrib
    version: '1.9'
  entity_translation:
    subdir: contrib
    version: '1.0'
  entityreference:
    subdir: contrib
    version: '1.5'
  features_extra:
    subdir: contrib
    version: '1.0'
  features:
    subdir: contrib
    version: '2.10'
  features_language:
    subdir: contrib
    version: '1.3'
  feeds:
    subdir: contrib
    version: 2.0-beta4
  feeds_jsonpath_parser:
    subdir: contrib
    version: '1.0'
  feeds_tamper:
    subdir: contrib
    version: '1.2'
  field_collection:
    subdir: contrib
    version: 1.0-beta12
  field_formatter_settings:
    subdir: contrib
    version: '1.1'
  field_group:
    subdir: contrib
    version: '1.6'
  field_multiple_limit:
    subdir: contrib
    version: 1.0-alpha5
  file_entity:
    subdir: contrib
    version: '2.22'
  file_force:
    subdir: contrib
    version: '1.2'
  filefield_sources:
    subdir: contrib
    version: '1.11'
  filter_perms:
    subdir: contrib
    version: '1.0'
  flag:
    subdir: contrib
    version: '3.9'
  float_filter:
    subdir: contrib
    version: '1.2'
  fontawesome:
    subdir: contrib
    version: '2.9'
  ftools:
    subdir: contrib
    version: '1.6'
  globalredirect:
    subdir: contrib
    version: '1.6'
  google_analytics:
    version: '2.5'
  gridbuilder:
    version: 1.0-alpha2
  hide_formats:
    subdir: contrib
    version: '1.3'
  hsts:
    subdir: contrib
    version: '1.2'
  html_title:
    subdir: contrib
    version: '1.5'
  html_title_trash:
    subdir: contrib
    version: '1.1'
  htmlmail:
    subdir: contrib
    version: '2.71'
  i18n:
    subdir: contrib
    version: '1.26'
  i18n_commerce_product:
    subdir: commerce
    version: '1.x-dev'
  panels:
    subdir: patched
    version: '3.9'
  i18nviews:
    subdir: contrib
    version: 3.0-alpha1
  plugins:
    type: module
    custom_download: true
    subdir: custom/spike_core/spike_layout
  iek:
    subdir: contrib
    version: '1.4'
  imagecache_actions:
    subdir: contrib
    version: '1.9'
  image_field_caption:
    subdir: contrib
    version: '2.3'
  image_link_formatter:
    subdir: patched
    version: '1.1'
  image_resize_filter:
    subdir: contrib
    version: '1.16'
  imagecache_token:
    subdir: contrib
    version: 1.0-rc2
  imagefield_focus:
    subdir: contrib
    version: '1.0'
  inline_conditions:
    subdir: contrib
    version: 1.0-rc1
  inline_entity_form:
    subdir: contrib
    version: '1.8'
  insert:
    subdir: contrib
    version: '1.4'
  jcarousel:
    subdir: contrib
    version: '2.7'
  job_scheduler:
    subdir: contrib
    version: '2.0'
  jquery_update:
    version: '2.7'
  js:
    subdir: contrib
    version: '2.4'
  json2:
    version: '1.1'
  l10n_update:
    subdir: contrib
    version: '1.1'
  layout:
    version: 1.0-alpha6
  libraries:
    subdir: contrib
    version: '2.5'
  link:
    subdir: patched
    version: '1.5'
  logintoboggan:
    subdir: contrib
    version: '1.5'
  mailsystem:
    subdir: contrib
    version: '2.35'
  masonry:
    subdir: contrib
    version: 3.x-dev
  masonry_views:
    subdir: contrib
    version: '3.0'
  media:
    subdir: contrib
    version: '2.20'
  menu_attributes:
    subdir: contrib
    version: '1.0'
  messageclose:
    subdir: contrib
    version: '1.5'
  metatag:
    subdir: contrib
    version: '1.25'
  mimemail:
    subdir: contrib
    version: '1.1'
  module_filter:
    version: '2.1'
  multiupload_filefield_widget:
    subdir: contrib
    version: '1.13'
  multiupload_imagefield_widget:
    subdir: contrib
    version: '1.3'
  nodequeue:
    subdir: contrib
    version: '2.1'
  nojs:
    subdir: contrib
    version: '1.0'
  override_node_options:
    subdir: contrib
    version: '1.14'
  panelizer:
    subdir: contrib
    version: '3.4'
  paragraphs:
    subdir: contrib
    version: 1.0-rc5
  paragraphs_pack:
    subdir: contrib
    version: 1.0-alpha5
  pathauto:
    subdir: contrib
    version: '1.3'
  pathologic:
    subdir: contrib
    version: '2.12'
  rabbit_hole:
    subdir: contrib
    version: '2.24'
  responsive_preview:
    version: '1.1'
  retina_images:
    subdir: contrib
    version: 1.0-beta5
  rules:
    subdir: contrib
    version: '2.11'
  rules_conditional:
    subdir: contrib
    version: 1.0-beta2
  scheduler:
    subdir: contrib
    version: '1.5'
  search_api:
    subdir: contrib
    version: '1.25'
  search_api_db:
    subdir: contrib
    version: '1.7'
  shs:
    subdir: contrib
    version: '1.7'
  simple_cookie_compliance:
    subdir: contrib
    version: '1.5'
  spike:
    type: theme
    custom_download: true
  smart_trim:
    subdir: contrib
    version: '1.5'
  smartcrop:
    subdir: contrib
    version: 1.0-beta2
  special_menu_items:
    subdir: contrib
    version: '2.0'
  spike_advert_content:
    type: module
    custom_download: true
    subdir: custom_ct
    version: '1.4'
  spike_article:
    type: module
    custom_download: true
    subdir: custom
    version: '1.4'
  spike_checkout_messages:
    type: module
    custom_download: true
    subdir: custom/custom_commerce/spike_shop
    version: '1.0'
  spike_core:
    type: module
    custom_download: true
    subdir: custom
    version: '1.2'
  spike_display_extra:
    type: module
    custom_download: true
    subdir: custom_ct
    version: '1.0'
  spike_editions:
    type: module
    custom_download: true
    subdir: custom_ct
    version: '1.5'
  spike_flags:
    type: module
    custom_download: true
    subdir: custom
    version: '1.3'
  spike_flipbook:
    type: module
    custom_download: true
    subdir: custom
    version: '1.0'
  spike_image_article:
    type: module
    custom_download: true
    subdir: custom
    version: '1.4'
  spike_issue:
    type: module
    custom_download: true
    subdir: custom
    version: '1.4'
  spike_lang:
    type: module
    custom_download: true
    subdir: custom
    version: 1.0-beta1
  spike_layout:
    type: module
    custom_download: true
    subdir: custom/spike_core
    version: '1.9'
  spike_logo_image:
    type: module
    custom_download: true
    subdir: custom
    version: 1.0-beta1
  spike_main_menu:
    type: module
    custom_download: true
    subdir: custom
    version: '1.1'
  spike_metatags:
    type: module
    custom_download: true
    subdir: custom
    version: '1.0'
  spike_newsletter:
    type: module
    custom_download: true
    subdir: custom
    version: '1.0'
  spike_node_pages:
    type: module
    custom_download: true
    subdir: custom
    version: '1.4'
  spike_permissions:
    type: module
    custom_download: true
    subdir: custom
    version: '1.1'
  spike_quotations:
    type: module
    custom_download: true
    subdir: custom
    version: '1.1'
  spike_search:
    type: module
    custom_download: true
    subdir: custom
    version: '1.2'
  spike_shop:
    type: module
    custom_download: true
    subdir: custom/custom_commerce
    version: '1.10'
  spike_shop_content_types:
    type: module
    custom_download: true
    subdir: custom/spike_shop
    version: '1.3'
  spike_splash_page_image:
    type: module
    custom_download: true
    subdir: custom_ct
    version: '1.0'
  spike_taxonomy:
    type: module
    custom_download: true
    subdir: custom
    version: '1.1'
  stage_file_proxy:
    subdir: contrib
    version: '1.8'
  stringoverrides:
    subdir: contrib
    version: '1.8'
  strongarm:
    subdir: contrib
    version: 2.x-dev
  table_element:
    subdir: contrib
    version: 1.0-beta5
  taxonomy_manager:
    subdir: contrib
    version: '1.0'
  taxonomy_menu:
    subdir: contrib
    version: '1.5'
  token:
    subdir: contrib
    version: '1.7'
  translation_helpers:
    subdir: contrib
    version: '1.0'
  transliteration:
    subdir: contrib
    version: '3.2'
  uuid:
    subdir: contrib
    version: '1.2'
  uuid_features:
    subdir: contrib
    version: 1.0-rc1
  variable:
    subdir: contrib
    version: '2.5'
  variable_email:
    subdir: contrib
    version: 1.0-alpha1
  video_filter:
    subdir: contrib
    version: 3.x-dev
  view_unpublished:
    subdir: contrib
    version: '1.2'
  views:
    version: '3.20'
  views_infinite_scroll:
    subdir: contrib
    version: '1.1'
  views_slideshow:
    subdir: contrib
    version: '3.9'
  views_ticker:
    subdir: contrib
    version: '2.0'
  waypoints:
    subdir: contrib
    version: '1.0'
  wysiwyg:
    subdir: contrib
    version: '2.5'
  wysiwyg_filter:
    subdir: contrib
    version: 1.6-rc9
  bootstrap:
    version: '3.19'
  ember:
    version: 2.0-alpha4
  responsive_bartik:
    version: 1.0-rc1
  ibt_custom_tax:
    type: module
    custom_download: true
  spike_mail:
    type: module
    custom_download: true
  spike_products:
    type: module
    custom_download: true
  custom_admin_seven_sub_theme:
    type: theme
    custom_download: true
libraries:
  _ckeditor:
    directory_name: _ckeditor
    custom_download: true
    type: library
  underscore:
    directory_name: underscore
    custom_download: true
    type: library
  modernizr:
    directory_name: modernizr
    custom_download: true
    type: library
  json2:
    directory_name: json2
    custom_download: true
    type: library
  backbone:
    directory_name: backbone
    custom_download: true
    type: library
  ___ckeditor_mini:
    directory_name: ___ckeditor_mini
    custom_download: true
    type: library
  isOnScreen:
    directory_name: isOnScreen
    custom_download: true
    type: library
  rotate3Di:
    directory_name: rotate3Di
    custom_download: true
    type: library
  Hover:
    directory_name: Hover
    custom_download: true
    type: library
  bootstrap:
    directory_name: bootstrap
    custom_download: true
    type: library
  fontawesome:
    directory_name: fontawesome
    custom_download: true
    type: library
  waypoints:
    directory_name: waypoints
    custom_download: true
    type: library
  jsonpath:
    directory_name: jsonpath
    custom_download: true
    type: library
  jflip:
    directory_name: jflip
    custom_download: true
    type: library
  Slides-SlidesJS-3:
    directory_name: Slides-SlidesJS-3
    custom_download: true
    type: library
  debounce:
    directory_name: debounce
    custom_download: true
    type: library
  turn.js:
    directory_name: turn.js
    custom_download: true
    type: library
  animate_shadow:
    directory_name: animate_shadow
    custom_download: true
    type: library
  shiner:
    directory_name: shiner
    custom_download: true
    type: library
  jquery.cycle:
    directory_name: jquery.cycle
    custom_download: true
    type: library
  emogrifier:
    directory_name: emogrifier
    custom_download: true
    type: library
  masonry:
    directory_name: masonry
    custom_download: true
    type: library
  jquery-csv:
    directory_name: jquery-csv
    custom_download: true
    type: library
  flipbook:
    directory_name: flipbook
    custom_download: true
    type: library
  colorbox:
    directory_name: colorbox
    custom_download: true
    type: library
  ckeditor:
    directory_name: ckeditor
    custom_download: true
    type: library
  readmore_formatter:
    directory_name: readmore_formatter
    custom_download: true
    type: library
  velocity:
    directory_name: velocity
    custom_download: true
    type: library
  jquery-visible:
    directory_name: jquery-visible
    custom_download: true
    type: library
  Web-Ticker:
    directory_name: Web-Ticker
    custom_download: true
    type: library
  imagesloaded:
    directory_name: imagesloaded
    custom_download: true
    type: library
  turnjs4:
    directory_name: turnjs4
    custom_download: true
    type: library
  fileicon_css.css:
    directory_name: fileicon_css.css
    custom_download: true
    type: library
  autopager:
    directory_name: autopager
    custom_download: true
    type: library
  gifresizer:
    directory_name: gifresizer
    custom_download: true
    type: library
  Flip-jQuery:
    directory_name: Flip-jQuery
    custom_download: true
    type: library
  ScrollMagic:
    directory_name: ScrollMagic
    custom_download: true
    type: library
  markitup:
    directory_name: markitup
    custom_download: true
    type: library
  jcarousel:
    directory_name: jcarousel
    custom_download: true
    type: library
  unevent:
    directory_name: unevent
    custom_download: true
    type: library
  scrollorama:
    directory_name: scrollorama
    custom_download: true
    type: library
  dompdf:
    directory_name: dompdf
    custom_download: true
    type: library
  jquery_color:
    directory_name: jquery_color
    custom_download: true
    type: library
