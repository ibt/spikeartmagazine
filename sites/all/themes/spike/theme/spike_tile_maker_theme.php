<?php

/*
 * @file spike_tile_maker_theme.php
 */

function spike_tile_maker_theme_callback($nid) {
  if ($nid) {
    $node_view = node_view(node_load($nid));
    $node = drupal_render($node_view);

    return $node;
    return '<div class="tile-breakpoint views-row">' . $node . '</div>';
  }
}
//function spike_tile_maker_theme_callback($nid) {
//  if ($nid) {
//    $node_view = node_view(node_load($nid));
//    $node = drupal_render($node_view);
//    return '<div class="tile-breakpoint">' . $node . '</div>';
//  }
//}


function spike_quotation_background_color() {
  $colors = array('#0F0FFF', '#FFFF96', '#1EFF00', '#FF5C99');
  shuffle($colors);
  return array_pop($colors);
}
