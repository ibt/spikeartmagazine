<?php
/**
 * @file
 * bootstrap-accordion.func.php
 * @todo replace this with bootstrap_panel?
 */

/**
 * Implements theme_bootstrap_bare().
 */
function spike_bootstrap_bootstrap_accordion($variables) {
  $elements = $variables['elements'];

  if (empty($variables['id'])) {
    $accordion_id = 'accordion-' . md5($elements);
  }
  else {
    $accordion_id = check_plain($variables['id']);
  }
  $output = '<div class="accordion" id="' . $accordion_id . '">';
  foreach ($elements as $id => $item) {
//    $classes = ' collapse';
    $classes ='collapse';
//    $classes = array_search($id, array_keys($elements)) === 0 ? ' collapse in' : ' collapse';
    $output .= '<div class="accordion-group panel"><div class="accordion-heading">';
//    $output .= '<div class="accordion-group"><div class="accordion-heading">';
    $output .= '<a class="accordion-toggle" data-toggle="collapse" data-parent="#' . $accordion_id . '" href="#' . $id . '">' . check_plain($item['header']) . '</a></div>';
    $output .= '<div id="' . $id . '" class="accordion-body ' . $classes . '"><div class="accordion-inner">';
    $output .= '<ul class"accordion-inner-menu">';
    if (is_array($item['content'])) {
      foreach ($item['content'] as $key=>$val) {

        $attributes = array();
        $li_class = 'inner-item';
        // dropdown exceptions
//        if (isset($val['#theme']) && $val['#theme']=== 'spike_author_menu_dropdown') {
//          $li_class .= 'dropdown';
//          $attributes = isset($val['#attributes']) ? $val['#attributes'] : '';
//          if (isset($val['#below'])) {
//            $val['#below']['#attributes']['class'] = 'dropdown-menu';
//            $dropdown = '<ul class="menu nav dropdown-menu pull-right">';
//            foreach ($val['#below'] as $link) {
//              $dropdown .= render($link);
//            }
//            $dropdown .= '</ul>';
//          }
//        }
        if (isset($val['#title']) && isset($val['#href'])) {
          $output .= '<li class="' . $li_class .'">';
//          $output .= '<li class="' . strtolower($val['#title']) . '-link' . $li_class .'">';
          $output .= l($val['#title'], $val['#href'], array('attributes' => $attributes) );
          if (isset($dropdown)) {
            $output .= $dropdown;
          }
          $output .= '</li>';
        }
      }
    }
    $output .= '</ul>';
    $output .= '</div></div></div>';
  }
  $output .= '</div>';
  return $output;
}
