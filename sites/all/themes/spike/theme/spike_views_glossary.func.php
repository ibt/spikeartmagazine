<?php

/**
 * @file spike_glossary_maker_theme.php
 *
 *
 */

function Unaccent($string) {
  return preg_replace('~&([a-z]{1,2})(acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', htmlentities($string, ENT_QUOTES, 'UTF-8'));
}


function spike_views_glossary($view) {
  $result = $view->result;
  $alphas = range('a', 'z');
  $glossary = array();
  foreach ($alphas as $val) {
    $glossary[]['alpha'] = $val;
  }
  $glossary[]['alpha'] = 'numeric';
  $glossary[]['alpha'] = 'other';
  foreach($result as $key=>$val) {
    switch($view->current_display) {
      case 'page':
        $name = $val->field_data_field_name_sorting_field_name_sorting_value ?
          $val->field_data_field_name_sorting_field_name_sorting_value :
          $val->taxonomy_term_data_name;

        $first_letter = Unaccent(strtolower(substr($name, 0, 1)));

        break;
      case 'page_1':
      default:
        $name = Unaccent($val->taxonomy_term_data_name);
        $first_letter = strtolower(substr($name, 0, 1));
        break;
    }
    $match = array_search ($first_letter , $alphas);
    if (is_numeric($match)) {
      $glossary[$match]['keys'][] = $key;
    } else if (is_numeric($first_letter)) {
      $glossary[26]['keys'][] = $key;
    } else {
      $glossary[27]['keys'][] = $key;
    }
  }
  return $glossary;
}