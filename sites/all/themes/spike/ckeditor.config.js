/*
 WARNING: clear browser's cache after you modify this file.
 If you don't do this, you may notice that browser is ignoring all your changes.
 */
CKEDITOR.editorConfig = function (config) {
  config.language = 'de';

  config.removePlugins = 'widget';

  config.stylesCombo_stylesSet = 'custom_full:/sites/all/themes/spike/ckeditor.styles.js';
  config.templates = 'custom';
  config.templates_files = ['/sites/all/themes/ckeditor/templates/ckeditor/ckeditor_templates.js'];

  config.templates_replaceContent = false;

  config.browserContextMenuOnCtrl = true;

  config.startupOutlineBlocks = true;

  // #97004f, #126eb6
  config.colorButton_colors = 'ff592b,126eb6,97004f,96d79b';

  // The minimum editor width, in pixels, when resizing it with the resize handle.
  config.resize_minWidth = 450;

  // Protect PHP code tags (<?...?>) so CKEditor will not break them when
  // switching from Source to WYSIWYG.
  // Uncommenting this line doesn't mean the user will not be able to type PHP
  // code in the source. This kind of prevention must be done in the server
  // side
  // (as does Drupal), so just leave this line as is.
  config.protectedSource.push(/<\?[\s\S]*?\?>/g); // PHP Code

  // [#1762328] Uncomment the line below to protect <code> tags in CKEditor (hide them in wysiwyg mode).
  // config.protectedSource.push(/<code>[\s\S]*?<\/code>/gi);
  config.extraPlugins = 'widget,lineutils,htmlwriter';

  config.contentsCss = '/sites/all/themes/spike/css/style.css';
  /*
   * Append here extra CSS rules that should be applied into the editing area.
   * Example:
   * config.extraCss = 'body {color:#FF0000;}';
   */
  config.extraCss = '';

  /**
   * CKEditor's editing area body ID & class.
   * See http://drupal.ckeditor.com/tricks
   * This setting can be used if CKEditor does not work well with your theme by default.
   */
  config.bodyClass = '';
  config.bodyId = '';

  // Make CKEditor's edit area as high as the textarea would be.
  if (this.element.$.rows > 0) {
    config.height = this.element.$.rows * 20 + 'px';
  }

  //config.indentClasses = ['rteindent1', 'rteindent2', 'rteindent3', 'rteindent4'];

  // [ Left, Center, Right, Justified ]
  config.justifyClasses = ['left', 'center', 'right', 'justify'];
};