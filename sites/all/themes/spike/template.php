<?php

/**
 * @file
 * template.php
 */

/**
 * THEME ELEMENT WRAPPER
 */

function theme_render_div($variables) {
  if (!isset($variables['element']['items'])) return;
  $items = $variables['element']['items'];
  $output = '<div class="render-wrapper">';
  foreach ($items as $el) {
    $class = 'render-el ';
    if (isset($el['attributes']['class'])) {
      $class .= $el['attributes']['class'];
    }
    $output .= '<div class="' . $class . '">';
    $output .= render($el);
    $output .= '</div>';
  }
  $output .= '</div>';
  return $output;
}


function spike_preprocess_field(&$variables) {
  # hide thumbnails on image_articles which are flagged hide_thumbnails
  if (
    isset($variables['element']['#object'])
    && isset($variables['element']['#object']->type)
    && $variables['element']['#object']->type == 'image_article'
    && isset($variables['element']['#field_name'])
    && $variables['element']['#field_name'] == 'field_image'
    && isset($variables['element']['#view_mode'])
    && $variables['element']['#view_mode'] == 'full'
  ) {
    $node = $variables['element']['#object'];
    $flag_hide_thumbs = flag_get_flag('hide_thumbnails');
    if ( $flag_hide_thumbs
      && $flag_hide_thumbs->is_flagged($node->nid)) {
      $variables['items'] = [];
      $variables['classes_array'] = [];
    }
  }
  if (
    isset($variables['element']['#object'])
    && isset($variables['element']['#object']->type)
    && $variables['element']['#object']->type == 'image_article'
    && isset($variables['element']['#view_mode'])
    && $variables['element']['#view_mode'] == 'teaser'
    && isset($variables['element']['#field_name'])
  ) {
    if ($variables['element']['#field_name'] == 'field_title_sub') {
      $node = $variables['element']['#object'];
      if (!isset($node->field_subtitle_display[LANGUAGE_NONE])
        ||
        (isset($node->field_subtitle_display[LANGUAGE_NONE]) && $node->field_subtitle_display[LANGUAGE_NONE][0]['value'] != 1)
        ) {
        $variables['items'] = [];
        $variables['classes_array'] = [];
      }
    }
    if ($variables['element']['#field_name'] == 'field_blurb') {
      $node = $variables['element']['#object'];
      if (!isset($node->field_blurb_display[LANGUAGE_NONE])
        ||
        (isset($node->field_blurb_display[LANGUAGE_NONE]) && $node->field_blurb_display[LANGUAGE_NONE][0]['value'] != 1)
        ) {
        $variables['items'] = [];
        $variables['classes_array'] = [];
      }
    }
  }
  switch ($variables['element']['#field_name']) {
    case 'field_image':

      break;
  }
}


function spike_preprocess_html(&$vars) {
  /*
   * add body classes for 404 & 303 errors
   */
  $headers = drupal_get_http_header();
  if (isset($headers['status'])) {
    if (strpos($headers['status'], '403') >= 0)
      $vars['classes_array'][] = 'error-403';
    elseif (strpos($headers['status'], '404') >= 0)
      $vars['classes_array'][] = 'error-404';
  }

  /*
   * add files for displaying front page
   * * masonry
   * * autopager
   */
//  $autopager_path = libraries_get_path('autopager');
  $masonry_path = libraries_get_path('masonry');
  drupal_add_js($masonry_path . '/jquery.masonry.min.js');


  /*
   * add classes to pages for theming
   */
  $node = menu_get_object();
  if (isset($node) && $node->type == 'issue') {
    $wrapped = entity_metadata_wrapper('node', $node);
    if ($wrapped->field_season_term->raw())  {
      /* add season term as body class */
      $season_title = strtolower($wrapped->field_season_term->value()->name);
      $vars['classes_array'][] = 'term-season-' . $season_title;
    }
  }

  if (
    isset($node)
    &&
    (
      $node->type == 'article'
      || $node->type == 'image_article'
    )
  ) {
    /* add body class for flag legacy caption */
    $flag = flag_get_flag('legacy_captions');
    if ($flag && $flag->is_flagged($node->nid)) {
      $vars['classes_array'][] = 'legacy-caption-article';
    }
    /* add body class for flag sponsored articles  */
    $flag = flag_get_flag('sponsored');
    if ($flag && $flag->is_flagged($node->nid)) {
      $vars['classes_array'][] = 'sponsored-article';
    }
  }


  $filepath = path_to_theme() . '/fonts/font-awesome/css/font-awesome.min.css';
  drupal_add_css($filepath, array(
    'group' => CSS_THEME,
  ));


  /*
 * MODIFY PAGE TITLES
 */
  spike_core_set_page_titles($vars);

}


/**
 * Implements theme_preprocess_page()
 *
 * @param $variables
 */
function spike_preprocess_page(&$variables) {


  /*
   * run advert splash display plugin
   * if any adverts are flagged for splash, they'll be rendered as block elements
   * into the region & styled by js
   */
  if (
    drupal_is_front_page()
    && isset($variables['page']['navigation'])
  ) {
    $advert_splash = spike_advert_content_splash_display();
    $advert_splash['markup'] = render($advert_splash['content']);
    array_push($variables['page']['navigation'], array('#markup' => $advert_splash['markup']));
  }
  $t=1;

  /*
   * @TODO | evaluate this - do we want this here ?
   */
  drupal_add_library('spike_core', 'rotate3Di');

  /*
   * change default bootstrap sidebar handling - change to constant width
   */
  if (!empty($variables['page']['sidebar_first']) && !empty($variables['page']['sidebar_second'])) {
    $variables['content_column_class'] = ' class="col-sm-11"';
  }
  elseif (!empty($variables['page']['sidebar_first']) || !empty($variables['page']['sidebar_second'])) {
    $variables['content_column_class'] = ' class="col-sm-11"';
  }
  else {
    $variables['content_column_class'] = ' class="col-sm-11"';
  }

}

/**
 * Implements theme_preprocess_node()
 *
 * @param $var
 */
function spike_preprocess_node(&$var) {
  /*
   * override advert class wrappers to avoid the word ADVERT in markup
   */
  if ( $var && is_array($var) && isset($var['type']) && (
      $var['type'] === 'advert_content'
      || $var['type'] === 'advert_sidebar'
    )

  ) {
    $var['classes_array'][1] = 'node-special-content';
  }

  /*
   * FLAG FUNCTION: ROUND FLAG
   * render round teasers based on nodes flagged ROUND
   */
  if ($var && is_array($var) && isset($var['type']) && $var['type'] === 'article') {
    // @TODO | why do we unset the blurb field ???
    unset($var['node']->field_blurb);
    // add node class for articles tagged with category: 'Column'
    // we use this for adding circular formatting to the square image style
    if (isset($var['node']) && $var['node']->nid && $var['teaser'] == TRUE) {
      $flag = flag_get_flag('round');
      if (
        $flag && $flag->is_flagged($var['node']->nid)
      ) {
        $var['classes_array'][] = 'teaser-square';
      }
    }
  }
  switch ($var['type']) {
    case 'article':
      // Add JS for inline advert placement.
      if ($var['view_mode'] == 'full') {
        drupal_add_js(drupal_get_path('module', 'spike_core') . '/js/spike_core_ads.js');
      }
      break;
  }
}


/**
 * field override function
 *
 * field_image Articles only  in fullview
 *
 * * different image / caption formatter for articles flagged 'legacy_captions'
 *
 * @param $variables
 * @return string
 */
// function spike_field__field_image__article($variables) {

//   if (isset($variables['element']['#object'])) {
//     $node = $variables['element']['#object'];
//     $view_mode = $variables['element']['#view_mode'];
//     if(
//       $view_mode == 'full'
//       && isset($node->type)
//       && $node->type == 'article'
//     ) {
//       $flag = flag_get_flag('legacy_captions');
//       if ($flag && $flag->is_flagged($node->nid)) {
//         $display_settings = array(
//           'colorbox_node_style' => 'full_width',
//           'colorbox_node_style_first' => '',
//           'colorbox_image_style' => 'full_width',
//           'colorbox_gallery' => 'post',
//           'colorbox_gallery_custom' => '',
//           'colorbox_caption' => 'none',
//           'colorbox_caption_custom' => '',
//           'field_multiple_limit' => '-1',
//           'field_multiple_limit_offset' => '0',
//           'colorbox_multivalue_index' => NULL
//         );
//         foreach ($variables['items'] as $key=>$item) {
//           $variables['items'][$key]['#theme'] = 'colorbox_image_formatter';
//           $variables['items'][$key]['#display_settings']['colorbox_node_style'] = 'spike_large';
//           $variables['items'][$key]['#display_settings'] = $display_settings;
//         }
//       }
//     }
//   }

// //   $t=1;

//   $output = '';
//   // Render the items.
//   $output .= '<div class="field-items"' . $variables['content_attributes'] . '>';
//   foreach ($variables['items'] as $delta => $item) {
//     // $output .= render($item);
//     /*
//      * custom colorbox formatting
//      */

//     $path = image_style_path('full_width', $item['#item']['uri']);

//     $style_path = image_style_url('full_width', $path);

//     $options = array(
//       'html' => TRUE,
//       'attributes' => array(
// //        'class' => 'article-image-new',
//         'class' => 'colorbox-load article-image-new',
//         'rel' => array('gallery')

//       ),
//       'query' => array(
//         'width' => 'auto',
//         'height' => 'auto',
//       )
//     );

//     $item = $view_mode == 'full' ? l(drupal_render($item), $style_path, $options) : drupal_render($item);

//     $classes = 'field-item ' . ($delta % 2 ? 'odd' : 'even');
//     $output .= '<div class="' . $classes . '"' . $variables['item_attributes'][$delta] . '>' . $item . '</div>';
//   }
//   $output .= '</div>';
//   // Render the top-level DIV.
//   $output = '<div class="' . $variables['classes'] . '"' . $variables['attributes'] . '>' . $output . '</div>';
//   return $output;
// }




/**
 * field override function
 *
 * field_image EDITIONS only  in fullview
 *
 * @param $variables
 * @return string
 */
function spike_field__field_issue_ref_article__issue($variables) {
  $output = '';
//  if (isset($variables['element']['#object']->nid)) {
//    $nid = $variables['element']['#object']->nid;
//    $view = views_get_view('issue_article_reference');
//    $t=1;
//  }

  // Render the items.
  $output .= '<div class="field-items"' . $variables ['content_attributes'] . '>';
  foreach ($variables ['items'] as $delta => $item) {
    $classes = 'field-item ' . ($delta % 2 ? 'odd' : 'even');
    $output .= '<div class="' . $classes . '"' . $variables ['item_attributes'][$delta] . '>' . drupal_render($item) . '</div>';
  }
  $output .= '</div>';

  // Render the top-level DIV.
  $output = '<div class="' . $variables ['classes'] . '"' . $variables ['attributes'] . '>' . $output . '</div>';

  return $output;
}

function spike_file_icon($variables) {
  $file = $variables['file'];
  $url = file_create_url($file->uri);
  $explode = explode('/', $file->filemime);
  $mime = array_pop($explode);
  $options = array(
    'html' => true,
    'attributes' => array(
      'class' => array(
        'spike-file-icon-link'
      ),
      'target' => '_blank'
    )
  );
//  $icon = array(
//    '#type' => 'container',
//    '#attributes' => array(
//      'data-type' => array($mime),
//      'class' => array(
//        'file-icon file-icon-xl'
//      )
//    ),
//  );
  $img_path = drupal_get_path('theme', 'spike') . '/images/info-pages/pdf_symbol-01-small.png';
  $icon = theme_image(array(
    'path' => $img_path,
    'width' => 150,
    'height' => 159,
    'alt' => '',
    'title' => '',
    'attributes' => array(),
  ));
  $render = render($icon);
  return l($render, $url, $options);
}

function spike_file_link($variables) {

  $file = $variables ['file'];
  $icon_directory = $variables ['icon_directory'];

  $url = file_create_url($file->uri);
  $icon = theme('file_icon', array('file' => $file, 'icon_directory' => $icon_directory));

  // Set options as per anchor format described at
  // http://microformats.org/wiki/file-format-examples
  $options = array(
    'attributes' => array(
      'type' => $file->filemime . '; length=' . $file->filesize,
    ),
  );

  // If desription is empty, attempt to extract from referencing ent,
  // if that fails, use filename
  $desc = '';
  if (empty($file->description)) {
    if (isset($file->referencing_entity)) {
      $ent = entity_metadata_wrapper('node', $file->referencing_entity);
      if ($ent) {
        foreach ($ent->field_file->value() as $f) {
          if ($f['fid'] == $file->fid) {
            $desc = $f['description'];
          }
        }
      }
    }
    $link_text = $desc ? $desc : $file->filename;
  }
  else {
    $link_text = $file->description;
    $options ['attributes']['title'] = check_plain($file->filename);
  }

  $options ['attributes']['class'] = array('file-name');
  $options ['attributes']['target'] = '_blank';
  return '<span class="file">' . $icon . ' <h4>' . l($link_text, $url, $options) . '</h4></span>';


}



/*
 * HELPER FUNCTIONS
 */

function random_hex_color() {
  return random_color_part() . random_color_part() . random_color_part();
}

function random_color_part() {
  return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
}

function random_step() {
  return rand(0, 255);
}

function adjustBrightness($hex, $steps) {
  // Steps should be between -255 and 255. Negative = darker, positive = lighter
  $steps = max(-255, min(255, $steps));

  // Format the hex color string
  $hex = str_replace('#', '', $hex);
  if (strlen($hex) == 3) {
    $hex = str_repeat(substr($hex,0,1), 2).str_repeat(substr($hex,1,1), 2).str_repeat(substr($hex,2,1), 2);
  }

  // Get decimal values
  $r = hexdec(substr($hex,0,2));
  $g = hexdec(substr($hex,2,2));
  $b = hexdec(substr($hex,4,2));

  // Adjust number of steps and keep it inside 0 to 255
  $r = max(0,min(255,$r + $steps));
  $g = max(0,min(255,$g + $steps));
  $b = max(0,min(255,$b + $steps));

  $r_hex = str_pad(dechex($r), 2, '0', STR_PAD_LEFT);
  $g_hex = str_pad(dechex($g), 2, '0', STR_PAD_LEFT);
  $b_hex = str_pad(dechex($b), 2, '0', STR_PAD_LEFT);

  return '#'.$r_hex.$g_hex.$b_hex;
}
