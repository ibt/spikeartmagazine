if (typeof(CKEDITOR) !== 'undefined') {
  CKEDITOR.dtd.div.img = 1;
  CKEDITOR.dtd.div.p = 1;
  CKEDITOR.addStylesSet('custom_full',
    [
      {name: 'Paragraph', element: 'p', attributes: {'class': ''}},
      {name: 'Heading 1', element: 'h1'},
      {name: 'Heading 2', element: 'h2'},
      {name: 'Heading 3', element: 'h3'},
      {name: 'Heading 4', element: 'h4'},
      {name: 'Heading 5', element: 'h5'},
      {name: 'Heading 6', element: 'h6'},
      {name: 'Download-Link', element: 'a', attributes: {'class': 'download'}}
    ]);
}