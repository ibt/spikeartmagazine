(function($) {

  Drupal.behaviors.spikeFrontToggle = {

    attach: function (context, settings) {

      /* SPLASH PAGE FUNCTIONALITY
       */

      /* GLOBAL VARS */
      var $navbar = $('#navbar')
      var $logo = $('.logo');

      var logoWbaq = 792;
      var logoHbaq = 234;
      var endW = 500;


      var logoHpadding = 0;
      var $main = $('.col-sm-11');
      var $barToggle = $('#bar-toggle');
      var $sb1 = $('.sb-1');
      var $sb2 = $('.sb-2');

      /*
      FUNCTIONS
       */

      /*
       logoScaleCalc
       return scale value for splash logo image based on scroll position
       */

      function logoScaleCalc($img) {
        var scrollTop = $(window).scrollTop();
        var mainTop = $main.position().top;
        var scrollFactor = scrollTop / mainTop;
        var varScale = 1 - scrollFactor;
        var scale = 'scale( '+ varScale + ', ' + varScale + ')';
        return scale;
      }



      /*
      BODY ON navSetPosition
      position language & search controls
       */
      $('body').on('navSetPosition', function() {
        var $search = $('#block-spike-search-spike-search-form');
        var $subscribe = $('#block-spike-newsletter-spike-subscribe-block-navbar');
        var $lang = $('#block-spike-lang-language-control');
	var top = $main.offset().top;
        var array = [$search, $subscribe];
        $lang.css({top: 30, 'position': 'fixed', 'color': 'black', 'z-index': 100000});
	$lang.remove();
        $lang.show().addClass('in');
	$.each(array, function() {
          $(this).css({
            top: top - 370
          });
          if (top > $(window).scrollTop() + 234) {
            $(this).hide().addClass('out')
          } else {
            $(this).show().addClass('in')
          }
        });
      });

      $('body').on('splashResize', function() {
        /* make navbar splash full screen */
        $logo.css({
          left: ($(window).width() / 2) - ($logo.width() / 2)
        });

      });


      /*
       BODY ON splashInit
       initialize splash page
       */
      $('body').on('splashInit', function() {


        /* img & window position vars */
        var winH = $(window).height();

        /* make navbar splash full screen */
        var winBottom = $(window).height();
        $navbar.css({ height: winBottom + 22 });
        $navbar.addClass('splash-page');

        /* clean up elements */
        var $cont = $('#bottom-region');
        var $search = $('#block-spike-search-spike-search-form');
        var $subscribe = $('#block-spike-newsletter-spike-subscribe-block-navbar');
        var $lang = $('#block-spike-lang-language-control');
        $cont.prepend($search).prepend($lang).prepend($subscribe);

        // set anchor position & element for returning w/o full slash
        $main.prepend('<div id="geil" class="breakpoint"></div>');
        $main.prepend('<div id="cool" class="breakpoint"></div>');
        $main.prepend('<div id="no-splash" class="breakpoint"></div>');

        var points = [$('#geil'), $('#cool'), $('#no-splash')];

        $.each(points, function() {
          $(this).css({
            'position': 'relative',
            top: -203
          });
        });



        // set breakpoint for retracting nav clone
        $main.find('.tile-breakpoint').first().addClass('nav-clone-retract breakpoint')

        // set main content just out of view for easy scroll in
        $main.css({ top: winH + 22 });
        $('.main-container').css({
          'z-index': 5
        })

        // init logo position
        $logo.trigger('logoPosInit');
        $navbar.find('#block-spike-search-spike-search-form').remove();
        $navbar.find('#block-spike-lang-language-control').remove();
        $navbar.find('#block-spike-newsletter-spike-subscribe-block-navbar').remove();

        // make duplicate for blurring
        var $splashImg = $navbar.find('#block-spike-layout-spike-header-image img').first();
        $splashImg.addClass('hide-img');
        $splashImg.siblings().addClass('blurred-img');
        $('<div class="hide-img"></div>').insertBefore('img.hide-img');
        $('<div class="blurred-img"></div>').insertBefore('img.blurred-img');
        var src1 = $('img.hide-img').attr('src');
        var src2 = $('img.blurred-img').attr('src');
        $('div.hide-img').css({
          'background-image': 'url(' + src1 + ')',
          width : '100%',
          height : '100%',
          position : 'fixed',
          top: 0,
          left: 0
        });
        $('div.blurred-img').css({
          'background-image': 'url(' + src2 + ')',
          width : '100%',
          height : '100%',
          position : 'fixed',
          top: 0,
          left: 0
        });
        $('img.hide-img').remove();
        $('img.blurred-img').remove();


      });


      /*
      LOGO ON logOrder
      remove logo from nav and place on seperate level
       */
      $logo.on('logoOrder', function() {
        $('#bottom-region').prepend($logo);
        $logo.css({ 'z-index': 10});
      });

      /*
       LOGO ON logoPosInit
       starting point for logo position
       */
      $logo.on('logoPosInit', function() {
        $logo.addClass('splash-trigger');


        var $img = $logo.children('img');
        var logoW = $img.height() != 0 ? $logo.width() : logoWbaq;

        $logo.css({
          left: ($(window).width() / 2) - (logoW / 2),
          'display': 'block',
          'height' : logoHbaq,
          'width' : logoW
        }).attr('id', 'splash-logo');

        $logo.wrap('<div class="splash-wrapper"></div>');
        $('.splash-wrapper').css({
          'width': '100%',
          'height' : '100%',
          'position' : 'relative',
          'display': 'block',
          'top': 0,
          'left': 0,
          'z-index': 6
        });

      });

      /* ***
       BAR TOGGLE INIT
       visibility relative to splash page
       */
      $('body').on('barToggleInit',function() {
        $barToggle.addClass('out').css({left: -60});

        // bar toggle visibility
        if (
          ($main.offset().top > $(window).scrollTop()
          && $('html').hasClass('front'))
          || ( $(window).scrollTop() == 0)
        ) {
          $barToggle.addClass('out').removeClass('in').css({left: -60});
        } else {
          $barToggle.addClass('in').removeClass('out').css({left: 0});        
        }
      });

      /*
      SIDEBAR ON init
       */
      $('body', context).on('sidebarInit', function() {
        /* sidebars only visible when:
         * * mainTop is above scrolltop plus height of nav clone (234px) */

        var mainTop = $main.position().top;
        var h = $main.outerHeight();

        $(this).find('.sb-').each(function() {

          $(this).css({
            position: 'absolute',
            top: mainTop - 26,
            height: h
          }).addClass('sb-non-sticky');

          if (mainTop <= $(window).scrollTop()) {
            var asdf;
            $(this).css({
              position: 'fixed',
              top: -26
            }).removeClass('sb-non-sticky').addClass('sb-sticky');
          }

          $('.sb-1').css({
            position: 'fixed',
            top: -26
          })

        });

        /* set top value for sidebars: if maintop is in view, keep it stuck to top
         * of main, otherwise stick to top screentop */
//        if (mainTop >= scrTop) {
//          $('.sb-').css({
//            position: 'absolute',
//            top: mainTop - scrTop - 26
//          });
//        }
//        else {
//          $('.sb-').css({
//            position: 'fixed',
//            top: 0 - 26
//          });
//        }
      });

      /*
      SIDEBAR ON scroll
       */
      $('body', context).on('sidebarScroll', function() {
        /* sidebars only visible when:
         * * mainTop is above scrolltop plus height of nav clone (234px) */
        var mainTop = $main.position().top;
        var scrTop = $(window).scrollTop();
        /* set top value for sidebars: if maintop is in view, keep it stuck to top
         * of main, otherwise stick to top screentop */


         if (mainTop >= scrTop) {

           $(this).find('.sb-2.sb-sticky').each(function() {
             $(this).removeClass('sb-sticky').addClass('sb-non-sticky').css({
               position: 'absolute',
               top: mainTop - 26
             });
           });

        } else  {

           $(this).find('.sb-2.sb-non-sticky').each(function() {
             $(this).removeClass('sb-non-sticky').addClass('sb-sticky').css({
               position: 'fixed',
               top: -26
             });
           });

           $(this).find('.sb-1.toggle-open').each(function() {

           });



        }
      });


      /*
       BODY ON scrollScaleInit
       basic functionality for scroll / scale of splash image
       */
      $('body').on('scrollScaleInit', function(){
        scrollScalePosition($logo);
      });

      $('body').on('scrollScaleScroll', function(){

        scrollScalePosition($logo);
//        scrollScalePostion($logo);
      });


      function scrollScalePosition($el) {
        var scrT = $(window).scrollTop();
        var winH = $(window).height();
        var $img = $el.children('img');
        var logoH = $img.height() != 0 ? $el.height() : logoHbaq;
        var logoW = $img.height() != 0 ? $el.width() : logoWbaq;
        var mainT = $main.offset().top;
        var $wr = $el.parent();


        var breakPointHeight = (winH / 2) + (logoH / 2) + (logoHpadding * 3);


        /* logo scroll control in the splash screen */

        function logoBaseline() {
          $el.css({
            position: 'relative',
            top: '50%',
//            top: (winH / 2) - (logoH / 2) - logoHpadding,
            transform: logoScaleCalc($logo) + ' translateY(-50%)',
            '-webkit-transform': logoScaleCalc($logo) + ' translateY(-50%)',
            'transform-origin' : '50% top'
          });
          $wr.css({
            'height' : '100%',
            'width' : '100%',
            'position' : 'fixed',
//            'z-index' : 6
          });
        }

        function logoTransition() {
          var asdf;
          $el.css({
            position: 'relative',
            top: '50%',
            'transform-origin' : '50% top',
            transform: logoScaleCalc($logo) + ' translateY(-50%)',
            '-webkit-transform': logoScaleCalc($logo) + ' translateY(-50%)',
          });
          $wr.css({
            'height' : '100%',
            'width' : '100%',
            'position' : 'fixed',
//            'z-index' : 6

          });
        }

        function logoFinish() {
          var ratio = endW / logoW; // basic ratio of full to end
          var newH = document.getElementById('splash-logo').getBoundingClientRect().height;
          $el.css({
            position: 'relative',
            top: winH - logoH - logoHpadding - 20,
//            top: '100%',
            transform: 'scale(' + ratio + ', ' + ratio + ')'  + ' translateY(0)',
            '-webkit-transform': 'scale(' + ratio + ', ' + ratio + ')'  + ' translateY(0)',
//            transform: 'scale(' + ratio + ', ' + ratio + ')'  + ' translateY(-100%)',
            'transform-origin' : '50% bottom'
//            transform: 'scale3d(' + ratio +e ', ' + ratio + ', ' + 1 + ')'
          });

          $wr.css({
            'position' : 'absolute',
            'width' : '100%',
            'height' : winH
//            'z-index' : 5
//            'height' : (winH / 2) + (logoHbaq / 2),
          })
        }

        // at baseline or negative scrolltop - bounce protection
        if ( scrT <= 0 ) {
          var asdf;
          logoBaseline()
        }
        // logo in transitional phase
        else if (
          scrT > 0
          && mainT - scrT > breakPointHeight
          )  {
          logoTransition()
        } else {
          // logo in finished phase
          var asdfl;
          logoFinish()
        }


      }




//      function scrollScalePostion($el) {
//        var scrT = $(window).scrollTop();
//        var winH = $(window).height();
//        var $img = $el.children('img');
//        var logoH = $img.height() != 0 ? $el.height() : logoHbaq;
//        var logoW = $img.height() != 0 ? $el.width() : logoWbaq;
//        var mainT = $main.offset().top;
//
//        var breakPointHeight = (winH / 2) + (logoH / 2);
//
//        /* logo scroll control in the splash screen */
//        if (scrT < 0) {
//          $el.css({
//            position: 'fixed',
//            top: (winH / 2) - (logoH / 2) - logoHpadding,
//            transform: logoScaleCalc($logo) // at baseline, full scale
//          });
//        } else if (scrT == 0) {
//          // at baseline
//          var asd;
//          $el.css({
//            position: 'fixed',
//            top: (winH / 2) - (logoH / 2) - logoHpadding,
//            transform: 'scale3d(1, 1, 1)' // at baseline, full scale
//          });
//        } else if (
//          scrT > 0
//            && mainT - scrT > breakPointHeight
//          )  {
//          // logo in transitional phase
//          $el.css({
//            position: 'fixed',
//            top: (winH / 2) - (logoH / 2) - logoHpadding,
//            transform: logoScaleCalc($logo)
//          });
//        } else {
//          // logo in finished phase
//          var ratio = endW / logoW; // basic ratio of full to end
//          $el.css({
//            position: 'absolute',
//            top: mainT - logoH - logoHpadding,
//            transform: 'scale3d(' + ratio + ', ' + ratio + ', ' + 1 + ')'
//          });
////          $('.advert-splash').removeClass('in').addClass('out').fadeOut();
//        }
//      }

       /*
       BODY ON scrollScaleController
       basic functionality for scroll / scale of splash image
       */
      $('body').on('scrollScaleController', function(){

        // scale & place splash element with scroll
//        $('body').trigger('scrollScaleInit');
        $('body').trigger('scrollScaleScroll');

        // スクロールの値と透明度
        // scroll value and opacity
        if ($main.position().top >= $(window).scrollTop()) {
          var s = $(window).scrollTop();
          var w = $(window).height() - 234;
          opacityVal = 1 / (w / (w - s));
          $('.hide-img').css('opacity', opacityVal);
        }
        var op = $main.offset().top < $(window).scrollTop() ? 0 : 1;
        var array = [
          $('.splash-wrapper'),
//          $('#block-spike-search-spike-search-form'),
//          $('#block-spike-lang-language-control'),
//          $('#block-spike-newsletter-spike-subscribe-block-navbar'),
          $('.splash-page')
        ]
        $.each(array, function() {
          $(this).css({
            'opacity': op
          })
        })

      });


      /*
      body scroll actions
      * sidebars, bar-toggle, & header extra element placement & visibility
       */
      $('body').on('bodyScrollActions', function() {
        // 1st breakpoint var
        var mainTop = $main.position();
        var scrTop = $(window).scrollTop();

        // bar toggle visibility based on top of main passing scrolltop
        if (mainTop.top > scrTop
          && $barToggle.hasClass('in')
          && $('body').hasClass('front')
          ) {
//          console.log('bar toggle out');
          $barToggle.removeClass('in').addClass('out').css({
//            'margin-left': -60
            left: -60
          });
        } else if (
          mainTop.top < scrTop
            && $barToggle.hasClass('out')
          )
        {
//          console.log('bar toggle in');
          if ($sb1.hasClass('toggle-open')) {
            $barToggle.removeClass('out').addClass('in').velocity({
//              'margin-left': 180
              left: 180
//              left: '+=180'
            });
          } else {
            $barToggle.removeClass('out').addClass('in').velocity({
//              'margin-left': 0
              left: 0
//              left: '+=60'
            });

          }
        }

        // fade splash adverts inverse to nav extras below
        var $splash_adverts = $('.advert-splash');
        var array = [$search, $subscribe, $splash_adverts];
        $.each(array, function() {
          if (mainTop.top < scrTop + 234) {
            if ($(this).hasClass('in')) {
              $(this).removeClass('in').addClass('out').fadeOut();
              console.log("fading out");
            }
          } else {
            if ($(this).hasClass('out')) {
              $(this).removeClass('out').addClass('in').fadeIn();
              console.log("fading in");
            }
          }
        });

        // fade nav extras in & out based on main top
        var $search = $('#block-spike-search-spike-search-form');
        var $subscribe = $('#block-spike-newsletter-spike-subscribe-block-navbar');
        var $lang = $('#block-spike-lang-language-control');
        var array = [$search, $subscribe];
        var screen = $(window).width();
        $.each(array, function() {
          if (mainTop.top < scrTop + 234) {
            var asdf;
            if ($(this).hasClass('out')) {
              $(this).removeClass('out').addClass('in').fadeIn();
              if (screen <= 768) {
                $('.logo').fadeOut();
              }
            }
          } else {
            if ($(this).hasClass('in')) {
              $(this).removeClass('in').addClass('out').fadeOut();
              if (screen <= 768) {
                $('.logo').fadeIn();
              }
            }
          }
        });
      });



      /*
      ADVERTS FLOATING once
      move advert block to level with logo
      place them nicely on screen
      prepare elements for scroll actions
       */
      $('.view-spike-adverts-floating').on('advertSplash', function() {

        var top = $main.offset().top;
        var rand_x=0;
        var rand_y=0;
        var min_x = 20;
        var min_y = 20;
        var area;
        var winW = $(window).width();
        var winH = $(window).height();

        function check_overlap(area) {

          for (var i = 0; i < filled_areas.length; i++) {
            var check_area = filled_areas[i];
            var bottom1 = area.y + area.height;
            var bottom2 = check_area.y + check_area.height;
            var top1 = area.y;
            var top2 = check_area.y;
            var left1 = area.x;
            var left2 = check_area.x;
            var right1 = area.x + area.width;
            var right2 = check_area.x + check_area.width;

            if
              (
              (bottom1 < top2 || top1 > bottom2 || right1 < left2 || left1 > right2)
//                  ||
//                  (
            // attempt to keep the image off the logo - prob have to return to this
//                    (top2 >= midL && right2 + x <= midR)
            //                  ||
            //                  (top2 >= midL && top2 + x <= midR)
//                  )
              )
            {
              continue;
            }
            return true;

          }
          return false;
        }

        var filled_areas = new Array();
        function check_overlap(area) {

          for (var i = 0; i < filled_areas.length; i++) {
            var check_area = filled_areas[i];
            var bottom1 = area.y + area.height;
            var bottom2 = check_area.y + check_area.height;
            var top1 = area.y;
            var top2 = check_area.y;
            var left1 = area.x;
            var left2 = check_area.x;
            var right1 = area.x + area.width;
            var right2 = check_area.x + check_area.width;

            if
              (
              (bottom1 < top2 || top1 > bottom2 || right1 < left2 || left1 > right2)
              )
            {
              continue;
            }
            return true;

          }
          return false;
        }


        var logoW = 790;
        var midL = (winW / 2) - (logoW / 2);
        var midR = midL + logoW;
        console.log(
//          'logo left: ' + midL + '<br>' +
//          'logo right: ' + midR + '<br>'
        )
        // tablet protection
        if (winW <= 1025) {
          $(this).hide();
//          console.log('hidden advert splash');
        } else {



          $('body').append($(this));
          $(this).find('.views-row').each(function() {
            // rename tag (for ease)
            $(this).addClass('advert-splash');

            // give classes to identify for scroll controll
            if (top > $(window).scrollTop() + 234) {
              $(this).hide().addClass('in')
            } else {
              $(this).show().addClass('out')
            }

            var w = $(this).width();
            var h = $(this).height();
  //          var x = $(this).width();
  //          var y = $(this).height();
            var max_x = winW - w - 20;
            var max_y = winH - h - 20;

            do {
              rand_x = Math.round((max_x*(Math.random() % 1)) + min_x);
              rand_y = Math.round((max_y)*(Math.random() % 1) + min_y);
              area = {x: rand_x, y: rand_y, width: w, height: h};
  //            area = {x: rand_x, y: rand_y, width: $(this).width(), height: $(this).height()};
            } while(check_overlap(area));

            filled_areas.push(area);
            $(this).css({left:rand_x, top: rand_y});

          });

        }


      });


      /* ***
      BODY ONCE
      initiate all elements & event listeners
       *** */
      $('body', context).once(function() {
//        $(this).hide();

        /* items to run on front only */
        var $body = $(this);
        if (
          $body.hasClass('front')
        || $body.hasClass('page-glish')
        || $body.hasClass('page-utsch')
          ) {

          /* init advert-splash-float positioning */
          $('.view-spike-adverts-floating').trigger('advertSplash');


          /* click animation
           *  */
          $logo.click(function(e) {
            if ($logo.hasClass('splash-trigger')) {
              e.preventDefault();
              var imgH = $logo.height();
              $('html, body').animate({
                scrollTop: $("#no-splash").offset().top
//                scrollTop: $("#no-splash").offset().top - imgH + 40
              }, 1000, function() {
              });
            }
          });
          $('#home-link').click(function(e) {
            if ($('body').hasClass('front')) {
              e.preventDefault();
              var imgH = $logo.height();
              $('html, body').animate({
                scrollTop: $("#no-splash").offset().top
//                scrollTop: $("#no-splash").offset().top - imgH + 40
              }, 700, function() {
              });
            }
          });

          /* end click */

          /* *
           resize initiation
           * */
          $(window).resize(function() {
            /* reposition splash init items*/
//            $logo.trigger('logoPosInit');
            $('.view-spike-adverts-floating').trigger('advertSplash');
            $('body').trigger('splashResize');

            /* keep front-top-spikes covering main splash,
             place at top of main */
//            $('.front-top-spikes').css({
//              top: $('.col-sm-11').offset().top - 22
//            });
//            $('#front-top-shadow').css({
//              top: $('.col-sm-11').offset().top - 22
//            });
//            $('#front-top-after').css({
//              top: $('.col-sm-11').offset().top - 22
//            });
          }); /* end resize */

          if (
            $body.hasClass('front')
            ) {
            /* rearrange page elements */
            $logo.trigger('logoOrder');

            /* init default body positions */
            $('body').trigger('splashInit');

            /* search & lang menus */
            $('body').trigger('navSetPosition');

          }
          /* sroll control */
          $('body').trigger('scrollScaleInit');
          $(window).scroll( $.throttle( 10, false, function(){
            $('body').trigger('scrollScaleController');
            $('body').trigger('sidebarScroll');
            $('body').trigger('bodyScrollActions');


          })); /* end scroll control */

          // SAFARI DEBUG
          // B/C  SCROLLING ANIMATION IS BAD IN SAFARI,
          // BUILD WORKAROUND BASED ON FAST SCROLLING TEST
          var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
          if (isSafari) {
            var lastOffset = $(window).scrollTop();
            var lastDate = new Date().getTime();
            $(window).scroll(function(e) {
              var delayInMs = e.timeStamp - lastDate;
              var offset = $(window).scrollTop() - lastOffset;
              var speedInpxPerMs = offset / delayInMs;
              lastDate = e.timeStamp;
              lastOffset = $(window).scrollTop();
              var limit = 1.5;
              speedInpxPerMs = speedInpxPerMs < 0 ? speedInpxPerMs * -1 : speedInpxPerMs;
              if (speedInpxPerMs  > limit) {
//                console.log ('say somethign')
              }
              var op = speedInpxPerMs >= limit ? 0 : 1;
              $logo.css({
                'opacity': op
              }, function() {
                if ($main.offset().top > $(window).scrollTop()) {
                  $logo.css({
                    'opacity' : 1
                  })
                }
              });
//              console.log('scrollspeed = ' + speedInpxPerMs);

            });

          }


        }

        /* items to run on every page */

        /* init sidebar positions */
        $('body').trigger('sidebarInit');

        /* init bartoggle position */
        $('body').trigger('barToggleInit');

//        $(this).fadeIn();
      });



    }
  };
})(jQuery);
