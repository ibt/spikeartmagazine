(function($) {

  Drupal.behaviors.elementRotator = {

    attach: function (context, settings) {

      var hover = false;

      /* SIDE FLIPPER */
      function mySideChange(front) {
        if (front) {
          $(this).parent().find('#side-1').show();
          $(this).parent().find('#side-2').hide();
        } else {
          $(this).parent().find('#side-1').hide();
          $(this).parent().find('#side-2').show();
        }
      }

      /*
       * .once FLIPPER
       */
      $('.node', context).once(function() {

        $(this).find('#flip-content').each(function() {
          var $node = $(this);
          if ($(this).parents('.node').hasClass('node-issue')) {
            var $el = $(this);
            var $front = $el.children('#side-1');
            var fH = $front.find('img').attr('height');
            var $back = $el.children('#side-2');
            // if present, move highlight el into side 1
            $front.prepend($node.children('.field-name-field-display-extra-ref'));
            if (
              $back.find('a').text() == '____missing_content____'
            && $node.parents('.sb-1').length == 0
              ) {
              var title = $node.find('.field-name-title').text();
              $back.remove();

//              $back.find('a').text('Check it out!');
            } else  {
              $back.css({
                'height': fH
              }).hide();
              $node.hover(function () {
                  $el.find('div').stop().rotate3Di('flip', 250, {direction: 'clockwise', sideChange: mySideChange});
                  $el.parents('.node').find('.field-name-field-display-extra-ref').fadeOut(100);
                },
                function () {
                  function complete() {
                    $el.parents('.node').find('.field-name-field-display-extra-ref').fadeIn(300);
                  }
                  $el.find('div').stop().rotate3Di(
                    'unflip',
                    500, {
                      sideChange: mySideChange,
                      complete : complete
                    }
                  );

                });
              // wrap side 2 w/ link
              $back.wrap($back.children('a').clone().html(''));

            }

          }

          // EXCEPTIONS - no flip for editions
          if ($(this).parents('.node').hasClass('node-edition')) {
            $(this).find('#side-2').remove();
          }

        });

      });

      $('.sb-1', context).once(function() {
        // EXCEPTIONS - sidebar element, different display
//        console.log("sidebarrring it")
        var node = $(this).find('.view-display-id-block_2 .node')
        var side1 = node.find('#side-1');
        var side2 = node.find('#side-2');
        var title = node.find('.field-name-title a');
        node.find('.field-name-field-display-extra-ref').remove();

        side2.find('a').html('<div>Issue</div><div>' + title.text() + '</div>');
        title.parents('.field-name-title').remove();
        side2.find('a').append(node.find('.field-name-field-product'));
        var h = side1.outerHeight();
        side2.css({
//          'z-index': 999,
          'height': 181 + 'px'
//          'height': side1.outerHeight()
        })
      });





      function getHeights(rows) {
        var h = [];
        $.each(rows, function(){
          h.push(this.height() + 143);
        });
        return h;
      }

      /* MAKE SLIDER OBJECT */
      function Vticker(rows) {
        this.index = 0;
        this.rows = rows;
        this.heights = getHeights(this.rows);
      }

      //////////////////////////////
      // Debounce
      // Returns a function, that, as long as it continues to be invoked, will not be triggered. The function will be called after it stops being called for N milliseconds. If `immediate` is passed, trigger the function on the leading edge, instead of the trailing.
      //////////////////////////////
      function debounce(func, wait, immediate) {
        var timeout;
        return function() {
          var context = this, args = arguments;
          var later = function() {
            timeout = null;
            if (!immediate) func.apply(context, args);
          }
          var callNow = immediate && !timeout;
          clearTimeout(timeout);
          timeout = setTimeout(later, wait);
          if (callNow) func.apply(context, args);
        }
      }


      /* VERTICAL SCROLLING */
//      var updateOrder = false; // after first run, set true
//      var timeout = 10000;
      var timeout = 2500;
      var interval;

      $('.view-content').on('set-vertical-ticker', function(event, ticker) {
        var $wrapper = $(this);
        /* master counter for zero reset, zero based */
//        var count = ticker.rows.length; count--;
        $wrapper.children().css({
          'z-index': 1
        })

        /* looping function */
        var scroll = function() {

          /* hover control */
          if(hover) {
//            console.log("i'm hovering bro!");
            return;
          }

          // do rotation if content is in view
            var scrT = $(document).scrollTop();
            var wrapTop = $wrapper.offset().top - scrT - ($(window).height() / 2);
            var $nav = $('#navbar');
            var borderTop = $nav.offset().top + $nav.height() - scrT ;
//            console.log(borderTop)
          if (
            $wrapper.parents('.tab-pane').hasClass('active')
            && wrapTop <= 273
            ) {

            var $top = $wrapper.children().first();
            $wrapper.append($top.clone().css({
              'z-index': 1
            }));
            $top.css({
              'z-index': 0,
              'overflow': 'hidden'
            });
            var adj = 3;
            var delay = 2000;
            /*
            @TODO | return to this point to improve 'tiled' rotation
             */
            $top.children().animate({
//              'transform': 'scale(0.1, 0.1)'
//              'padding-top' : 23,
//              'padding-left' : 25,
//              'backgroundColor' : 'rgba(204, 204, 204, 0.5)'
            }, 100, 'swing', function() {
              $top.animate({
                'height': 0
              }, 600, 'swing', function() {
                $top.remove();
              });
            })

          }


          /* PAUSE ON HOVER */
          $wrapper.hover(
            function() { hover = true; },
            function() {
              clearInterval(interval);
              interval = setTimeout(scroll, timeout);
              hover = false;
//              console.log("done hovering");
            }
          );
          interval = setTimeout(scroll, timeout);


        };
        /* end looping function */
        /* */
        interval = setTimeout(scroll, timeout);



      });


      $('body',context).once(function() {
        var $feed = $(this).find('.view-spike-recent-content');
        var $hot = $(this).find('.view-spike-hot-content');
        //      $('.view-spike-recent-content',context).once(function() {

        /*
        set up
        test modify view so it scrolls right
         */
        var $clone = $hot.children('.view-content');
        var $row = $clone.children().first();
        var $feedEdit = $feed.children('.view-content');
//        console.log('feed size start ' + $feed.find('.views-row').size());

        /* normal scroll setup */
        $feed.each(function() {
          var rows = []
          $(this).find('.views-row').each(function() {
            var $row_orig = $(this);

            /*
            replace values in cloned content and modify view
             */
//            var $clone_row = $row.clone();


            rows.push($(this));
//            rows.push($clone_row);

          });
          var ticker = new Vticker(rows);//
          $(this).addClass('vertical-ticker');
          $(this).children('.view-content').trigger('set-vertical-ticker', [ticker]);
//          scrollResize($(this).children('.view-content'), ticker);
        });


        $hot.each(function() {
          var rows = []
          $(this).find('.views-row').each(function() {
            rows.push($(this));
          });
          var ticker = new Vticker(rows);//
          $(this).addClass('vertical-ticker');
          $(this).children('.view-content').trigger('set-vertical-ticker', [ticker]);
//          scrollResize($(this).children('.view-content'), ticker);
        });


//        $(this).trigger('set-vertical-ticker', [ticker]);
      });

      $('.view-spike-hot-content',context).once(function() {
//        var rows = []
//        $(this).find('.views-row').each(function() {
//          rows.push($(this));
//        });
//        var ticker = new Vticker(rows);//
//        $(this).addClass('vertical-ticker');
//        $(this).children('.view-content').trigger('set-vertical-ticker', [ticker]);
      });

    }
  };
})(jQuery);