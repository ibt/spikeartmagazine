(function ($) {

  Drupal.behaviors.spikeTiles = {

    attach: function (context, settings) {

      function initTiler($el, rowWidth, gutter) {
        $el.imagesLoaded(function () {
          $el.masonry({
            columnWidth: rowWidth,
            gutterWidth: gutter,
            isFitWidth: true
          });
          $el.children().removeClass('loading');
        });
      }



      /*
      if viewport is large enough, init masonry
       */
      // if ($(window).width() > 414) {
      $('.tile-set-wrapper', context).once(function () {
        var cleanUp = $(this).parent().siblings('.tile-break-wrapper');
        cleanUp.insertAfter($(this)); // this is done on first load, autoscroll correctly places from there
        initTiler($(this), 400, 60);
      });
      $('.view-spike-term-pages .view-content', context).once(function () {
        initTiler($(this), 400, 60);
      });
      $('.view-spike-search.view-display-id-page_1 .view-content', context).once(function () {
        initTiler($(this), 400, 60);
      });
      $('.view-search.view-display-id-page .view-content', context).once(function () {
        initTiler($(this), 400, 60);
      });
      // }



    }
  };
})(jQuery);
