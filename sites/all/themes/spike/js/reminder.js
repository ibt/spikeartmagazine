(function($) {

  Drupal.behaviors.reminderThemeElement = {

    attach: function (context, settings) {

      /*
       * .once FLIPPER
       */

      // initial positioning
      $('html', context).once(function() {
        var $mascott = $('.mascott-scroll-down');
        $(this).append($mascott);
        var w = $(window).width() / 2;
        $mascott.hide().css({
          left: w,
          position: 'absolute',
          'z-index': 1000
        });
      });

      $('.mascott-scroll-down', context).once(function() {

        var $el = $(this);

        var remind = function() {
          var testPos = $('.main-container').height() / 3;
          var viewTop = $(window).scrollTop();
          var asdf;
          if (viewTop < testPos) {
            var top = $(window).scrollTop();
            var bottom = $(window).scrollTop() + $(window).outerHeight() - 200;
            $el.show().css({ top: top - 200 });
            $el.animate({ top: bottom }, '100');
          }
        };

        setTimeout(function() {
          remind();
        }, 3000);

        $(window).scroll(function() {
          $el.fadeOut(100);
          /* REMINDER MOVE TRIGGER LOOP */
//          setInterval(remind, 10000);
//          remind();
        });


      });

    }
  };
})(jQuery);