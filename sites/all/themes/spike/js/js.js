/*
custom jquery command for contains w/o case sensitivity
 */
jQuery.expr[":"].icontains = function (a, i, m) {
  return (
    jQuery(a)
    .text()
    .toUpperCase()
    .indexOf(m[3].toUpperCase()) >= 0
  );
};

(function ($) {
  Drupal.behaviors.spikeTheme = {
    attach: function (context, settings) {
      /**
       * prevent the word 'advertise' from appearing in markup
       */
      //      $('body', context).once(function() {
      //        var str = 'advertise-link';
      //        var rpl = 'replace-link'
      //        $(this).find('.'+str).each(function() {
      //          $(this).removeClass(str).addClass(rpl);
      //        })
      //      });

      /**
       * link file/download section to mention in text
       * - for info pages / "media files"
       */
      $(".node-type-page", context).once(function () {
        var sfFiles = $(this).find(".sticky-footer .field-type-file");
        var body = $(this).find(".panel-pane p:icontains(DOWNLOADS)");
        var until = body
          .nextAll("p")
          .find("strong")
          .parent();
        var sel = "dl-link";
        var asdf;
        if (sfFiles.length > 0 && body.length > 0) {
          body.nextUntil(until).addClass(sel);
          $("." + sel).click(function () {
            $("html, body").animate({
                scrollTop: $(".sticky-footer-seperator").offset().top
              },
              1000
            );
          });
          //          console.log('attempted syncing "download" text with dl files in footer');
        }
      });

      /**
       * insert watermark
       */
      $(".view-spike-front-page .view-content", context).once(function () {
        var $append = $(
          '<div id="bg-insert"><img src="/sites/all/themes/spike/images/spike_wasserzeichen3.gif"></div>'
        );
        $(this).append(
          $append.css({
            position: "fixed",
            top: "50%",
            left: "50%",
            width: "600px",
            height: "530px",
            margin: "-265px 0 0 -300px",
            "z-index": "-3"
          })
        );
        var $main = $(".col-sm-11");
        var mainTop = $main.position().top;

        if (mainTop <= $(window).scrollTop()) {
          // if (mainTop <= $(window).scrollTop() + 210) {
          $append.show();
        } else {
          $append.hide();
        }

        $(window).scroll(function () {
          if (mainTop <= $(window).scrollTop()) {
            if ($append.is(":hidden")) {
              $append.fadeIn(100);
            }
          } else {
            if ($append.is(":visible")) {
              $append.fadeOut(100);
            }
          }
        });
      });

      /**
       * main menu background toggle
       */
      $("#home-link", context).once(function () {
        if ($(window).width() > 414) {
          $(this).hover(
            function () {
              $(".toggle-img").animate({
                  "margin-left": "-=60"
                },
                75
              );
            },
            function () {
              $(".toggle-img").animate({
                  "margin-left": "+=60"
                },
                75
              );
            }
          );
        }
      });

      /**
       * CREATE MENU 3-BAR ELEMENT
       */
      $("#toggle-el", context).once(function () {
        for (var i = 0; i < 3; i++) {
          var box = $('<div class="box">');
          $(this).append(box);
        }
      });

      /**
       *
       */
      $(".node-teaser", context).once(function () {
        /*
         items w/ products as teaser
         hide the buy button
         only show on hover
         */
        var $el = $(this).find(".field-name-field-product");
        // only in the main section
        // don't do this if we're in the sidebar
        if ($el.parents("#block-system-main").length) {
          $el.css({
            opacity: 0
          });
          var $par = $(this);
          $par.hover(
            function () {
              $el.animate({
                opacity: 1
              });
            },
            function () {
              $el.animate({
                opacity: 0
              });
            }
          );
        }
      });

      /*
      messages
       */
      $("body", context).once(function () {
        //        var $msg = $(this).find('.messages');
        var $remove = $(this).children(".placeholder");
        var txt = $remove.text();
        $("<span>" + txt + "</span>").insertAfter($remove);
        $remove.remove();
      });

      /*
      FORCE copy over correct trans-set link from block which is hidden...
      kinda hacky
       */
      $(
        ".field-name-corr-article-language-version, #block-spike-issue-spike-issue-sidebar",
        context
      ).once(function () {
        // console.log('lang hack')
        var $field = $(this);
        var text = $field.find("a").text();
        var targetLang = text == "Read in English" ? "en" : "de";
        var pathArray = window.location.pathname.split("/");
        var lang = pathArray[1];
        var link = $("#block-spike-lang-language-control--2 ." + targetLang)
          .find("a")
          .attr("href");

        if (link && link.length > 0) {
          $field
            .find("a")
            .addClass(targetLang)
            .attr("href", link);
        }
        //        $field.find('a').
        //
        //          (link);
        //        console.log(link)
        //        $.each(links, function() {
        //          var array = {
        //            link : this.find('a')
        //          }
        //        });
        var aasdf;
      });

      /*
      alterations to the cart
       */
      $("#block-views-commerce-cart-block-block-1", context).once(function () {
        $(this)
          .find(".cart-link")
          .append($(this).find(".line-item-quantity-raw"));
      });

      /*
      alterations to subjects / archive page
       */
      $(".page-subjects", context).once(function () {
        var elm = $(".view-spike-term-titles .views-exposed-form input");
        elm.attr("placeholder", "search");
        elm.focus(function (event) {
          event.preventDefault();
          if (elm.attr("placeholder", "search")) {
            elm.attr("placeholder", "");
          }
        });
        elm.focusout(function (event) {
          event.preventDefault();
          if (elm.attr() == "") {
            elm.attr("placeholder", "search");
          }
        });
      });

      /**
      alter search input forms on search page
       */
      $(".page-search", context).once(function () {
        /* remove 'no results' if search is empty, and enter info text in input */
        var search = $(
          "#page-content #views-exposed-form-search-page .views-exposed-form input"
        );
        var pathArray = window.location.pathname.split("/");
        var lang = pathArray[1];
        var input = {
          en: "Enter search terms",
          de: "Suchanfragen eingeben"
        };
        if (!search.attr("placeholder")) {
          search.attr("placeholder", input[lang]);
        }
        var input = getQueryVariable("input");

        function getQueryVariable(variable) {
          var query = window.location.search.substring(1);
          var vars = query.split("&");
          for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split("=");
            if (pair[0] == variable) {
              return pair[1];
            }
          }
        }
        //        if (input.length == 0) $('.view-search .view-empty').remove();
        if ($(".view-search .view-content").length == 0) {
          var selectors =
            ".views-widget-filter-type, .views-widget-sort-by, .views-widget-sort-order, .views-submit-button, .views-widget-filter-search_api_language";
          $(selectors).hide();
        }
        // remove date field from issue & edition rows
        $.each($(this).find(".node-issue", ".node-edition"), function () {
          $(this)
            .parents(".views-field")
            .siblings(".views-field-created")
            .remove();
        });
      });

      /*
      alter search forms inputs
       */
      $("html", context).once(function () {
        var dev = 1;
        if (dev == 1) return;
        var pathArray = window.location.pathname.split("/");
        var lang = pathArray[1];
        //        var ph = { en: 'Search', de: 'Suche' };
        $(this)
          .find("#views-exposed-form-search-page input")
          .each(function () {
            var input = $(this);
            var ph = {};
            if (input.parents("#page-content").length > 0) {
              ph = {
                en: "Enter search terms",
                de: "Suchanfrage eingeben"
              };
            } else {
              ph = {
                en: "Search",
                de: "Suche"
              };
            }

            if (input.parents("#page-content").length == 0) {
              input.attr("value", "");
              input.attr("placeholder", ph[lang]);
            }

            //          var input = $('#page-content #views-exposed-form-search-page .views-exposed-form input');
            /* remove placeholder when focus on input */
            var val = input.attr("value");
            input.focus(function () {
              input.addClass("in-focus");
              input.attr("placeholder", "");
              input.attr("value", "");
            });
            /* replace default input on unfocus if input is empty */
            input.focusout(function () {
              input.removeClass("in-focus");
              if (!input.attr("placeholder")) {
                if (val) {
                  input.attr("value", val);
                } else {
                  input.attr("placeholder", ph[lang]);
                }
              }
            });
          });
      });

      /*
      give highlight / display extra elements a link to their parent
       */
      $(".node.view-mode-teaser", context).once(function () {
        var href = $(this)
          .find(".field-name-field-image a")
          .attr("href");
        $(this)
          .find(".field-name-field-display-extra-ref img")
          .each(function () {
            $(this).wrap('<a href="' + href + '"></a>');
          });
      });

      /*
      add css class to iframe container
      ???? WHY ????
       */
      $(".node-type-article", context).once(function () {
        $(this)
          .find("iframe")
          .each(function () {
            var frame = $(this);
            if (frame.parent().is("p")) {
              frame.parent().addClass("breakout-el");
            } else {
              frame.wrap('<p class="breakout-el"></p>');
            }
          });
      });

      $("body", context).once(function () {

        /*
         move cart to sidebar toggle
         */
        $("#bar-toggle").append($(".view-commerce-cart-block"));

        /*
        set bg & spike colors
         */
        //+ Jonas Raoni Soares Silva
        //@ http://jsfromhell.com/array/shuffle [v1.0]
        function shuffle(o) {
          //v1.0
          for (
            var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x
          );
          return o;
        }
        var colors = ["#0F0FFF", "#FFFF96", "#1EFF00", "#FF5C99"];
        shuffle(colors);
        // sticky footer bg
        var col = colors[1];
        var $sf = $(".sticky-footer-loadmore");
        $sf
          .css({
            background: colors[1]
          })
          .addClass(col.replace("#", "color-code-"));

        $sf.prepend('<div class="sticky-bg-cover"></div>');

        // image article bg
        $(".field-name-field-article-ref-issue")
          .parents(".node-type-issue")
          .addClass("spike-col-" + colors[0].substr(1, 6));

        var count = $(".tile-breakpoint .field-name-blurb-link").length;
        $(this)
          .find(".tile-breakpoint .field-name-blurb-link")
          .each(function (i, e) {
            if (count > colors.length) {
              //            shuffle(colors);
              var dif = count - colors.length;
              while (dif > 0) {
                colors.forEach(function (e) {
                  colors.push(e);
                  dif--;
                });
              }
            }
            if (
              !$(this)
              .find(".field-item")
              .hasClass("quo-bg-img")
            ) {
              $(this).css({
                "background-color": colors[i]
              });
            }
          });

        /*
        image flipper cursor
         '.field-name-flip-book'
         */
        $(".image-flipper", context).once(function () {
          console.log('findMe')
          var mouse = {
            x: -1,
            y: -1
          };
          var pWidth = $(window).innerWidth(); //use .outerWidth() if you want borders
          var cursor;
          var bgContent;
          $(document).mousemove(function (e) {
            mouse.x = e.pageX;
            mouse.y = e.pageY;
            pagerCursorEval();
          });

          $(this).click(function () {
            (cursor = "default"), (bgContent = "");
            //            $(document).mousemove(function(e) {
            //              mouse.x = e.pageX;
            //              mouse.y = e.pageY;
            //              pagerCursorEval();
            //            });
          });

          function pagerCursorEval() {

            if (pWidth / 2 <= mouse.x) {
              $(".image-flipper").addClass('image-flipper--pos-right')
              $(".image-flipper").removeClass('image-flipper--pos-left')
              // cursor = "url(/sites/all/themes/spike/images/spike-l.png), auto";
              //              bgContent = '/sites/all/themes/spike/images/spike-l-w.png';
              // if ($(".image-flipper").turn("page") === 1)
              //   (cursor = "default"), (bgContent = "");
            } else {
              $(".image-flipper").addClass('image-flipper--pos-left')
              $(".image-flipper").removeClass('image-flipper--pos-right')

              // cursor = "url(/sites/all/themes/spike/images/spike-r.png), auto";
              //              bgContent = '/sites/all/themes/spike/images/spike-r-w.png';
              // if (
              //   $(".image-flipper").turn("page") ===
              //   $(".image-flipper").turn("pages")
              // )
              // (cursor =
              //   "url(/sites/all/themes/spike/images/smiley.png), auto"),
              //   (bgContent = "");
            }

          }
        });

        /*
        article language switch
        hard copy over the language switcher link values into the article
        language switcher field which was unsuccessfully programmed in spike_article
         */
        $("body.node-type-article", context).once(function () {
          var $langBlock = $("#block-spike-lang-language-control--2");
          var $link = $langBlock
            .find("li")
            .not(".active")
            .children();
          var $replace = $(".field-name-corr-article-language-version a");
          $replace.attr("href", $link.attr("href"));
          $langBlock.remove();
        });

        /*
        add sponsored article markup to rigth col
         */
        $("body.sponsored-article", context).once(function () {
          $(".region-sidebar-second").prepend(
            '<div id="block-part-tag">Sponsored</div>'
          );
        });

        // remove title attributes
        $("[title]").removeAttr("title");

        /*
        links in body text should always open in a new tab
         */
        $(".page-node #page-content .node .field-name-body .field-item a").attr(
          "target",
          "_blank"
        );

        /*
         * @TODO | make sure this runs last, or the positioning will be off
         */
        $(this).css({
          opacity: 0
        });
      });
    }
  };
})(jQuery);
