(function ($) {

  var script = {

    flaggedClass: 'overview--closed-by-user',

    attach: function (context, settings) {

      script.overlay = $('.node-overlay', context).once(script.initOverlay);
      script.wrapper = script.overlay.children('.field-group-div', context);
      script.wrapper.once(script.init)
      script.close = script.overlay.find('.flag-overlay a', context);
      script.close.click(script.updateClosed);

      $(document).ajaxComplete(function (event, xhr, settings) {
        script.updateClosed();
      });

    },

    init: function () {
      var item = $(this);
      item.hover(function () {
        item.addClass('has-hover');
      }, function () {
        item.removeClass('has-hover');
      });
      if (script.checkOverlay()) {
        $('.flag-overlay a.unflag-action').click();
        $(document).find('.node-overlay').removeClass(script.flaggedClass);
      } else {
        $(document).find('.node-overlay').addClass(script.flaggedClass)
      }
      script.fit();
    },

    isHorizontal: function () {
      return $(window).outerHeight() < $(window).outerWidth();
    },

    fit: function () {
      var display = script.overlay.find('.group-wrapper');
      var image = script.overlay.find('.field-name-field-image-overlay img');
      var imageMobile = script.overlay.find('.field-name-field-image-overlay-aux img');
      var link = image.parents('a');
      var src = script.isHorizontal() ? image.attr('src') : imageMobile.attr('src');
      image.hide();
      link.css({
        'background-image': 'url(' + src + ')',
      }).addClass('image-link');
      display.append(link);
      script.isHorizontal();
    },

    checkOverlay: function () {
      return (
        !document.referrer.includes(window.location.hostname) === true ||
        document.referrer === ''
      )
    },

    updateClosed: function () {
      var close = $(document).find('.flag-overlay a');
      if (close.hasClass('unflag-action')) {
        $(document).find('.node-overlay').addClass(script.flaggedClass)
      }
    }

  };

  Drupal.behaviors.spikeOverlay = script;

})(jQuery);
