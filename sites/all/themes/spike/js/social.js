
(function($) {

  Drupal.behaviors.spikeSocial = {

    attach: function (context, settings) {

      /*
      duplicate sharing block into more parts of the page
       */
      $('.page-node', context).once(function(){
        var page = $(this);
        var share = $('#block-spike-layout-spike-share');
        var clone = page.find('.share-clone');

        $(window).scroll(function() {
          var win = $(window);
          var scrT = win.scrollTop();
          var winH = win.height();
          if (
            share.length > 0
            && clone.length > 0
            ) {
            var cloneOffset = clone.offset().top;
            var shareSidebar = $('.sb- #block-spike-layout-spike-share')
            if (
              cloneOffset < (scrT + winH)
              )
            {
              shareSidebar.fadeOut();
//              shareSidebar.addClass('hidden');
            } else {
              shareSidebar.fadeIn();
//              shareSidebar.removeClass('hidden');
            }

          }

        });

      });




    }
  };
})(jQuery);