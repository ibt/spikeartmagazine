(function($) {

  Drupal.behaviors.spikeThemeEnd = {

    attach: function (context, settings) {

      /*
      ANIMATE FADE IN OF WHOLE PAGE
      RUN THIS JS LAST
       */


      $('body', context).once(function() {
        $(this).animate({
          opacity: 1
        });
      });
    }
  };
})(jQuery);