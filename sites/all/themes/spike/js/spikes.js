(function($) {

  Drupal.behaviors.spikes = {

    attach: function (context, settings) {

      /* SPIKES
       */
      var tW = 25;
      var tH = 22;

      // generic points for svg spike
//      var points = '12.887,0.08 0,25 25.073,25';
//      var p1 = tW / 2 + ',0 ';
//      var p2 = '0,' + tH + ' ';
//      var p3 = tW + ',' + tH;



      $('.view-spike-front-page').on('frontTopTeeth', function() {
        var $insert = '<div class="front-top-spikes"></div><div id="front-top-shadow"></div><div id="front-top-after"></div>';
        $(this).parents('#bottom-region').prepend($insert);
//        $insert.insertBefore('#navbar');
        var $spikeEl = $('.front-top-spikes');
        $spikeEl.css({
          width: '100%',
          height: 25,
          position: 'relative'
        });
      });
//
//

      /*
      SPIKES FOR TOP OF CONTENT AREA TO PUSH OUT INITIAL SPLASH (RIGHT?)
       */
      $('body').on('topSpikePosTrigger', function() {
        var $el = $(this).find('.front-top-spikes')
        if ($el.length > 0) {
  //        var $el = $(this);
          var pos = $('.col-sm-11').offset();
          var offs = 83;
  //        var offs = 84;
          $el.css({
            top: pos.top - offs,
            position: 'absolute'
          });
          $('#front-top-shadow').css({
            top: pos.top - offs,
            position: 'absolute'
          });
          $('#front-top-after').css({
            top: pos.top - offs,
            position: 'absolute'
          });

        }
      });

//      /*
//      MINI SPIKES FOR EVERY PAGE W/ TOGGLE DOWN
//       */
//      $('#navbar', context).once(function() {
//        var $el = $(this).clone();
////        $(this).parent().prepend($(this).clone().addClass('mini-spikes'));
//      });


      /*
      BODY ONCE
       */
      $('body', context).once(function() {
//        var $el = $('.view-spike-front-page');

        // trigger teeth insert
        $('.view-spike-front-page').trigger('frontTopTeeth');
        $('.view-spike-term-pages').trigger('frontTopTeeth');
//
//        // trigger positioning
        $('body').trigger('topSpikePosTrigger');
//        var asdf;




      });



    }
  };
})(jQuery);