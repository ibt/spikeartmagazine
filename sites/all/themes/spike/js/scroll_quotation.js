(function($) {

  Drupal.behaviors.scrollQuotations = {

    attach: function (context, settings) {

      var brkPntSel = '.tile-break-wrapper';
      var bgImgSel = '.bp-image';
      var bgWrap = '.quo-bg-img';
      var lastScrollTop = 0;

      var views = [$('.view-spike-front-page')];


      /*
       SCROLL ACTIONS
       */
      function breakPointScroll($el) {
        if ($el.size() < 1) return;


        /* check position relative to viewport */
        var winTop = $(window).scrollTop();
        var winBot = $(window).scrollTop() + $(window).height() - 10;
        var elTop = $el.offset().top;
        var elBot = $el.offset().top + $el.outerHeight();
        var xMargin = $el.attr('x-margin');
        var yMargin = $el.attr('y-margin');
        var newH = $el.attr('new-h');
        var oldH = $el.attr('old-h');

        if ( /* element is below view */
          elTop > winTop // below winTop
            && elBot > winTop
            && elTop > winBot // below winBot
            && elBot > winBot
          ) {
          $el.css({
            'background-position': xMargin / 2 +'px 0px'
          });
        }
        if ( /* element is above view */
          elTop < winTop // below winTop
            && elBot < winTop
            && elTop < winBot // below winBot
            && elBot < winBot
          ) {
          $el.css({
            'background-position': xMargin / 2 +'px ' + yMargin + 'px'
          });
        }
        if
          (
          ( /* element is partially in view */
            ( // el straddles bottom
              elTop < winBot
                && elBot > winBot
              )
              || ( // el straddles top
              elTop < winTop
                && elBot > winTop
              )
            )
            || ( /* element is completely in view */
            elTop > winTop // below winTop
              && elBot < winBot // above winBot
            )
          )
        {
          /* Paralax image scrolling action */
          $el.addClass('paralax');
          var activeMargin = elTop - winBot;
          // test if there's enough image to keep scrolling
          var shift = newH - $el.outerHeight();
          if ( (activeMargin * -1) >= shift) {
//              console.log('no more image');
            $el.css({
              'background-position': xMargin / 2 +'px ' + (shift * -1) + 'px'
            });
          } else {
//              console.log('still shifting');
            $el.css({
              'background-position': xMargin / 2 +'px ' + (activeMargin * 0.7) + 'px'
            });
          }
        }

      }

      /*
      init breakpoint pre-arrange
      set image as background of
       */
      $(brkPntSel).on('breakPointInit', function(event, cur) {
        var $tile = $(this);
        var $img = $tile.find(bgImgSel + ' img');
        var imgPath = $img.attr('src');
        if (typeof(imgPath) != "undefined" && imgPath !== null) {

          var imgH = $img.outerHeight();
          var imgW = $img.outerWidth();
          $tile.find(bgImgSel).remove();
          var $blrb = $tile.find('.field-name-blurb-link');
          $blrb.css({
            'background-color' : 'white'
          });
          $blrb.find('.field-item').css({
            'background-image': 'url(' + imgPath + ')'
          }).addClass('quo-bg-img').attr('bg-img-width', imgW).attr('bg-img-height', imgH);
          var asfd;
        }
      });

      /*
      set background position based on breakpoints position relative to viewport
       */
      $(brkPntSel).on('breakPointBGresize', function(){

        var $el = $(this).find(bgWrap);
        //        if (typeof($el) != "undefined" && $el !== null) { return ; }

        // element sizes
        var breakSize = [$(window).width() - 180, $el.outerHeight()];
        //        var breakSize = [$el.outerWidth(), $el.outerHeight()];

        var imgSize = [$el.attr('bg-img-width'), $el.attr('bg-img-height')];
        var margin = $(window).height() - breakSize[1];

        // new sizes
        var newHeight = $(window).height() * 2;
        //        var newHeight = $(window).height() + (margin * 4);
        var height = imgSize[1];
        var proportion = newHeight/height;
        var newWidth = imgSize[0] * proportion;

        // check sizes to cover bp element
        if (newWidth < breakSize[0]) {
          newWidth = breakSize[0];
          proportion = newWidth/imgSize[0];
          newHeight = imgSize[1] * proportion;
          var asdf;
        }

        // adjuster margins
        var xMargin = breakSize[0] - newWidth;
        var yMargin = breakSize[1] - newHeight;
        $el.attr('x-margin', xMargin);
        $el.attr('y-margin', yMargin);
        $el.attr('new-h', newHeight);
        $el.attr('old-h', height);

        $el.css({
          'background-size': newWidth+'px '+ newHeight + 'px',
          'background-position': xMargin / 2 +'px 0px'
        });


      });




      /*
      trigger body actions
      * - separated for ajax reloading
       */
      $('body').on('triggerInit', function() {
        $.each(views, function() {
          // init breakpoint style
          var $breaks = $(this).find(brkPntSel);
          $breaks.each(function(e) {
            $(this).trigger('breakPointInit', e);
          });
        });
      });

      function scrollInit($bps) {
        $(window).scroll( $.throttle( 3, false, function() {
          $bps.each(function() {
            breakPointScroll($(this));
          });
        }));
      }

      /*
      main body once
       */
      $('body',context).once(function() {
        $(brkPntSel).imagesLoaded( function() {
//          console.log('page load images success')
          $('body').trigger('triggerInit');
          $(brkPntSel).trigger('breakPointBGresize');
          scrollInit($(brkPntSel).find(bgWrap));
        });

        $(document).ajaxComplete(function() {
          $(brkPntSel).imagesLoaded( function() {
//            console.log('ajax load images success')
            $('body').trigger('triggerInit');
            $(brkPntSel).trigger('breakPointBGresize');
            scrollInit($(brkPntSel).find(bgWrap));

          });
        });

        $(window).resize(function() {
          $(brkPntSel).on('breakPointBGresize');
        });

      });



    }
  };
})(jQuery);