(function($) {

  Drupal.behaviors.spikeSidebarToggle = {

    attach: function (context, settings) {

      var $main = $('.main-container');
//      var $mainInner = $main.find('#block-system-main');
      var $mainInner = $main.find('#page-content');
//      var $footer = $('.footer');
      var $side1 = $('.region-sidebar-first').parent('.sb-');
      var $side2 = $('.region-sidebar-second').parent('.sb-');
      var $lang = $('#block-spike-lang-language-control--2');

//      var $side1 = $('.region-sidebar-first').parent('aside');
//      var $side2 = $('.region-sidebar-second').parent('aside');
//      var $side1 = $main.find('.region-sidebar-first').parent('aside');
//      var $side2 = $main.find('.region-sidebar-second').parent('aside');
      var $barToggle = $('#bar-toggle');
      var $navpills = $('#block-spike-layout-spike-feed-block .nav-pills');

      var active = false;

      /**
       * Sidebar Classes
       */
      $main.on('sideBarClasses', function(event, $onBar, $offBar) {
        $onBar.addClass('toggle-open').removeClass('toggle-closed');
        $onBar.next().addClass('toggle-open').removeClass('toggle-closed');
        $offBar.addClass('toggle-closed').removeClass('toggle-open');
        $offBar.next().addClass('toggle-closed').removeClass('toggle-open');
      });


      /*
      colorbox despiration
       */
//       $(document).on('cbox_open', function(){
//         $(document).on('cbox_closed', function(){
//           // temp fix
// //          location.reload();
//           console.log("cb off");
//         });
//       });



      /**
       * Sidebar Toggle
       */

      var duration = 75;
      var easing = 'easeInQuart';

      $main.on('sideBarToggle', function() {
        if (active == false) return;

//        $side1.children().fadeOut();
//        $side2.children().fadeOut();

        $('.sb-').toggleClass('toggle-open').toggleClass('toggle-closed');

        var slide = $barToggle.attr('slide');
        var asdf;

        /* SLIDE LEFT */
        if (slide == 'slide-left') {
//          $side2.children().hide();
          $side2.animate({
//            left: '+=180px',
            width: 'toggle',
            duration: duration,
            easing: 'easing'
          });

          $barToggle.attr('slide', 'slide-right');

          $mainInner.animate({
            left: '+=180px',
            duration: duration,
            easing: 'easing'
          });


          $side1.animate({
            width: 'toggle',
            duration: duration,
            easing: 'easing'
          });
          $lang.addClass('lang-toggle');

          $('.sb-1 .region-sidebar-first').css({
            left: -180
          }).animate({
            left: '+=180',
//            width: 'toggle',
            duration: duration,
            easing: 'easing'
          });


//          $side1.children().fadeIn(duration * 2);
        }

        /* SLIDE RIGHT */
        if (slide == 'slide-right') {
//          $navpills.fadeOut();
          $side1.animate({
            width: 'toggle',
            duration: duration,
            easing: 'easing'
          })
          $lang.removeClass('lang-toggle');

          $('.sb-1 .region-sidebar-first').css({}).animate({
            left: '-=180',
//            width: 'toggle',
            duration: duration,
            easing: 'easing'
          });

//          $('.region-sidebar-first').animate({
//            width: 'toggle',
//            duration: duration,
//            easing: 'easing'
//          });


          $barToggle.attr('slide', 'slide-left');

            $mainInner.animate({
              left: '-=180px',
              duration: duration,
              easing: 'easing'
            });
          if ($('body').hasClass('sidebar-standard')) {
          }



//          $side2.children().fadeOut();
          $side2.animate({
            width: 'toggle',
            duration: duration,
            easing: 'easing'
          });

//          $lang.animate({
//            left: '-=180',
////            width: 'toggle',
//            duration: duration,
//            easing: 'easing'
//          });


//          $side2.children().fadeIn(duration * 2);
        }
        $barToggle.trigger('barToggleToggle');
        active = false;
      });

      /**
       * Toggle button toggle
       * triggered by sideBarSetState
       */
      $barToggle.on('barToggleToggle', function(event, action) {
        if (active == false) return;
        var $el = $(this);
        if (action) {
          $el.attr('slide', action);
        } else {
          var slide = $el.attr('slide');
        }
        var asfd;
        if (slide == 'slide-left') {
          $el.animate({
            left: '-=180px',
            duration: duration,
            easing: 'easing'
          });
//          console.log('toggle to the left');
        }
        if (slide == 'slide-right') {
          $el.animate({
            left: '+=180px',
            duration: duration,
            easing: 'easing'
          });
//          console.log('toggle to the right');
        }
        active = false;
      });

      $main.on('defSide1', function() {
        $side1.show();
        $side2.hide();
        $main.trigger('sideBarClasses', [$side1, $side2]);
        $barToggle.attr('slide', 'slide-left');
      });
      /**
       * default state: Side 2
       * triggered by sideBarSetState
       */
      $main.on('defSide2', function() {
        $side2.show();
        $side1.hide();
        $main.trigger('sideBarClasses', [$side2, $side1]);
        $barToggle.attr('slide', 'slide-left');
      });





//      $main.on('setWidth', function(event, $el) {
//        var contW = $main.width();
//        var sideW = 180;
//        $main.find('#page-content').css({ width: contW - sideW});
//      });

      /**
       * Sidebar State
       */
      $main.on('sideBarSetState', function(event, state) {
        var h = $main.outerHeight();
        $(this).find('.sb-').each(function() {
          $(this).css({
            height: h
          });

        });



            /**
         * default state: Side 1
         */
        $main.trigger(state);
//        $main.trigger('setWidth', [$main]);
      });


      /**
       * Main Container Once
       */
      $('.main-container').on('main-on', function() {
        $main.trigger('sideBarSetState', ['defSide2']);
//        $(window).resize(function() {
//          $main.trigger('setWidth', [$main]);
//        });

      });
      $('.main-container', context).once(function() {
        $('.main-container').trigger('main-on');
      });

      $('body').on('body-on', function() {
        var $top = $('.col-sm-11');

        /*
        NOTE - DISABLED FOR TESTING
         */


        $(window).scroll(function(){
          var mainTop = $top.position().top;
          var scrT = $(window).scrollTop();
          if (
            $top.position().top > $(window).scrollTop()
              && $('.sb-1').hasClass('toggle-open')
            ) {
            var asdf;
            active = true;
            $main.trigger('sideBarToggle');
            if ($('body').hasClass('not-front')) {
//              $barToggle.animate({
//                left: 0
//              })
            }
          } else {
//            if ($('.sb-1').hasClass('toggle-closed')) {
//              $barToggle.css({
////                left: 0
//              })
//
//            }
          }
        });


      });
      $('body', context).once(function() {
        $('body').trigger('body-on');
      });


      $('#bar-toggle').on('bar-tog-on', function() {

//        $lang.addClass('lang-toggle');

        $(this).find('#toggle-el').click(function(e) {
          active = true;
          $main.trigger('sideBarToggle');
          if($('aside').hasClass('toggle-closed')){
              $('aside').css('z-index', '1000');

            }else{
              $('aside').css('z-index', '6');
            }
          //          $(this).trigger('barToggleToggle');

        });

        $(this).find('#toggle-el').hover(function() {

          $(this).children().each(function() {
            $(this).css({ background: 'white'});
          })
        }, function() {
          $(this).children().each(function() {
            $(this).css({ background: 'black'});
          })


        });

        // Function to launch sidebar when mouseover on toggle-item
        $(this).find('#toggle-el').mouseenter(function(){
          if($('aside').hasClass('toggle-closed')){
            active = true;
            $main.trigger('sideBarToggle');
            $('aside').css('z-index', '1000');

          }else{
            $('aside').css('z-index', '6');
          }

        });



        $(this).children('a').each(function() {
          var link = $(this).attr('href');
          $(this).attr('href', link + '#cool');
          $(this).append('<div class="toggle-img">SPIKE</div>')
          $(this).append('<div class="toggle-img toggle-img-2">HOME</div>')
//          $(this).append('<div class="toggle-img"><img src="/sites/all/themes/spike/images/spike-menu-toggle.png"></div>')
//          $(this).append('<div class="toggle-img toggle-img-2"><img src="/sites/all/themes/spike/images/home.png"></div>')
          //
        });
      });

      $('html', context).once(function() {
        $('#bar-toggle').trigger('bar-tog-on');

      });

//      $side1.hide();
//      $side2.hide();

    }
  };
})(jQuery);