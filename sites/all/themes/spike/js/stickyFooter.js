(function($) {

  Drupal.behaviors.stickyFooter = {

    attach: function (context, settings) {

      var $sf = $('.sticky-footer-loadmore');
      var $pageOut = $sf.siblings('.spike-banded-element');


      /**
      article footer pos
       */
      $sf.on('footerInit', function() {
        var stickyHeight = $(window).outerHeight() + 100;
        var clone = $sf.clone();
        clone.insertAfter($sf).addClass('footer-clone');
//        $pageOut.append('<div id="sticky-cover"></div><div id="sticky-shadow"></div>');
//        $pageOut.css({ marginBottom : stickyHeight });
      });

      /**
       * process for sticky visibility
       */
      $sf.on('sfVis', function(){
        var st = $(window).scrollTop();
        var wh = $(window).height();
        var elT = $pageOut.offset().top;
        var elH = $pageOut.height();
        if ((st + wh) > (elT + elH)) {
          // make sure we hide sidebars
          $('.sb-2').css({
            'visibility': 'hidden',
            'opacity': 0
          })
        } else {
          $('.sb-2').css({
            'visibility': 'visible',
            'opacity': 1
          })
        }
      });

      /**
       * process for sticky visibility
       */
      $('.sticky-footer').on('sfVis2', function(){
        var $pageOut = $('.spike-banded-element');
        var st = $(window).scrollTop();
        var wh = $(window).height();
        var elT = $pageOut.offset().top;
        var elH = $pageOut.height();
        if ((st + wh) > (elT + elH)) {
          // make sure we hide sidebars
          $('.sb-2').css({
            'visibility': 'hidden',
            'opacity': 0
          })
        } else {
          $('.sb-2').css({
            'visibility': 'visible',
            'opacity': 1
          })
        }
      });


      /**
      sticky footer pos
       */
      $sf.on('sfPos', function() {
        var sfH = $sf.height();
        var stickyHeight = $(window).outerHeight() + 100;
        // make sure content is at least as long as window,
        // so SF is covered on normal page load
        if ($pageOut.offset().top < $(window).height()) {
          var $body = $pageOut.prev().prev();
          $body.css({
            'height': $(window).height()
          });
        }
        $(this).siblings().each(function() {
          $(this).css({ position: 'relative', 'z-index': 4});
        });
        $('.footer-clone').css({ position: 'fixed', top: 0, 'z-index': 0});
        $sf.css({ position: 'relative', top: 0, 'z-index': 0});
//        $sf.css({ position: 'relative', top: stickyHeight * -1, 'z-index': -1});
      });


      /**
      sticky footer pos
       */
      $sf.on('sfScroll', function() {
        // if main content is out of view, set footer to free scroll
        $('#bg-insert').addClass('opacity-fade').addClass('opacity-hidden');;
        if (
          $pageOut.offset().top < $(window).scrollTop()
        && !$sf.hasClass('sf-processed')
          ) {
          $sf.addClass('sf-processed');
          $sf.css({'z-index' : 0 })
          $('.footer-clone').remove()

        }
        if ($pageOut.offset().top < $(window).scrollTop()) {
          $('#bg-insert').removeClass('opacity-hidden');
        } else {
          $('#bg-insert').addClass('opacity-hidden');
        }
      });


      /**
       * Main Container Once
       */
      $('.sticky-footer-loadmore', context).once(function() {

        // position sticky footer & other elements for proper scrolling
        var $sf = $(this);
        $sf.trigger('footerInit');
        $sf.trigger('sfPos');
        $sf.trigger('sfVis');
        $sf.trigger('sfScroll');

        $(window).scroll(function() {
          $sf.trigger('sfVis');
          $sf.trigger('sfScroll');
        });
        $(window).resize(function() {
          $sf.trigger('sfVis');
          $sf.trigger('sfPos');
        });


      });


      /**
       * Main Container Once
       */
      $('.sticky-footer', context).once(function() {
//        console.log('test')
        // position sticky footer & other elements for proper scrolling
        var $sf = $(this);
        $sf.trigger('sfVis2');

        $(window).scroll(function() {
          $sf.trigger('sfVis2');
        });
        $(window).resize(function() {
          $sf.trigger('sfVis2');
        });


      });

    }
  };
})(jQuery);
