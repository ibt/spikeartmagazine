(function ($) {
  var views_infinite_scroll_was_initialised = false;

  Drupal.behaviors.views_infinite_scroll = {
    attach: function (context, settings) {

      var pageIndex = 1;

      function scroll_action_post($el) {

        // var $last = $('.tile-break-wrapper').last();
        var $last = $el.find($('.view-content .scroll-unit').last());
        var list = $('#scroll-insert');
        var listItems = list.children();
        listItems.each(function (i) {
          $(this).attr('insert-unit', pageIndex)
          pageIndex++;
        });
        list.append(listItems.get().reverse());
        $('#scroll-insert').children().each(function (e) {
          $(this).insertAfter($last);
          if ($(this).hasClass('tile-set-wrapper')) {
            $(this).imagesLoaded(function () {
              $(this).masonry({
                columnWidth: 400,
                gutterWidth: 60,
                isFitWidth: true
              });
              $(this).children().removeClass('loading');
            });
          }
        });

        setBreakpointBGColor($el);
        $('[title]').removeAttr('title');
      }

      function setBreakpointBGColor($el) {
        var colors = ['#0F0FFF', '#FFFF96', '#1EFF00', '#FF5C99'];
        shuffle(colors);
        var count = $('.tile-breakpoint .field-name-blurb-link').length;
        $el.find('.tile-breakpoint .field-name-blurb-link').each(function (i, e) {
          if (count > colors.length) {
            var dif = count - colors.length;
            while (dif > 0) {
              colors.forEach(function (e) {
                colors.push(e);
                dif--;
              });
            }
          }
          if ($(this).prop('style').backgroundColor == '') {
            $(this).css({
              'background-color': colors[i],
              'opacity': 0
            });
            $(this).animate({
              'opacity': 1
            }), 200;
          }
        });
      }

      //+ Jonas Raoni Soares Silva
      //@ http://jsfromhell.com/array/shuffle [v1.0]
      function shuffle(o) { //v1.0
        for (var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
        return o;
      };




      // Make sure that autopager plugin is loaded
      if ($.autopager) {
        if (!views_infinite_scroll_was_initialised) {
          views_infinite_scroll_was_initialised = true;
          // There should not be multiple Infinite Scroll Views on the same page
          if (Drupal.settings.views_infinite_scroll.length == 1) {
            var settings = Drupal.settings.views_infinite_scroll[0];
            var use_ajax = false;

            if (!use_ajax) {

              var view_selector = 'div.view-id-' + settings.view_name + '.view-display-id-' + settings.display;
              var content_selector = view_selector + ' > ' + settings.content_selector;
              var items_selector = content_selector + ' ' + settings.items_selector;
              var pager_selector = view_selector + ' > div.item-list ' + settings.pager_selector;
              var next_selector = view_selector + ' ' + settings.next_selector;
              var img_path = settings.img_path;
              var img = '<div id="views_infinite_scroll-ajax-loader"><img src="' + img_path + '" alt="loading..."/></div>';

              $(pager_selector).hide();
              var handle = $.autopager({
                // append to scroll insert, we'll process the nodes here,
                // then move them into display wrapper
                appendTo: '#scroll-insert',
                content: '.scroll-unit',
                link: next_selector,
                page: 0,
                start: function () {
                  $(img).insertBefore('.pager');
                },
                load: function () {
                  Drupal.attachBehaviors(this);
                  $('div#views_infinite_scroll-ajax-loader').html('loaded').delay(200).remove();

                  scroll_action_post($(view_selector))
                }
              });
              if (view_selector != 'div.view-id-search.view-display-id-page') {

                // Trigger autoload if content height is less than doc height already
                var prev_content_height = $(content_selector).height();
                do {
                  var last = $(items_selector).filter(':last');
                  if (last.offset().top + last.height() <= $(document).scrollTop() + $(window).height()) {
                    last = $(items_selector).filter(':last');
                    handle.autopager('load');
                  } else {
                    break;
                  }
                }
                while ($(content_selector).height() > prev_content_height);
              }

            } else {
              alert(Drupal.t('Views infinite scroll pager is not compatible with Ajax Views. Please disable "Use Ajax" option.'));
            }
          } else if (Drupal.settings.views_infinite_scroll.length > 1) {
            alert(Drupal.t('Views Infinite Scroll module can\'t handle more than one infinite view in the same page.'));
          }
        }
      } else {
        alert(Drupal.t('Autopager jquery plugin in not loaded.'));
      }



    }
  }

})(jQuery);
