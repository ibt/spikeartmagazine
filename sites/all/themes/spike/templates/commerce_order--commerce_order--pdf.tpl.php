<?php

/**
 * @file
 * Template for invoiced orders.
 */
$subject = t('Order') . ' ' . t('No.') . ' ' . $commerce_order->order_number;
$content['order_number']['#markup'] = $subject;
$invoice_text = t('Thanks for your order!') . '<br>' . t('Please note that this is not an invoice. You will receive a bill seperately by mail.');
$invoice_text .= '<div class ="terms">' . t('The general terms of Spike Art Magazine OG apply: www.spikeartmagazine.com.') . '</div>';
$content['invoice_text']['#markup'] = $invoice_text;
$content['order_number']['#children'] = $subject;
$tt=1;

?>
<div class="invoice-invoiced">
  <div class="header">
    <table>
      <tr>
        <td>
          <img src="<?php print $content['invoice_logo']['#value']; ?>"/>
        </td>
        <td>
          <div id="addressee">
            <div class="customer-title"><?php print t('To'); ?></div>
            <div class="customer"><?php print render($content['commerce_customer_billing']); ?></div>
          </div>
        </td>
      </tr>
    </table>

    <div class="invoice-header">
<!--        <p>--><?php //print render($content['invoice_header']); ?><!--</p>-->
    </div>
  </div>

  <div class="invoice-header-date"><?php print render($content['invoice_header_date']); ?></div>

  <div class="invoice-number"><?php print render($content['order_number']); ?></div>
<!--  <div class="order-id">--><?php //print render($content['order_id']); ?><!--</div>-->

  <div class="line-items">
    <div class="line-items-view"><?php
      $line_items = render($content['commerce_line_items']);
      print render($content['commerce_line_items']);
      $t=1;

      ?></div>
    <div class="order-total"><?php
      print
        commerce_order_total_output_element($content['commerce_order_total'])
      ?></div>
<!--    <div class="order-total">--><?php //print render($content['commerce_order_total']); ?><!--</div>-->
  </div>
  <div class="invoice-text"><?php print render($content['invoice_text']); ?></div>

</div>

<?php



function commerce_order_total_output_element($order_total) {
  $order = $order_total['#object'];
  $wrapped = entity_metadata_wrapper('commerce_order', $order);
  $total = $wrapped->commerce_order_total->value();

//  $prefix = '<tr><td><div class="order-total-el">';
  $prefix = '<tr>';
  $title_el_prefix = '<td class="empty"></td><td><div class="el-inner title">';
  $el_prefix = '<div class="el-inner element">';
  $suffix = '</div>';
  $currency_code = $total['currency_code'];


  $totals = $total['data']['components'];
  $total_output_array = array();
  $header = array();
  $numItems = count($totals); $i = 1;
  foreach ($totals as $el) {
    switch ($el['name']) {
      case 'base_price' :
        $header = t('Subtotal');
        break;
      case 'tax|spike_tax_vat' :
        $rate = $el['price']['data']['tax_rate']['rate'] * 100;
        $header = '+'.$rate.' % ' . t('VAT');
        break;
      case 'shipping' :
        $header = t('Shipping');
        break;
    }
    $formatted_total = commerce_currency_format($el['price']['amount'], $currency_code);
    $formatted_total = str_replace('€', '', $formatted_total);
    $formatted_total = 'EUR ' . $formatted_total;
    $total_output_array[$el['name']] = array(
      '#prefix' => $prefix,
      '#suffix' => $suffix . '</td>',
      '#markup' => $title_el_prefix . $header . $suffix . '<td>' . $el_prefix .
        $formatted_total . $suffix
    );
    // after last element, add total
    if ($i == $numItems) {
      $grand_total = commerce_currency_format($total['amount'], $total['currency_code']);
      $total_output_array['total'] = array(
        '#prefix' => $prefix,
        '#suffix' => $suffix . '</td>',
        '#markup' => '<td class="empty"></td><td><div class="el-inner total-title">' . t('Total') . $suffix
          . '<td><div class="el-inner total-element">' .
          $grand_total . $suffix
      );
    }
    $i++;
  }
  $return = render($total_output_array);
  return '<div class="order-total-inner"><table class="total" cellspacing="0" cellpadding="0">' . $return . '</table></div>';
}