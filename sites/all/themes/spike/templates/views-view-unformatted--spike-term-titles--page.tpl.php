<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
include_once(drupal_get_path('theme', 'spike') . '/theme/spike_views_glossary.func.php');
$glossary = spike_views_glossary($view);
?>
<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>
<?php foreach ($glossary as $alpha): ?>
  <?php if (isset($alpha['keys']) && count($alpha['keys']) > 0) : ?>
  <div class="glossary-row">
    <div class="alpha" id="<?php print $alpha['alpha'] ?>"><?php print $alpha['alpha'] ?></div>
    <?php foreach ($alpha['keys'] as $key) : ?>
        <div<?php if ($classes_array[$key]) { print ' class="' . $classes_array[$key] .'"';  } ?>>
          <?php print $rows[$key]; ?>
        </div>
    <?php endforeach; ?>
  </div>
  <?php endif ?>
<?php endforeach; ?>