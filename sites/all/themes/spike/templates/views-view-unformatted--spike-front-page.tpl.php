<?php
/**
 * @file
 * Default view template to display content in a Masonry layout.
 *
 * SPIKE FRONT PAGE GENERATOR
 *
 */
?>
<?php



  // content elements b/w new rows
  $margin = 9;

  // Margin Array represents the index positions in the row structure,
  // at which to place the page break elements
  $options = array(
    'view-name' => $view->name,
    'margin-array' => $margin
  );

  // modify for advert content - pass margin_array as an '$options' var
  spike_advert_content_display_plugin($rows, $options);
  $view->row_index = count($rows);


  // new row count from modified rows
  $row_count = count($rows);
  // modify classes_array to fit with new row count
  if ($row_count > count($classes_array)) {
    $dif = $row_count - count($classes_array);
    $class1 = 'views-row views-row-';
    $class2 = ' views-row-';
    for ($dif; $dif > 0; $dif--) {
      $pos = count($classes_array) + 1;
      $stripe = $pos % 2 == 0 ? 'even' : 'odd';
      $classes_array[] = $class1 . $pos . $class2 . $stripe;
    }
  }


  // adjust classes array

  // calculate number of breakpoints for the view, based on content segments
  // in blocks of MARGIN
  $breakpoints = round($row_count / $margin, 0 , PHP_ROUND_HALF_DOWN);
//  $options['breakpoint_count'] = $breakpoints;

  // new row count
  $new_row_count = $row_count + $breakpoints;

  /*
   * @TODO | not getting exactly why it's done this way, but this generates
   * @TODO | an array of index positions for breakpoints,
   * @TODO | ie - the page break elements
   * @ old: modify margins based on number of breakpoints
   */
  $margin_array = array($options['margin-array']);
  for($e = $breakpoints, $i=0; $e>0; $e--, $i++) {
    $val = $margin * ($i + 1) + $i;
    $margin_array[] = $val;
  }

  array_shift($margin_array);

  // @TODO now - cache the entire markup for each breakpoint,
  // @ more efficient than just caching the ID
  // get quotation ids, these are stored in cache
  $q_markup = spike_layout_breakpoint_display_cache('quotation', QUO_MARKUP_CACHE);

  $i = 1; // row/set numb - maximum is row margin - number of rows b/w breaks
  $page = $view->query->pager->current_page + 1;
  $set_num = 1;
  $y = 0; // current overall row
    $count = count($rows);
    foreach ($rows as $id => $row):
    $tile_set_wrapper = '<div class="tile-set-wrapper scroll-unit">';
    $break_wrapper = '<div class="tile-break-wrapper scroll-unit">';
    if ($i == 1) {
      print $tile_set_wrapper;
    }
  ?>
  <div class="tiled-row loading <?php if (isset($classes_array[$id])) print ' ' . $classes_array[$id]; ?>"
    row-num="<?php print $i ?>" page-num="<?php print $page ?>" set-num="<?php print $set_num ?>">
    <?php
    print $row;
    ?>
  </div>
      <?php if ($i == $count) print '</div>' ?>

  <?php
  $i++;
  $y++;
  endforeach;


  // if we don't end on a breakpoint, & leftover count is greater than half
  // of our standard margin, add another breakpoint
    if ( true ) {
//    if ( $i == $margin-1 ) {
      $i = 1;
      $set_num++;
      $cur = $q_markup['current-position'];
      $count = count($q_markup['items']) - 1;
      $output = $q_markup['items'][$cur];

      print '</div><div class="tile-break-wrapper scroll-unit">'
        . '<div class="tile-breakpoint views-row" page-num="' .  $page
        . '"set-num="'
        . $set_num . '">' . $output['markup'] . '</div>';
      $cur++;
      $cur = bp_display_current_position($count, $cur);
      $q_markup['current-position'] = $cur;
    } else $i++;
  $y++;
//  endforeach;

  cache_set(SP_CACHE_DISP . QUO_MARKUP_CACHE, $q_markup, 'cache');
?>
