<?php

/**
 * @file
 * Display Suite 1 column template.
 */
?>
<!--@NOTE ~ THIS MARKUP originated in this file, was copied adapted for ctools custom plugin
with same banded spike layout
see spike-banded.tpl.php
-->
<div class="breakpoint-spike bp-spike-before breakpoint-el"></div>
<div class="breakpoint-shadow shadow-top breakpoint-el"></div>
<<?php print $ds_content_wrapper; print $layout_attributes; ?> class="ds-1col <?php print $classes;?> clearfix">

<?php if (isset($title_suffix['contextual_links'])): ?>
  <?php print render($title_suffix['contextual_links']); ?>
<?php endif; ?>

<?php print $ds_content; ?>
</<?php print $ds_content_wrapper ?>>

<?php if (!empty($drupal_render_children)): ?>
  <?php print $drupal_render_children ?>
<?php endif; ?>
<div class="breakpoint-spike  bp-spike-after"></div><div class="breakpoint-shadow shadow-bottom"></div>

