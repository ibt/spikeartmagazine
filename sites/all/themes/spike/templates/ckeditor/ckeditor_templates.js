CKEDITOR.addTemplates('custom', {
  // The name of sub folder which hold the shortcut preview images of the
  // templates.
  imagesPath: CKEDITOR.getUrl('/sites/all/themes/tkdbase/templates/ckeditor/images/'),

  // The templates definitions.
  templates: [{
    title: '2 Columns',
    image: '2col.gif',
    description: '2 Spaltiger Inhalt',
    html: '<div class="columns columns--two"><p>Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce vulputate eleifend sapien.</p><p>Duis lobortis massa imperdiet quam. Suspendisse potenti. Vivamus laoreet. Vestibulum fringilla pede sit amet augue.</p></div>'
  }, {
    title: '3 Columns',
    image: '3col.gif',
    description: '3 Spaltiger Inhalt',
    html: '<div class="columns columns--three"><p>Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce vulputate eleifend sapien.</p><p>Praesent porttitor, nulla vitae posuere iaculis, arcu nisl dignissim dolor, a pretium mi sem ut ipsum. Ut varius tincidunt libero.</p><p>Duis lobortis massa imperdiet quam. Suspendisse potenti. Vivamus laoreet. Vestibulum fringilla pede sit amet augue.</p></div>'
  }]
});
