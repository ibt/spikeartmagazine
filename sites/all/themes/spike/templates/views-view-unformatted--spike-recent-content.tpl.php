<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
spike_advert_content_display_plugin_sidebar($rows);

$view->row_index = count($rows);


// new row count from modified rows
$row_count = count($rows);
// modify classes_array to fit with new row count
if ($row_count > count($classes_array)) {
  $dif = $row_count - count($classes_array);
  $class1 = 'views-row views-row-';
  $class2 = ' views-row-';
  for ($dif; $dif > 0; $dif--) {
    $pos = count($classes_array) + 1;
    $stripe = $pos % 2 == 0 ? 'even' : 'odd';
    $classes_array[] = $class1 . $pos . $class2 . $stripe;
  }
}


$t=1;
?>
<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>
<?php foreach ($rows as $id => $row): ?>
  <div<?php if ($classes_array[$id]) { print ' class="' . $classes_array[$id] .'"';  } ?>>
    <?php print $row; ?>
  </div>
<?php endforeach; ?>