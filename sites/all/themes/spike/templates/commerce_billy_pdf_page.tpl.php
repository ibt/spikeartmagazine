<?php

/**
 * @file
 * Template for PDFs.
 */
function commerce_order_invoice_footer_output_element() {
  $prefix = '<td><div class="footer-el">';
  $el_prefix = '<div class="el-inner">';
  $suffix = '</div>';
  $output = array(
    '#type' => 'markup',
    '#prefix' => '<table><tr>',
    '#suffix' => '</tr></table>',
    'elements' => array(
      array(
        '#prefix' => $prefix,
        '#suffix' => $suffix . '</td>',
        '#markup' => $el_prefix .
          'Spike Art Magazine OG<br>' .
          'Loewengasse 18/13c<br>' .
          '1030 ' . t('Vienna') . '<br>' .
//          t('Austria') . '<br>' .
          'T +43 1 236 29 95<br>' .
          'F +43 1 236 29 95 10<br>' . $suffix
      ),
      array(
        '#prefix' => $prefix,
        '#suffix' => $suffix . '</td>',
        '#markup' => $el_prefix .
          'spike@spikeartmagazine.com<br>'.
          'www.spikeartmagazine.com<br>'.
          'UID-Nr. ATU64650149<br>'.
          'FN 320738z'. $suffix
      ),
      array(
        '#prefix' => $prefix,
        '#suffix' => $suffix . '</td>',
        '#markup' => $el_prefix .
          t('Bank Details') . ':<br>'.
          'Erste Bank<br>'.
          'IBAN: AT282011129135128501<br>'.
          'BIC: GIBAATWW'. $suffix
      ),
    )

  );
  return render($output);
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML+RDFa 1.1//EN">
<html lang="en" dir="ltr" version="HTML+RDFa 1.1" xmlns:xsd="http://www.w3.org/2001/XMLSchema#">
<head profile="http://www.w3.org/1999/xhtml/vocab">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <link href="https://fonts.googleapis.com/css?family=Cormorant+Garamond" rel="stylesheet">
  <?php
    $url = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'];
    $host = str_replace('http://', '', $url);
    $array = explode('.', $host);
    $el = array_pop($array);
    $t=1;
//    if ($el == 'dev') {
//      $style_path = '/sites/all/themes/spike/pdf/css/pdf_styles.css';
//      ?><!--'<link rel="stylesheet" type="text/css" href="--><?php //print $style_path ?><!--">' --><?php
//    } else {
//    }
      print '<style type="text/css">' . $inline_css . '</style>';
  ?>


</head>
<body>
<div class="invoice-footer" id="footer"><?php
  print
    commerce_order_invoice_footer_output_element()
  ?>

<!--  <p class="page">Page --><?php //$PAGE_NUM ?><!--</p>-->
</div>
<?php
  foreach ($viewed_orders as $viewed_order):
    print '<div class="invoice">';
    print render($viewed_order);
    print '</div>';
    // Force a page break.
//    print '<div style="page-break-after: always;" />';
  endforeach;
?>
</body></html>
