<?php
/**
 * @file
 * spike_metatags.features.metatag.inc
 */

/**
 * Implements hook_metatag_export_default().
 */
function spike_metatags_metatag_export_default() {
  $config = array();

  // Exported Metatag config instance: node:image_article.
  $config['node:image_article'] = array(
    'instance' => 'node:image_article',
    'config' => array(
      'og:image' => array(
        'value' => '[node:field_image]',
      ),
    ),
  );

  return $config;
}
