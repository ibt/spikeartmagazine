<?php
/**
 * @file
 * spike_metatags.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function spike_metatags_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'metatag_enable_node__article';
  $strongarm->value = TRUE;
  $export['metatag_enable_node__article'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'metatag_enable_node__image_article';
  $strongarm->value = TRUE;
  $export['metatag_enable_node__image_article'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'metatag_enable_node__issue';
  $strongarm->value = TRUE;
  $export['metatag_enable_node__issue'] = $strongarm;

  return $export;
}
