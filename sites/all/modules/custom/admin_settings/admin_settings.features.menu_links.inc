<?php
/**
 * @file
 * admin_settings.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function admin_settings_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-operator_add-article:node/add/article.
  $menu_links['menu-operator_add-article:node/add/article'] = array(
    'menu_name' => 'menu-operator',
    'link_path' => 'node/add/article',
    'router_path' => 'node/add/article',
    'link_title' => 'Add article',
    'options' => array(
      'attributes' => array(),
      'identifier' => 'menu-operator_add-article:node/add/article',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-operator_add-issue:node/add/issue.
  $menu_links['menu-operator_add-issue:node/add/issue'] = array(
    'menu_name' => 'menu-operator',
    'link_path' => 'node/add/issue',
    'router_path' => 'node/add/issue',
    'link_title' => 'Add issue',
    'options' => array(
      'attributes' => array(),
      'identifier' => 'menu-operator_add-issue:node/add/issue',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 1,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: user-menu_add-content:node/add.
  $menu_links['user-menu_add-content:node/add'] = array(
    'menu_name' => 'user-menu',
    'link_path' => 'node/add',
    'router_path' => 'node/add',
    'link_title' => 'Add content',
    'options' => array(
      'attributes' => array(),
      'identifier' => 'user-menu_add-content:node/add',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 1,
    'weight' => -50,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: user-menu_content:admin/content.
  $menu_links['user-menu_content:admin/content'] = array(
    'menu_name' => 'user-menu',
    'link_path' => 'admin/content',
    'router_path' => 'admin/content',
    'link_title' => 'Content',
    'options' => array(
      'attributes' => array(),
      'identifier' => 'user-menu_content:admin/content',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 1,
    'weight' => -49,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: user-menu_log-in:user/login.
  $menu_links['user-menu_log-in:user/login'] = array(
    'menu_name' => 'user-menu',
    'link_path' => 'user/login',
    'router_path' => 'user/login',
    'link_title' => 'Log in',
    'options' => array(
      'identifier' => 'user-menu_log-in:user/login',
    ),
    'module' => 'system',
    'hidden' => -1,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'language' => 'und',
    'menu_links_customized' => 0,
    'parent_identifier' => 'user-menu_user-account:user',
  );
  // Exported menu link: user-menu_log-out:user/logout.
  $menu_links['user-menu_log-out:user/logout'] = array(
    'menu_name' => 'user-menu',
    'link_path' => 'user/logout',
    'router_path' => 'user/logout',
    'link_title' => 'Log out',
    'options' => array(
      'identifier' => 'user-menu_log-out:user/logout',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -45,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: user-menu_store:admin/commerce.
  $menu_links['user-menu_store:admin/commerce'] = array(
    'menu_name' => 'user-menu',
    'link_path' => 'admin/commerce',
    'router_path' => 'admin/commerce',
    'link_title' => 'Store',
    'options' => array(
      'attributes' => array(),
      'identifier' => 'user-menu_store:admin/commerce',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 1,
    'weight' => -47,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: user-menu_terms:terms.
  $menu_links['user-menu_terms:terms'] = array(
    'menu_name' => 'user-menu',
    'link_path' => 'terms',
    'router_path' => 'terms',
    'link_title' => 'Terms',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'user-menu_terms:terms',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: user-menu_user-account:user.
  $menu_links['user-menu_user-account:user'] = array(
    'menu_name' => 'user-menu',
    'link_path' => 'user',
    'router_path' => 'user',
    'link_title' => 'User account',
    'options' => array(
      'alter' => TRUE,
      'identifier' => 'user-menu_user-account:user',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -46,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Add article');
  t('Add content');
  t('Add issue');
  t('Content');
  t('Log in');
  t('Log out');
  t('Store');
  t('Terms');
  t('User account');

  return $menu_links;
}
