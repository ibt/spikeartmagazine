<?php
/**
 * @file
 * spike_search.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function spike_search_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
}

/**
 * Implements hook_views_api().
 */
function spike_search_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_default_search_api_index().
 */
function spike_search_default_search_api_index() {
  $items = array();
  $items['default_node_index'] = entity_import('search_api_index', '{
    "name" : "Default node index",
    "machine_name" : "default_node_index",
    "description" : "An automatically created search index for indexing node data. Might be configured to specific needs.",
    "server" : "spike_search_server",
    "item_type" : "node",
    "options" : {
      "datasource" : { "bundles" : [] },
      "index_directly" : 1,
      "cron_limit" : "-1",
      "data_alter_callbacks" : {
        "search_api_alter_bundle_filter" : {
          "status" : 1,
          "weight" : "-10",
          "settings" : {
            "default" : "0",
            "bundles" : {
              "article" : "article",
              "edition" : "edition",
              "image_article" : "image_article",
              "issue" : "issue"
            }
          }
        },
        "search_api_alter_node_access" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_node_status" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_language_control" : {
          "status" : 0,
          "weight" : "0",
          "settings" : { "lang_field" : "", "languages" : [] }
        },
        "search_api_alter_add_viewed_entity" : { "status" : 0, "weight" : "0", "settings" : { "mode" : "full" } },
        "search_api_alter_add_url" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_aggregation" : {
          "status" : 1,
          "weight" : "0",
          "settings" : { "fields" : { "search_api_aggregation_1" : {
                "name" : "Title full text",
                "type" : "fulltext",
                "fields" : [ "title" ],
                "description" : "A Fulltext aggregation of the following fields: Title."
              }
            }
          }
        },
        "search_api_alter_add_hierarchy" : { "status" : 0, "weight" : "0", "settings" : { "fields" : [] } }
      },
      "processors" : {
        "search_api_case_ignore" : {
          "status" : 1,
          "weight" : "0",
          "settings" : { "fields" : {
              "title" : true,
              "field_author" : true,
              "field_title_sub" : true,
              "field_online_bool" : true,
              "search_api_language" : true,
              "body:value" : true,
              "field_tags:name" : true,
              "field_author:name" : true
            }
          }
        },
        "search_api_html_filter" : {
          "status" : 1,
          "weight" : "10",
          "settings" : {
            "fields" : { "title" : true },
            "title" : 0,
            "alt" : 1,
            "tags" : "h1 = 5\\r\\nh2 = 3\\r\\nh3 = 2\\r\\nstrong = 2\\r\\nb = 2\\r\\nem = 1.5\\r\\nu = 1.5"
          }
        },
        "search_api_transliteration" : {
          "status" : 0,
          "weight" : "15",
          "settings" : { "fields" : { "title" : true } }
        },
        "search_api_tokenizer" : {
          "status" : 1,
          "weight" : "20",
          "settings" : { "fields" : [], "spaces" : "[^\\\\p{L}\\\\p{N}]", "ignorable" : "[-]" }
        },
        "search_api_stopwords" : {
          "status" : 0,
          "weight" : "30",
          "settings" : {
            "fields" : { "title" : true },
            "file" : "",
            "stopwords" : "but\\r\\ndid\\r\\nthe this that those\\r\\netc"
          }
        },
        "search_api_highlighting" : {
          "status" : 0,
          "weight" : "35",
          "settings" : {
            "prefix" : "\\u003Cstrong\\u003E",
            "suffix" : "\\u003C\\/strong\\u003E",
            "excerpt" : 1,
            "excerpt_length" : "256",
            "exclude_fields" : [],
            "highlight" : "always"
          }
        }
      },
      "fields" : {
        "body:value" : { "type" : "text", "boost" : "2.0" },
        "created" : { "type" : "date" },
        "field_author" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "field_author:name" : { "type" : "list\\u003Cstring\\u003E", "boost" : "3.0" },
        "field_blurb:value" : { "type" : "text", "boost" : "0.3" },
        "field_online_bool" : { "type" : "boolean" },
        "field_tags:name" : { "type" : "list\\u003Ctext\\u003E", "boost" : "5.0" },
        "field_title_sub" : { "type" : "text", "boost" : "2.0" },
        "search_api_aggregation_1" : { "type" : "text", "boost" : "13.0" },
        "search_api_language" : { "type" : "string" },
        "title" : { "type" : "string" },
        "type" : { "type" : "string" }
      }
    },
    "enabled" : "1",
    "read_only" : "0"
  }');
  return $items;
}

/**
 * Implements hook_default_search_api_server().
 */
function spike_search_default_search_api_server() {
  $items = array();
  $items['spike_search_server'] = entity_import('search_api_server', '{
    "name" : "spike search server",
    "machine_name" : "spike_search_server",
    "description" : "",
    "class" : "search_api_db_service",
    "options" : {
      "database" : "default:default",
      "min_chars" : 1,
      "partial_matches" : 0,
      "indexes" : { "default_node_index" : {
          "search_api_language" : {
            "table" : "search_api_db_default_node_index",
            "column" : "search_api_language",
            "type" : "string",
            "boost" : "1.0"
          },
          "field_author" : {
            "table" : "search_api_db_default_node_index_field_author",
            "column" : "value",
            "type" : "list\\u003Cinteger\\u003E",
            "boost" : "1.0"
          },
          "field_title_sub" : {
            "table" : "search_api_db_default_node_index_text_1",
            "type" : "text",
            "boost" : "2.0"
          },
          "body:value" : {
            "table" : "search_api_db_default_node_index_text_1",
            "type" : "text",
            "boost" : "2.0"
          },
          "field_online_bool" : {
            "table" : "search_api_db_default_node_index",
            "column" : "field_online_bool",
            "type" : "boolean",
            "boost" : "1.0"
          },
          "field_tags:name" : {
            "table" : "search_api_db_default_node_index_text_1",
            "type" : "list\\u003Ctext\\u003E",
            "boost" : "5.0"
          },
          "field_author:name" : {
            "table" : "search_api_db_default_node_index_field_author_name",
            "type" : "list\\u003Cstring\\u003E",
            "boost" : "3.0",
            "column" : "value"
          },
          "type" : {
            "table" : "search_api_db_default_node_index",
            "column" : "type_1",
            "type" : "string",
            "boost" : "1.0"
          },
          "created" : {
            "table" : "search_api_db_default_node_index",
            "column" : "created",
            "type" : "date",
            "boost" : "1.0"
          },
          "title" : {
            "table" : "search_api_db_default_node_index",
            "column" : "title",
            "type" : "string",
            "boost" : "1.0"
          },
          "search_api_aggregation_1" : {
            "table" : "search_api_db_default_node_index_text_1",
            "type" : "text",
            "boost" : "13.0"
          },
          "field_blurb:value" : {
            "table" : "search_api_db_default_node_index_text_1",
            "type" : "text",
            "boost" : "0.3"
          }
        }
      }
    },
    "enabled" : "1"
  }');
  return $items;
}
