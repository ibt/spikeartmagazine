<?php
/**
 * @file
 * spike_search.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_handlers().
 */
function spike_search_default_page_manager_handlers() {
  $export = array();

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'search_node_panel_context';
  $handler->task = 'search';
  $handler->subtask = 'node';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Spike Search',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'access' => array(
      'logic' => 'and',
    ),
  );
  $display = new panels_display();
  $display->layout = 'flexible';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '29f78278-f5ad-42dc-bd68-1f2893a13d35';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-17e72029-2450-42de-ae95-4eda48ef750a';
    $pane->panel = 'center';
    $pane->type = 'page_title';
    $pane->subtype = 'page_title';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'markup' => 'h2',
      'class' => '',
      'id' => '',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '17e72029-2450-42de-ae95-4eda48ef750a';
    $display->content['new-17e72029-2450-42de-ae95-4eda48ef750a'] = $pane;
    $display->panels['center'][0] = 'new-17e72029-2450-42de-ae95-4eda48ef750a';
    $pane = new stdClass();
    $pane->pid = 'new-417a57fa-6653-4c1a-bdc1-17ef04a71c85';
    $pane->panel = 'center';
    $pane->type = 'block';
    $pane->subtype = 'views--exp-spike_search-panel_pane_1';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'inherit_path' => 1,
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '417a57fa-6653-4c1a-bdc1-17ef04a71c85';
    $display->content['new-417a57fa-6653-4c1a-bdc1-17ef04a71c85'] = $pane;
    $display->panels['center'][1] = 'new-417a57fa-6653-4c1a-bdc1-17ef04a71c85';
    $pane = new stdClass();
    $pane->pid = 'new-7982b3ec-6255-4512-bc1a-0cba7345c561';
    $pane->panel = 'center';
    $pane->type = 'views_panes';
    $pane->subtype = 'spike_search-panel_pane_1';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '7982b3ec-6255-4512-bc1a-0cba7345c561';
    $display->content['new-7982b3ec-6255-4512-bc1a-0cba7345c561'] = $pane;
    $display->panels['center'][2] = 'new-7982b3ec-6255-4512-bc1a-0cba7345c561';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = 'new-17e72029-2450-42de-ae95-4eda48ef750a';
  $handler->conf['display'] = $display;
  $export['search_node_panel_context'] = $handler;

  return $export;
}
