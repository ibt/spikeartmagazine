<?php

/**
 * @file spike_search.module
 * Code for the Spike Search feature.
 */

include_once 'spike_search.features.inc';

function spike_search_views_pre_render(&$view)
{
  switch ($view->name) {
    case 'search':
      /*
       * adjust views search to not display values on blank page
       */
      if (!$view->filter['search_api_views_fulltext']->value) {
        unset($view->result);
      }
      /*
       * pre-process content
       */
      spike_search_views_pre_render_processor($view);

      break;
  }
}

function spike_search_views_pre_render_processor(&$view)
{
  $result = &$view->result;
  $nids = array();
  $nodes = array();
  global $language;
  // gather nodes
  if (!$result) return;
  foreach ($result as $key => $val) {
    if (isset($val->entity)
      && $node = node_load($val->entity)) {
      $nodes[$key] = $node;
      $nids[$key] = $node->nid;
    }
  }
  unset($node);
  // for issues & editions, process view vals to remove translations
  foreach ($nodes as $key => $val) {
    if ($val->type == 'issue' || $val->type == 'edition') {
      if (isset($val->tnid)) {
        $translations = translation_node_get_translations($val->tnid);
        if (!$translations) return;
        unset($translations[$language->language]); // remove active lang node
        foreach ($translations as $trans) {
          $nid = $trans->nid;
          foreach ($result as $key => $val) {
            if ($val->entity == $nid) unset($result[$key]); // remove translations
          }
        }
      }
    }
  }
}

/**
 * Implements hook_form_alter().
 *
 *
 */
function spike_search_form_alter(&$form, &$form_state, $form_id)
{
  if ($form_id == 'views_exposed_form'
    && $form['#id'] === 'views-exposed-form-search-page') {
    if (arg(0) != 'search') {
      // search form  not on search page
      $form['submit']['#value'] = '<span class="glyphicon glyphicon-search"></span>';
      $form['submit']['#weight'] = -1;
      $form['keys']['#weight'] = 0;
    } else {
      $form['field_online_bool']['#options'][0] = t('Print only');
      $form['field_online_bool']['#options'][1] = t('Online');
    }
    $form['input']['#attributes']['placeholder'] = t('search');
  }
}

function spike_search_views_exposed_form_alter(&$form, $form_state)
{
  // change date format example from a field called 'date_filter'
  $form['date_filter']['value']['#date_format'] = 'd-m-Y';
}

/**
 * Implements hook_block_info().
 */
function spike_search_block_info()
{
  $blocks['spike_search_form'] = array(
    'info' => t('Spike search form navbar'),
    'cache' => DRUPAL_NO_CACHE,
  );
  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function spike_search_block_view($delta = '')
{
  $block = array();
  switch ($delta) {
    case 'spike_search_form':
      $block['subject'] = t('');
      $block['content'] = spike_search_form_block_callback();
      break;
  }
  return $block;
}

function spike_search_form_block_callback()
{
  $block = module_invoke('views', 'block_view', '-exp-search-page');
  return $block['content'];
}

function spike_search_block_view_alter(&$data, $block)
{
  switch ($block->delta) {
    case 'language_control':
      // append the search query to the lang-switcher links for seamless search switch
      if (arg(0) == 'search') {
        $query = drupal_get_query_parameters();
        $items = array(
          'attributes' => array(
            'class' => array(
              'language-switcher-locale-url',
              'findMe'
            )
          )
        );
        $languages = language_list();
        foreach ($languages as $language) {
          if ($language->enabled == 1) {
            $link = 'search';
            $items['items'][] = l($language->native, $link, array(
              'language' => $language,
              'query' => $query,
            ));
          }
          $data['content'] = theme('item_list', $items);
        }
      }
      break;
  }
}