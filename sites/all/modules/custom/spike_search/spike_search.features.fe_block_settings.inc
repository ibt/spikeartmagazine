<?php
/**
 * @file
 * spike_search.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function spike_search_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['spike_search-spike_search_form'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'spike_search_form',
    'module' => 'spike_search',
    'node_types' => array(),
    'pages' => '<front>
glish
utsch',
    'roles' => array(),
    'themes' => array(
      'ember' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'ember',
        'weight' => 0,
      ),
      'spike' => array(
        'region' => 'navigation',
        'status' => 1,
        'theme' => 'spike',
        'weight' => -27,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  return $export;
}
