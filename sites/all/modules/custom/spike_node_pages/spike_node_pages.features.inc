<?php
/**
 * @file
 * spike_node_pages.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function spike_node_pages_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "panelizer" && $api == "panelizer") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function spike_node_pages_node_info() {
  $items = array(
    'page_paragraphs' => array(
      'name' => t('Page with Sections'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

/**
 * Implements hook_paragraphs_info().
 */
function spike_node_pages_paragraphs_info() {
  $items = array(
    'paragraphs_pack_content' => array(
      'name' => 'Content',
      'bundle' => 'paragraphs_pack_content',
      'locked' => '1',
    ),
  );
  return $items;
}
