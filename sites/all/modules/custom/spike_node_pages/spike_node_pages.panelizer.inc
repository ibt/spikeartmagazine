<?php
/**
 * @file
 * spike_node_pages.panelizer.inc
 */

/**
 * Implements hook_panelizer_defaults().
 */
function spike_node_pages_panelizer_defaults() {
  $export = array();

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->title = 'Default';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'page';
  $panelizer->access = array();
  $panelizer->view_mode = 'page_manager';
  $panelizer->name = 'node:page:default';
  $panelizer->css_id = '';
  $panelizer->css_class = 'info-page';
  $panelizer->css = '';
  $panelizer->no_blocks = FALSE;
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $panelizer->pipeline = 'standard';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $display = new panels_display();
  $display->layout = 'flexible';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = '388a848c-3b79-4e04-b712-0793ac290a81';
  $display->storage_type = 'panelizer_default';
  $display->storage_id = 'node:page:default';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-501c9b7f-8183-4146-b247-2e0be7eb020d';
  $pane->panel = 'center';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:field_blurb';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'text_default',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(
      'field_multiple_limit' => -1,
      'field_multiple_limit_offset' => 0,
      'linked_field' => array(
        'linked' => 0,
        'destination' => '',
        'advanced' => array(
          'title' => '',
          'target' => '',
          'class' => '',
          'rel' => '',
          'text' => '',
        ),
      ),
    ),
    'context' => 'panelizer',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '501c9b7f-8183-4146-b247-2e0be7eb020d';
  $display->content['new-501c9b7f-8183-4146-b247-2e0be7eb020d'] = $pane;
  $display->panels['center'][0] = 'new-501c9b7f-8183-4146-b247-2e0be7eb020d';
  $pane = new stdClass();
  $pane->pid = 'new-4fa142dd-1c25-4bee-94d1-d51287c5d009';
  $pane->panel = 'center';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:body';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'text_default',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(
      'field_multiple_limit' => -1,
      'field_multiple_limit_offset' => 0,
      'linked_field' => array(
        'linked' => 0,
        'destination' => '',
        'advanced' => array(
          'title' => '',
          'target' => '',
          'class' => '',
          'rel' => '',
          'text' => '',
        ),
      ),
    ),
    'context' => 'panelizer',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '4fa142dd-1c25-4bee-94d1-d51287c5d009';
  $display->content['new-4fa142dd-1c25-4bee-94d1-d51287c5d009'] = $pane;
  $display->panels['center'][1] = 'new-4fa142dd-1c25-4bee-94d1-d51287c5d009';
  $pane = new stdClass();
  $pane->pid = 'new-f8af1ab4-533f-4e77-a9f7-18f5cf42492f';
  $pane->panel = 'center';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:field_file';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'hidden',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(),
    'context' => 'panelizer',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = 'f8af1ab4-533f-4e77-a9f7-18f5cf42492f';
  $display->content['new-f8af1ab4-533f-4e77-a9f7-18f5cf42492f'] = $pane;
  $display->panels['center'][2] = 'new-f8af1ab4-533f-4e77-a9f7-18f5cf42492f';
  $pane = new stdClass();
  $pane->pid = 'new-f9ab1704-9705-4111-874a-46b250995bb1';
  $pane->panel = 'center';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:field_image';
  $pane->shown = FALSE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'hidden',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(),
    'context' => 'panelizer',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 3;
  $pane->locks = array();
  $pane->uuid = 'f9ab1704-9705-4111-874a-46b250995bb1';
  $display->content['new-f9ab1704-9705-4111-874a-46b250995bb1'] = $pane;
  $display->panels['center'][3] = 'new-f9ab1704-9705-4111-874a-46b250995bb1';
  $pane = new stdClass();
  $pane->pid = 'new-38ed83db-51ea-4bd0-9981-6126d0644f7e';
  $pane->panel = 'center';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => 'Sticky footer seperator',
    'title' => '',
    'body' => '<!-- container for sticky footer seperator, tooth  whatever --><p><a name="subscriptions"></a></p>',
    'format' => 'full_html',
    'substitute' => 1,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'sticky-footer-seperator',
  );
  $pane->extras = array();
  $pane->position = 4;
  $pane->locks = array();
  $pane->uuid = '38ed83db-51ea-4bd0-9981-6126d0644f7e';
  $display->content['new-38ed83db-51ea-4bd0-9981-6126d0644f7e'] = $pane;
  $display->panels['center'][4] = 'new-38ed83db-51ea-4bd0-9981-6126d0644f7e';
  $pane = new stdClass();
  $pane->pid = 'new-52d1f01f-90a1-487d-b9f8-f2de9059deea';
  $pane->panel = 'center';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:field_file';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'file_rendered',
    'delta_limit' => '0',
    'delta_offset' => '0',
    'delta_reversed' => 0,
    'formatter_settings' => array(
      'field_multiple_limit' => '-1',
      'field_multiple_limit_offset' => '0',
      'file_view_mode' => 'default',
    ),
    'context' => 'panelizer',
    'override_title' => 1,
    'override_title_text' => 'Downloads',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => 'sticky-footer-files',
    'css_class' => 'sticky-footer',
  );
  $pane->extras = array();
  $pane->position = 5;
  $pane->locks = array();
  $pane->uuid = '52d1f01f-90a1-487d-b9f8-f2de9059deea';
  $display->content['new-52d1f01f-90a1-487d-b9f8-f2de9059deea'] = $pane;
  $display->panels['center'][5] = 'new-52d1f01f-90a1-487d-b9f8-f2de9059deea';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-501c9b7f-8183-4146-b247-2e0be7eb020d';
  $panelizer->display = $display;
  $export['node:page:default'] = $panelizer;

  return $export;
}
