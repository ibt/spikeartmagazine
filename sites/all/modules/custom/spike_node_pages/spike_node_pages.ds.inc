<?php
/**
 * @file
 * spike_node_pages.ds.inc
 */

/**
 * Implements hook_ds_layout_settings_info().
 */
function spike_node_pages_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|page_paragraphs|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'page_paragraphs';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_paragraphs',
        1 => 'field_file_insert',
      ),
    ),
    'fields' => array(
      'field_paragraphs' => 'ds_content',
      'field_file_insert' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['node|page_paragraphs|default'] = $ds_layout;

  return $export;
}
