(function($) {

  Drupal.behaviors.paraPage = {

    attach: function (context, settings) {


      /*
       on paragraph pages, for inserting file field items properly into the
        paragraph item, we use tokens input by the user in the interface
        to identify which file field item to replace w/ jquery into the
        desired position - similar to caption replacement in spike articles
       */
      $('.node-type-page-paragraphs .node-page-paragraphs .paragraphs-items', context).once(function() {
        var pEl = $(this);
        var fileTarget = pEl.find('.insert-files');
        var fileField = pEl.siblings('.field-name-field-file-insert');
        $.each(fileTarget, function () {
          var insertPos = $(this).find('td');
          $.each(insertPos, function(i, e) {
            // target td element into which we insert the file field item
            var el = $(this);
            var str = el.text();
            // check for token, which is input by user in interface!
            var test = '[[insert-file-';
            if (str.indexOf(test) > -1) {
              var pos = str.replace(test, '');
              pos = pos.replace(']]', ''); // clean variable for index int
              var index = pos - 1;
              var fileItem = fileField.find('.field-item').eq(index).clone();
              fileItem.find('h4').remove();
              var link = fileItem.find('a').attr('href');
              el.html(fileItem)
              // wrap corresponding td in next row with same link
              var labelEl = insertPos.eq(i+3);
              labelEl.wrapInner('<a></a>');
              labelEl.find('a').attr('href', link)
            }
          })
        })
//        fileField.remove();
      });



    }
  };
})(jQuery);