jQuery(function($) {
  // Asynchronously Load the map API
  var script = document.createElement('script');
  script.src = "http://maps.googleapis.com/maps/api/js?sensor=false&callback=initMap";
//  script.src = "http://maps.googleapis.com/maps/api/js?sensor=false&callback=initialize";
  document.body.appendChild(script);
});

function initMap() {
  var bounds = new google.maps.LatLngBounds();
  var map = new google.maps.Map(document.getElementById('map-canvas'), {
    zoom: 4,
    center: {lat: 50, lng: 20}
  });
  map.setTilt(45);
  var markers = [
    ['Buchhandlung Walther König im Stedelijk Museum, Museumplein 10, Amsterdam ', 52.358001, 4.87973],
    ['International Magazine Store Meir 78, Antwerpen',	51.218049,	4.411331],
    ['International Magazine Store Melkmarkt 17, Antwerpen',	51.220453, 4.402785],
    ['Motto @ Kunsthalle Basel Steinenberg 7, Basel',	47.553725,	7.59119],
    ['Museum Tinguely Paul Sacher-Anlage 2, Basel ',	47.558814,	7.611705],
    ['Artificium Rosenthaler Straße 40-41, Berlin', 52.523859,	13.402259],
    ['b_books Lübbenerstraße 14, Berlin',	52.49969,	13.43804],
    ['Buchhandlung Kisch & Co Oranienstraße 25, Berlin',	52.501065,	13.420656],
    ['Buchhandlung Walther König An Der Museumsinsel Burgstraße 27, Berlin',	52.520007,	13.404954],
    ['Bücherbogen Am Savignyplatz Stadtbahnbogen 593, Berlin',	52.50488,	13.32162],
    ['do you read me?!  Auguststraße 28, Berlin',	52.527276,	13.398179],
    ['do you read me?!  Potsdamer Straße 98, Berlin',	52.527276,	13.398179],
    ['Hamburger Bahnhof Museum für Gegenwart Invalidenstraße 50-51, Berlin',	52.528183,	13.371493],
    ['Motto Berlin Skalitzerstraße 68, Berlin',	52.500246,	13.439784],
    ['ArtStore Deutsche Bank KunstHalle Unter den Linden 13/15, Berlin',	52.516774,	13.391648],
    ['Neues Kapitel Kopenhagener Straße 7, Berlin',	52.549003,	13.411296],
    ['playing with eels Urbanstraße 32, Berlin',	52.4912,	13.41385],
    ['Pro QM Almstadtstraße 48-50, Berlin',	52.52698,	13.41009],
    ['Soda Weinbergsweg 1, Berlin',	52.530057,	13.401883],
    ['Das Lehrerzimmer Waisenhausplatz 30, Bern',	46.950156,	7.443874],
    ['Motto @ Wiels Ave van Volxemlaan 354, Brüssel ',	50.85034,	4.35171],
    ['Librairie Brillat Savarin Ave Brillat Savarin 25, Brüssel ',	50.85034,	4.35171],
    ['Le Canard Rue Borrens 1, Brüssel ',	50.85034,	4.35171],
    ['Relay Gare du midi, Brüssel ',	50.85034,	4.35171],
    ['CAPC musée d’art contemporain 7, rue Ferrère, Bordeaux',	44.837789,	-0.57918],
    ['Quimby’s Bookstore 1854 W. North Ave, Chicago',	41.910718,	-87.674763],
    ['Buchhandlung Walther König in der Kunsthalle Grabbeplatz 4, Düsseldorf ',	51.227442,	6.775807],
    ['Buchhandlung Walther König Hasengasse 5-7, Frankfurt', 	50.11292,	8.68373],
    ['Fri Art Kunsthalle Petites-Rames 22, Fribourg',	46.803199,	7.159407],
    ['Centre d’Art Contemporain Genève Rue des Vieux-Grenadiers 10, Genf',	46.198733,	6.137668],
    ['International Press Store Rooigemlaan 501, Gent',	51.063604,	3.695099],
    ['Nieuwscentrum Walry Zwijnaardsesteenweg 6, Gent',	51.03702,	3.726371],
    ['Ganda Bookshop Brusselsesteenweg 218, Gent',	51.04101,	3.741965],
    ['B+C Museumsshop Kunsthaus Graz, Lendkai 1, Graz',	47.071031,	15.434002],
    ['Gudberg Nerger Poolstraße 8, Hamburg',	53.55396,	9.98008],
    ['Sautter + Lackmann Admiralitötstraße 71/72, Hamburg',	53.548,	9.984],
    ['Deichtorhallen Deichtorstraße 1_2, Hamburg',	53.547,	10.006],
    ['[Six Chairs] Books Vytautas av. 71, Kaunas',	54.898,	23.903],
    ['Buchhandlung Walther König Ehrenstraße 4, Köln ',	50.938,	6.944],
    ['Buchhandlung Walther König Im Museum Ludwig Bischofsgartenstraße 1, Köln ',	50.940,	6.961],
    ['Fotografisk Center Pasteursvej 14, Kopenhagen',	55.665,	12.534],
    ['Motto @ Charlottenborg Nyhavn 2, Kopenhagen',	55.680, 12.588],
    ['MZIN Bookstore Kolonnadenstraße 20, Leipzig',	51.338, 12.365],
    ['ICA The Mall, London',	51.504	-0.13342],
    ['Serpentine Gallery Kensington Gardens, London',	51.506,	-0.179165],
    ['Centerfold 716 N Fairfax Ave, Los Angeles',	34.084,	-118.361],
    ['Family 436 N. Fairfax Ave., Los Angeles',	34.079,	-118.361],
    ['LACMA 5905 Wilshire Blvd., Los Angeles',	34.063,	-118.360],
    ['National News Hollywood, Los Angeles',	34.092,	-118.328],
    ['The MOCA Store 250 S Grand Ave, Los Angeles',	34.053, -118.250],
    ['Robertson Magazine & Book Store 1414 S Robertson Blvd, Los Angeles',	34.054,	-118.383],
    ['CCA Andratx C/ Estanyera 2, Andratx, Mallorca ',	39.574,	2.420],
    ['Haus der Kunst Prinzregentenstraße 1, München',	48.14411,	11.5861],
    ['Literatur Moths Rumfordstraße 48, München',	48.13357,	11.582289],
    ['Soda Rumfordstraße 3, München',	48.133,	11.575],
    ['Around the world 148 West 37th St, New York',	40.752,	-73.988],
    ['Artbook @ MoMA PS 1 22-25 Jackson Ave., New York',	40.745, -73.947],
    ['B.J. Magazines Inc. 200 Varick St, New York',	40.72821,	-74.00472],
    ['Bouwerie Iconic Magazine 215 Bowery, New York',	40.7216370,-73.9932000],
    ['Canal Iconic Magazine 385 Canal St 6th Ave., New York',	40.74281,	-73.992866],
    ['J P Lobby Stand 601 West 26th St, New York',	40.751,	-74.006],
    ['Mulberry Iconic Magazines 188 Mulberry St, New York',	40.721,	-73.996],
    ['New Museum Store 235 Bowery, New York',	40.722,	-73.992],
    ['Saint MarkÕs Bookshop 31 Third Avenue (zwischen 8th und 9th), New York',	40.712,	-74.005],
    ['Shami Smoke & Magazine 1886 Broadway, New York',	40.771,	-73.981],
    ['Soho News International 186 Prince St, New York',	40.72634,	-74.002267],
    ['Village Global News Inc., New York',	40.712,	-74.005],
    ['Buchhandlung Walther König im Neuen Museum Nürnberg, Luitpoldstraße 5, Nürnberg ',	49.44772,	11.08004],
    ['castillo/corrales 80 rue Julien Lacroix, Paris',	48.872,	2.382],
    ['Chez Moi 25 rue Herold, Paris',	48.865,	2.341],
    ['Drugstore Publicis 133 ave des Champs Elysees, Paris',	48.872,	2.297],
    ['Les Mots ˆ la Bouche 6 Rue Ste Croix de la Bretonnerie, Paris',	48.856, 2.352],
    ['Librairie Artcurial 61 ave Montaigne, Paris',	48.868,	2.308],
    ['Librairie Flammarion La Hune 16 Rue de l’Abbaye, Paris',	48.854,	2.333],
    ['Palais de Tokyo 13 avenue du Président Wilson, Paris',	48.8645720,2.2963700],
    ['No Youth Control 41 rue des Dames, Paris',	48.884,	2.323],
    ['Thierry Presse 76 rue du Ch‰teau dÕEau, Paris',	48.872,	2.354],
    ['WHSmith 248 rue de Rivoli, Paris',	48.866,	2.324],
    ['Avril 50 3406 Sansom St, Philadelphia',	39.953, -75.192],
    ['Fog City News 455 Market St, San Francisco',	37.790,	-122.398],
    ['Smoke Signals 2223 Polk St, San Francisco',	377.972,	-122.422],
    ['Green Apple Books 506 Clement St, San Francisco',	37.783, -122.464],
    ['Buchhandlung Walther König im Kunstmuseum, Kleiner Schlossplatz 1, Stuttgart',	48.779,	9.178],
    ['Eslite Xin Yi Flagship Store 11 Songgao Rd., Taipeh',	25.032, 121.565],
    ['Eslite Shi Jian University Store 70 Da Zhi St, Taipeh',	25.032,	121.565],
    ['Motto Vancouver in der Or Gallery, 555 Hamilton Street, Vancouver ',	49.281, -123.112],
    ['21er Haus Salon Für Kunstbuch Schweizergarten, Arsenalstraße 1, Wien ',	48.18586,	16.383881],
    ['Buchhandlung Walther König im Museumsquartier, Museumsplatz 1, Wien ',	48.203,	16.359],
    ['Kunsthalle Wien Shop Museumsquartier, Museumsplatz 1, Wien ',	48.203,	16.359],
    ['Mak Design Shop Stubenring 5, Wien ',	48.207,	16.381],
    ['Park Mondscheingasse 20, Wien ',	48.201,	16.349],
    ['Museumsshop Kunstmuseum Wolfsburg Hollerplatz 1, Wolfsburg',	52.418,	10.785],
    ['Buchhandlung Kunstgriff Limmatstrasse 270, Zürich',	47.388,	8.525],

  ];

  // Info Window Content
  var infoWindowContent = [
    ['<div class="info-content"><h3>London Eye</h3>' + '</div>'],
    ['<div class="info-content"><h3>Palace of Westminster</h3>' + '</div>']
  ];

  // Display multiple markers on a map
  var infoWindow = new google.maps.InfoWindow(), marker, i


  // Loop through our array of markers & place each one on the map
  for( i = 0; i < markers.length; i++ ) {
    var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
    bounds.extend(position);
    marker = new google.maps.Marker({
      position: position,
      map: map,
      title: markers[i][0]

    });

    // Allow each marker to have an info window
    google.maps.event.addListener(marker, 'click', (function(marker, i) {
      return function() {
        infoWindow.setContent(markers[i][0]);
//        infoWindow.setContent(infoWindowContent[i][0]);
        infoWindow.open(map, marker);
      }
    })(marker, i));

  }


  // Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
//  var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
//    this.setZoom(14);
//    google.maps.event.removeListener(boundsListener);
//  });

//
//  var image = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png';
//  var beachMarker = new google.maps.Marker({
//    position: {lat: -33.890, lng: 151.274},
//    map: map,
//    icon: image
//  });
}
