<?php
/**
 * @file
 * spike_node_pages.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_handlers().
 */
function spike_node_pages_default_page_manager_handlers() {
  $export = array();

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'node_view_http_response';
  $handler->task = 'node_view';
  $handler->subtask = '';
  $handler->handler = 'http_response';
  $handler->weight = -28;
  $handler->conf = array(
    'title' => 'Redirect from content to frontpage',
    'contexts' => array(),
    'relationships' => array(),
    'code' => '301',
    'destination' => '<front>',
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'node_type',
          'settings' => array(
            'type' => array(
              'advert_content' => 'advert_content',
              'highlight_element' => 'highlight_element',
              'feed_item' => 'feed_item',
              'spike_logo' => 'spike_logo',
              'quotation' => 'quotation',
              'subscription' => 'subscription',
              'advert_sidebar' => 'advert_sidebar',
            ),
          ),
          'context' => 'argument_entity_id:node_1',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
  );
  $export['node_view_http_response'] = $handler;

  $handler = new stdClass();
  $handler->disabled = TRUE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'node_view_panel_context';
  $handler->task = 'node_view';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = -25;
  $handler->conf = array(
    'title' => 'Issues',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'node_type',
          'settings' => array(
            'type' => array(
              'issue' => 'issue',
            ),
          ),
          'context' => 'argument_entity_id:node_1',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
  );
  $display = new panels_display();
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = 'db1394cf-0f68-406c-adf1-f4d35f16ec83';
  $display->storage_type = 'page_manager';
  $display->storage_id = 'node_view_panel_context';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-875a6c6f-4530-4b62-93ee-0a43252a63fd';
  $pane->panel = 'middle';
  $pane->type = 'node_content';
  $pane->subtype = 'node_content';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'links' => 1,
    'no_extras' => 1,
    'override_title' => 0,
    'override_title_text' => '',
    'identifier' => '',
    'link' => 0,
    'leave_node_title' => 0,
    'build_mode' => 'full',
    'context' => 'argument_entity_id:node_1',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '875a6c6f-4530-4b62-93ee-0a43252a63fd';
  $display->content['new-875a6c6f-4530-4b62-93ee-0a43252a63fd'] = $pane;
  $display->panels['middle'][0] = 'new-875a6c6f-4530-4b62-93ee-0a43252a63fd';
  $pane = new stdClass();
  $pane->pid = 'new-bcd44cad-f46f-4d44-8b4d-15f44b09abff';
  $pane->panel = 'middle';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => 'Sticky footer seperator',
    'title' => '',
    'body' => '<!-- container for sticky footer seperator, tooth  whatever -->',
    'format' => 'full_html',
    'substitute' => 1,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'sticky-footer-seperator',
  );
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = 'bcd44cad-f46f-4d44-8b4d-15f44b09abff';
  $display->content['new-bcd44cad-f46f-4d44-8b4d-15f44b09abff'] = $pane;
  $display->panels['middle'][1] = 'new-bcd44cad-f46f-4d44-8b4d-15f44b09abff';
  $pane = new stdClass();
  $pane->pid = 'new-90aa8aff-4f2d-4a69-b777-e60b378575be';
  $pane->panel = 'middle';
  $pane->type = 'views';
  $pane->subtype = 'more_issues';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_pager_settings' => 0,
    'use_pager' => 0,
    'nodes_per_page' => '3',
    'pager_id' => '0',
    'offset' => '0',
    'more_link' => 0,
    'feed_icons' => 0,
    'panel_args' => 0,
    'link_to_view' => 0,
    'args' => '',
    'url' => '',
    'display' => 'block',
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'sticky-footer',
  );
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = '90aa8aff-4f2d-4a69-b777-e60b378575be';
  $display->content['new-90aa8aff-4f2d-4a69-b777-e60b378575be'] = $pane;
  $display->panels['middle'][2] = 'new-90aa8aff-4f2d-4a69-b777-e60b378575be';
  $display->hide_title = PANELS_TITLE_PANE;
  $display->title_pane = 'new-875a6c6f-4530-4b62-93ee-0a43252a63fd';
  $handler->conf['display'] = $display;
  $export['node_view_panel_context'] = $handler;

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'node_view_panel_context_2';
  $handler->task = 'node_view';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = -27;
  $handler->conf = array(
    'title' => 'Spike Articles',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'node_type',
          'settings' => array(
            'type' => array(
              'article' => 'article',
            ),
          ),
          'context' => 'argument_entity_id:node_1',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
  );
  $display = new panels_display();
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = 'db1394cf-0f68-406c-adf1-f4d35f16ec83';
  $display->storage_type = 'page_manager';
  $display->storage_id = 'node_view_panel_context_2';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-875a6c6f-4530-4b62-93ee-0a43252a63fd';
  $pane->panel = 'middle';
  $pane->type = 'node_content';
  $pane->subtype = 'node_content';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'links' => 1,
    'no_extras' => 1,
    'override_title' => 0,
    'override_title_text' => '',
    'identifier' => '',
    'link' => 0,
    'leave_node_title' => 0,
    'build_mode' => 'full',
    'context' => 'argument_entity_id:node_1',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '875a6c6f-4530-4b62-93ee-0a43252a63fd';
  $display->content['new-875a6c6f-4530-4b62-93ee-0a43252a63fd'] = $pane;
  $display->panels['middle'][0] = 'new-875a6c6f-4530-4b62-93ee-0a43252a63fd';
  $pane = new stdClass();
  $pane->pid = 'new-0cd4ac74-ed13-4f29-91f9-0cf9a1bb59e2';
  $pane->panel = 'middle';
  $pane->type = 'block';
  $pane->subtype = 'service_links-service_links';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Share it!',
    'override_title_heading' => 'h4',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'share-clone',
  );
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '0cd4ac74-ed13-4f29-91f9-0cf9a1bb59e2';
  $display->content['new-0cd4ac74-ed13-4f29-91f9-0cf9a1bb59e2'] = $pane;
  $display->panels['middle'][1] = 'new-0cd4ac74-ed13-4f29-91f9-0cf9a1bb59e2';
  $pane = new stdClass();
  $pane->pid = 'new-bcd44cad-f46f-4d44-8b4d-15f44b09abff';
  $pane->panel = 'middle';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => 'Sticky footer seperator',
    'title' => '',
    'body' => '<!-- container for sticky footer seperator, tooth  whatever -->',
    'format' => 'full_html',
    'substitute' => 1,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
    'style' => 'spike_banded',
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'sticky-footer-seperator',
  );
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = 'bcd44cad-f46f-4d44-8b4d-15f44b09abff';
  $display->content['new-bcd44cad-f46f-4d44-8b4d-15f44b09abff'] = $pane;
  $display->panels['middle'][2] = 'new-bcd44cad-f46f-4d44-8b4d-15f44b09abff';
  $pane = new stdClass();
  $pane->pid = 'new-77b2b132-a5e9-4989-a817-2bc7143c2864';
  $pane->panel = 'middle';
  $pane->type = 'views';
  $pane->subtype = 'spike_front_page';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_pager_settings' => 0,
    'use_pager' => 1,
    'nodes_per_page' => '14',
    'pager_id' => '0',
    'offset' => '0',
    'more_link' => 0,
    'feed_icons' => 0,
    'panel_args' => 0,
    'link_to_view' => 0,
    'args' => '',
    'url' => '',
    'display' => 'default',
    'context' => array(
      0 => '',
    ),
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'sticky-footer-loadmore',
  );
  $pane->extras = array();
  $pane->position = 3;
  $pane->locks = array();
  $pane->uuid = '77b2b132-a5e9-4989-a817-2bc7143c2864';
  $display->content['new-77b2b132-a5e9-4989-a817-2bc7143c2864'] = $pane;
  $display->panels['middle'][3] = 'new-77b2b132-a5e9-4989-a817-2bc7143c2864';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-875a6c6f-4530-4b62-93ee-0a43252a63fd';
  $handler->conf['display'] = $display;
  $export['node_view_panel_context_2'] = $handler;

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'node_view_panel_context_3';
  $handler->task = 'node_view';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = -26;
  $handler->conf = array(
    'title' => 'Image Article',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'node_type',
          'settings' => array(
            'type' => array(
              'image_article' => 'image_article',
            ),
          ),
          'context' => 'argument_entity_id:node_1',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
  );
  $display = new panels_display();
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = 'db1394cf-0f68-406c-adf1-f4d35f16ec83';
  $display->storage_type = 'page_manager';
  $display->storage_id = 'node_view_panel_context_3';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-875a6c6f-4530-4b62-93ee-0a43252a63fd';
  $pane->panel = 'middle';
  $pane->type = 'node_content';
  $pane->subtype = 'node_content';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'links' => 1,
    'no_extras' => 1,
    'override_title' => 1,
    'override_title_text' => '',
    'identifier' => '',
    'link' => 0,
    'leave_node_title' => 0,
    'build_mode' => 'full',
    'context' => 'argument_entity_id:node_1',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '875a6c6f-4530-4b62-93ee-0a43252a63fd';
  $display->content['new-875a6c6f-4530-4b62-93ee-0a43252a63fd'] = $pane;
  $display->panels['middle'][0] = 'new-875a6c6f-4530-4b62-93ee-0a43252a63fd';
  $pane = new stdClass();
  $pane->pid = 'new-fe84c7ae-a4ea-43c4-a773-412a683d1971';
  $pane->panel = 'middle';
  $pane->type = 'block';
  $pane->subtype = 'service_links-service_links';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Share it!',
    'override_title_heading' => 'h4',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'share-clone',
  );
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = 'fe84c7ae-a4ea-43c4-a773-412a683d1971';
  $display->content['new-fe84c7ae-a4ea-43c4-a773-412a683d1971'] = $pane;
  $display->panels['middle'][1] = 'new-fe84c7ae-a4ea-43c4-a773-412a683d1971';
  $pane = new stdClass();
  $pane->pid = 'new-bcd44cad-f46f-4d44-8b4d-15f44b09abff';
  $pane->panel = 'middle';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => 'Sticky footer seperator',
    'title' => '',
    'body' => '<!-- container for sticky footer seperator, tooth  whatever -->',
    'format' => 'full_html',
    'substitute' => 1,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
    'style' => 'spike_banded',
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'sticky-footer-seperator',
  );
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = 'bcd44cad-f46f-4d44-8b4d-15f44b09abff';
  $display->content['new-bcd44cad-f46f-4d44-8b4d-15f44b09abff'] = $pane;
  $display->panels['middle'][2] = 'new-bcd44cad-f46f-4d44-8b4d-15f44b09abff';
  $pane = new stdClass();
  $pane->pid = 'new-3d92fef7-a2ec-4a9e-ac09-f11a9f9dff3e';
  $pane->panel = 'middle';
  $pane->type = 'views';
  $pane->subtype = 'spike_front_page';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_pager_settings' => 0,
    'use_pager' => 1,
    'nodes_per_page' => '14',
    'pager_id' => '0',
    'offset' => '0',
    'more_link' => 0,
    'feed_icons' => 0,
    'panel_args' => 0,
    'link_to_view' => 0,
    'args' => '',
    'url' => '',
    'display' => 'default',
    'context' => array(
      0 => '',
    ),
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'sticky-footer-loadmore',
  );
  $pane->extras = array();
  $pane->position = 3;
  $pane->locks = array();
  $pane->uuid = '3d92fef7-a2ec-4a9e-ac09-f11a9f9dff3e';
  $display->content['new-3d92fef7-a2ec-4a9e-ac09-f11a9f9dff3e'] = $pane;
  $display->panels['middle'][3] = 'new-3d92fef7-a2ec-4a9e-ac09-f11a9f9dff3e';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-875a6c6f-4530-4b62-93ee-0a43252a63fd';
  $handler->conf['display'] = $display;
  $export['node_view_panel_context_3'] = $handler;

  return $export;
}
