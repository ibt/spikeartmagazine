<?php

/**
 * @file spike_node_pages.pages.inc
 */




function info_page_Advert_callback() {
  GLOBAL $language; $lang = $language->language;
  $elements = array(
    '#type' => 'markup',
    'elements' => array(
      array( '#markup' => info_page_Advert_El_1($lang) ),
      array( '#markup' => info_page_Advert_El_2($lang) ),
      array( '#markup' => info_page_Advert_El_3($lang) ),
      array( '#markup' => info_page_Advert_El_4($lang) ),
      array( '#markup' => info_page_Advert_El_5($lang) ),
    )
  );
  $variables = array(
    '#theme' => 'render_spike_page_element',
    '#prefix' => '<div class="spike-page-stripes">',
    '#suffix' => '</div></div>',
    '#color-1' => 'green',
    '#color-2' => 'white',
    '#spike-pos' => 2, // 1 is odd, 2 is even (positioned stripes)
  );
  $merge = array_merge($elements, $variables);
  $render = drupal_render($merge);
  return $render;
}


/**
 * Section Theme function.
 */
function theme_render_spike_page_element($vars) {
  $elements = $vars['element']['elements'];
  $markup = '';
  $spike_pos = $vars['element']['#spike-pos'];
  foreach ($elements as $key=>$val) {
    $color = $key % 2 == 0 ? $vars['element']['#color-1'] : $vars['element']['#color-2'];
    $classes = 'spike-page-stripe stripe-' . ($key + 1) . ' ' . $color . '-strip';
    $spike_band = (($spike_pos * 1000) - $key + 1) % 2 == 0 ? TRUE : FALSE; // add spikes bool
    if ($spike_band) {
      $prefix = '<div class="spiked-band spiked-band-top"></div><div class=" ' . $classes . '"><div class="stripe-inner">';
      $suffix = '</div></div><div class="spiked-band spiked-band-bottom"></div>';
    } else {
      $prefix = '<div class=" ' . $classes . '"><div class="stripe-inner">';
      $suffix = '</div></div>';
    }
    $classes = 'page-el';
    $markup .= $prefix . '<div class="' . $classes . '">' . $val['#markup'] . '</div>' . $suffix;

  }
  return $markup;
}
