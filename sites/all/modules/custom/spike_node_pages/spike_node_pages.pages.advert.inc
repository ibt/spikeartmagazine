<?php

/**
 * @file spike_node_pages.pages.advert.inc
 */

GLOBAL $base_url;
define('BASEPATH', $base_url);

/**
 * Section markup output
 * Section 1 AD CONTACT
 *
 * @param $lang
 * @return string
 */
function info_page_Advert_El_1($lang) {
  $markup = '<h1>' . t('Advertise') . '</h1>';
  $markup .= '<div class="video stack-el">'
    . '<iframe src="https://player.vimeo.com/video/162504216" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>'
    . '</div>';
  $markup .= '<h4>' . t('Contact') . '</h4>';
  $markup .= '<p>' . 'Susanna Hoffmann-Ostenhof<br>+43 699 181 93 506<br><a href="mailto:susanna.ostenhof@spikeartmagazine.com">susanna.ostenhof@spikeartmagazine.com</a></p>';
  return $markup;
}

/**
 * Section markup output
 * Section 2 AD DETAILS
 *
 * @param $lang
 * @return string
 */
function info_page_Advert_El_2($lang) {
  $markup = '<h4 class="stripe-title">' . t('Spike Art Quarterly') . '</h4>';
  $markup .= '<p>' . t('Release dates and general information') . '</p>';
  $markup .= '<div class="dates">';
    $markup .= '<span class="issue-item">' . '<strong>#48 ' . t('Summer 2016'). '</strong><br>Publishing date: 13 June 2016<br>Deadline for print data: <br>23 May 2016' . '</span>';
    $markup .= '<span class="issue-item">' . '<strong>#49 ' . t('Autumn 2016'). '</strong><br>Publishing date: 7 Sept. 2016<br>Deadline for print data: <br>15 Aug. 2016' . '</span>';
    $markup .= '<span class="issue-item">' . '<strong>#50 ' . t('Winter 2016/2016'). '</strong><br>Publishing date: 5 Dec. 2016<br>Deadline for print data: <br>18 Nov. 2016' . '</span>';
//    $markup .= '<span class="issue-item">' . '<strong>#46 ' . t('Winter'). '</strong> 20.2.2016' . '</span>';
  $markup .= '</div>';
  $markup .= '<div class="ad-sizes stack-el">';
  $img_path = BASEPATH . '/sites/all/themes/spike/images/info-pages/ad-sizes-all.gif';
    $markup .= '<img src="' . $img_path . '">';
  $markup .= '</div>';
  $markup .= '<p>' . t('circulation 20,000, international distribution, bilingual (German and English), ~ 180 pages, 4 issues / year, all ads are printed in 4-color, the format of the magazine is 217 x 280 mm, all ads come with 3mm bleed.')
    . '</p>';
  return $markup;
}

/**
 * Section markup output
 * Section 3 DOWNLOADS
 *
 * @param $lang
 * @return string
 */
function info_page_Advert_El_3($lang) {
  $img_path = BASEPATH . '/sites/all/themes/spike/images/info-pages/pdf_symbol-01-small.png';
  $markup = '<h4 class="stripe-title">' . t('Downloads') . '</h4>';
  $markup .= '<div class="downloads stack-el inline-wrapper">';
    $link_attr = array('html' => TRUE);
    $file_1 = 'http://spikeartmagazine.com/sites/default/files/pages/files/spike_media_kit_2016_0.pdf';
    $file_2 = 'http://spikeartmagazine.com/sites/default/files/pages/files/spike_media_kit_2016_galleries_and_institutions_0.pdf';
    $file_3 = 'http://spikeartmagazine.com/sites/default/files/pages/files/spike_terms_of_trade.pdf';
    $link_1 = l('<img src="' . $img_path  . '"><h5>' . t('Media Kit 2016'). '</h5>', $file_1, $link_attr);
    $link_2_text = '<h5>' . t('Media Kit 2016'). '</h5><p>' . t('(Galleries and cultural institutions)') . '</p>';
    $link_2 = l('<img src="' . $img_path  . '">' . $link_2_text, $file_2, $link_attr);
    $link_3 = l('<img src="' . $img_path  . '"><h5>' . t('Terms of Trade'). '</h5>', $file_3, $link_attr);
    $markup .= '<span class="issue-item inline-item"><div class="inner">'. $link_1 . '</div></span>';
    $markup .= '<span class="issue-item inline-item"><div class="inner">'. $link_2 . '</div></span>';
    $markup .= '<span class="issue-item inline-item"><div class="inner">'. $link_3 . '</div></span>';
  $markup .= '</div><div class="clearfix"></div>';
  return $markup;
}

/**
 * Section markup output
 * Section 4 AD SCREEN EXAMPLES
 *
 * @param $lang
 * @return string
 */
function info_page_Advert_El_4($lang) {
  $markup = '<h4 class="stripe-title">' . t('Spike Art Daily') . '</h4>';
  $markup .= '<p>' . t('The bilingual online magazine (German and English) offers new forms of art criticism, integrating social phenomena, pop, politics, current affairs and the latest gossip. It also breaks new ground in online marketing.')
    . '</p>';
  $markup .= '<div class="elements">';
    $img_path = BASEPATH . '/sites/all/themes/spike/images/info-pages/';
    $img_1 = '<img src="' . $img_path . 'spike_mac_customized_button-1.jpg">';
    $img_2 = '<img src="' . $img_path . 'spike_ipad_customized_cover_button.jpg">';
    $img_3 = '<img src="' . $img_path . 'spike_mac_partner.jpg">';
    $img_4 = '<img src="' . $img_path . 'example-screen-3.png">';
    $markup .= '<div class="inline-wrapper el-1"><div class="inline left">' . $img_1 . '</div><div class="inline right"><h6>' . t('Customized Button') . '</h6><p>';
      $markup .= t('Customized buttons build on the format of the online advertising button. They display the name of your institution or company and a teaser written in collaboration with our staff. The button will be placed among editorial content on Spike’s home page and will bring readers to your website when clicked.');
    $markup .= '</p></div></div>';
    $markup .= '<div class="inline-wrapper el-2"><div class="inline left">' . $img_2 . '</div><div class="inline right"><h6>' . t('Customized Cover Button') . '</h6><p>';
      $markup .= t('Customized cover buttons give two institutions or companies the exclusive opportunity to present themselves not only on Spike’s home page but directly on the cover of the website.');
    $markup .= '</p></div></div>';
    $markup .= '<div class="inline-wrapper el-3"><div class="inline left">' . $img_3 . '</div><div class="inline right"><h6>' . t('Partner Ad') . '</h6><p>';
      $markup .= t('An organic integration into Spike’s editorial content. A box on the home page serves as the entrance to a space that can be “rented” on a monthly basis. This a place for you to put content which can then be shared, tweeted, liked, and reposted.');
    $markup .= '</p></div></div>';
    $markup .= '<div class="inline-wrapper el-4"><div class="inline left">' . $img_4 . '</div><div class="inline right"><h6>' . t('Newsletter Advertorial') . '</h6><p>';
      $markup .= t('Circulation approximately ten times per year to about 15,000 international recipients');
    $markup .= '</p></div></div>';
  $markup .= '</div>';
  return $markup;
}

/**
 * Section markup output
 * Section 5 DOWNLOADS
 *
 * @param $lang
 * @return string
 */
function info_page_Advert_El_5($lang) {
  return info_page_Advert_El_3($lang);
}