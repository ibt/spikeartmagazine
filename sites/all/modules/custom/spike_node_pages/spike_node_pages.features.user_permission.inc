<?php
/**
 * @file
 * spike_node_pages.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function spike_node_pages_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create page_paragraphs content'.
  $permissions['create page_paragraphs content'] = array(
    'name' => 'create page_paragraphs content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any page_paragraphs content'.
  $permissions['delete any page_paragraphs content'] = array(
    'name' => 'delete any page_paragraphs content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own page_paragraphs content'.
  $permissions['delete own page_paragraphs content'] = array(
    'name' => 'delete own page_paragraphs content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'operator' => 'operator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any page_paragraphs content'.
  $permissions['edit any page_paragraphs content'] = array(
    'name' => 'edit any page_paragraphs content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'operator' => 'operator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own page_paragraphs content'.
  $permissions['edit own page_paragraphs content'] = array(
    'name' => 'edit own page_paragraphs content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'operator' => 'operator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'view any unpublished page_paragraphs content'.
  $permissions['view any unpublished page_paragraphs content'] = array(
    'name' => 'view any unpublished page_paragraphs content',
    'roles' => array(),
    'module' => 'view_unpublished',
  );

  return $permissions;
}
