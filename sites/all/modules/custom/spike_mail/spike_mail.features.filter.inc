<?php
/**
 * @file
 * spike_mail.features.filter.inc
 */

/**
 * Implements hook_filter_default_formats().
 */
function spike_mail_filter_default_formats() {
  $formats = array();

  // Exported format: email_filter.
  $formats['email_filter'] = array(
    'format' => 'email_filter',
    'name' => 'email_filter',
    'cache' => 1,
    'status' => 1,
    'weight' => 0,
    'filters' => array(
      'filter_emogrifier' => array(
        'weight' => 0,
        'status' => 1,
        'settings' => array(),
      ),
      'pathologic' => array(
        'weight' => 50,
        'status' => 1,
        'settings' => array(
          'local_paths' => '',
          'protocol_style' => 'full',
        ),
      ),
    ),
  );

  return $formats;
}
