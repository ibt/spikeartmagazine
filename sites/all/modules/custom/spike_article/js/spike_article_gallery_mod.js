// @file spike_article_gallery_mod.js

(function($) {

  Drupal.behaviors.spikeArticleGalleryMod = {

    attach: function (context, settings) {
      var $imgField = $('.main-container .pane-node-content').find('.field-name-field-image');
      var $bodyField = $('.main-container .pane-node-content').find('.field-name-body');
      // print page
      /*
       // move all colorbox active top images after the first into duplicate inline positions
       */



      $('.main-container .pane-node-content', context).once(function() {
        // set images for colorbox


        // get images - we leave image 1 in place.
        var images = [];
        // $imgField.find('.field-item').not(":eq(0)").each(function() {
        $imgField.find('.field-item').addClass('insert-image').not(":eq(0)").each(function() {
          images.push($(this));
        });

        // replace inline images UPDATE VERSION 9.May.2018
        targets = $bodyField.find("p:contains('_______INSERT_______')");
        $.each(images, function(i){
          if (typeof targets[i] !== 'undefined') {
            $(targets[i]).html('').append(this);
          }
        });
        // console.log(this);

        // $bodyField.find("p:contains('_______INSERT_______')").each(function() {
        //   //          $(this).prepend(images.shift());
        //   // the array is defined and has at least one element
        //   if (typeof images !== 'undefined' && images.length > 0) {
        //     images.shift().insertBefore($(this)).addClass('insert-image');
        //     $(this).remove();
        //   }
        // });


        // replace inline images LEGACY VERSION
        $bodyField.find('.image-caption-insert').each(function() {
          $(this).children('img').remove();
          $(this).prepend(images.shift());
        });

      });
      /* end .once('main-container') */



      /*
       * DO THE SAME FOR THE PRINT PAGE
       */
      $('.page-article-print', context).once(function() {
        var $imgPrint = $('.page-article-print .node.view-mode-full').find('.field-name-field-image');
        var $bodyPrint = $('.page-article-print .node.view-mode-full').find('.field-name-body');
        // get cb ready elements
        var images = [];
        $imgPrint.find('.field-item').not(":eq(0)").each(function() {
          console.log ('load array');
          images.push($(this));
        });

        // replace inline images UPDATE VERSION
        $bodyPrint.find("p:contains('_______INSERT_______')").each(function() {
          console.log ('run on body text');
          images.shift().insertBefore($(this)).addClass('insert-image');
//          $(this).prepend(images.shift());
          $(this).remove();
        });

      });

    }
  };
})(jQuery);