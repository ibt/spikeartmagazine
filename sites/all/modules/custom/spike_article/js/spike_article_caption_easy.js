(function($) {
  Drupal.behaviors.spikeArticleCaptionEasy = {
    attach: function(context, settings) {
      /*
       * CREATES THE SCROLLING CAPTION EFFECT ON ARTICLES
       */

      // var $capWrapper = $('#article-captions-container');
      var $capWrapper = $(".view-id-article_sidebar_captions");
      /*
       * .once('main-container')
       */
      $("body", context).once(function() {
        var $node = $(".node-type-article .pane-content").find(".node");
        // prepare first cap 1for img cap wrapper
        // quick fix - remove this class from from footer
        $(".sticky-footer .field-item").removeClass("insert-image");

        // Index the captions, copy them into place under main images,
        // and init scroll effect.
        var $captions = $(
          ".view-id-article_sidebar_captions .image-field-caption"
        );
        var $imgSidebar = $(".view-id-article_sidebar_captions img");
        $imgSidebar.each(function(i) {
          // To find the real caption index, count the previous image.
          // This is the accurate number, b/c not all images have captions.
          let $img = $(this);
          let $cap = $img.next("blockquote");
          var attribute = "img-cap-pair-" + i;
          $cap
            .addClass(attribute)
            .addClass("img-cap")
            .attr("img-cap-pair", i);
          // if (i != 0) {
          //   // $(this).addClass('hidden');
          // }
        });
        $node.find(".insert-image").each(function(i) {
          var $wrapper = $(this);
          let attribute = "img-cap-pair-" + i;
          $wrapper.find("img").each(function() {
            var $img = $(this);
            $img.addClass(attribute);
            $img.attr("img-cap-pair", attribute);
            var $clone = $("[img-cap-pair=" + i + "]").clone();
            // var $clone = $captions.attr("img-cap-pair", i).clone();
            $clone.insertAfter($img);
          });
        });

        var items = $node.find(".insert-image img");
        $node.find(".insert-image img").each(function(i) {
          var img = $(this);
          // var wrapper = $(this).parent();
          $(window).scroll(function() {
            /* Check the location of each desired element */
            // find corresponding caption
            // var imgClass = '.'+ img.attr('class').split(' ')[1];
            var imgClass = "." + img.attr("img-cap-pair");
            var top_of_object = img.position().top;
            var bottom_of_object = img.position().top + img.outerHeight();
            var top_of_window = $(window).scrollTop();
            var bottom_of_window = $(window).scrollTop() + $(window).height();

            /* fade in caption when object top reaches top caption container */
            //              /* If the object top is above bottom, fade in the caption */
            $("body")
              .find($capWrapper)
              .find(imgClass)
              .each(function() {
                var $cap = $(this);
                if (
                  top_of_object < bottom_of_window &&
                  top_of_object <= $capWrapper.offset().top &&
                  bottom_of_object > top_of_window
                ) {
                  $cap.removeClass("invisible").addClass("visible");
                  // $cap.fadeIn("fast");
                  // console.log(imgClass + "somehting");
                } else {
                  if ($cap.hasClass("visible")) {
                    $cap.removeClass("visible").addClass("invisible");
                    // $cap.fadeOut("fast");
                  }
                }
              });
          });
        });
      });
      /* end .once('main-container') */
    }
  };
})(jQuery);
