(function($) {

  Drupal.behaviors.spikeCaptionInsert = {

    attach: function (context, settings) {

      var $imageField = $('.field-name-field-image');
      var $image = $imageField.find('td');
      var $submit = $('#article-node-form').find('.form-actions').children('.form-submit');

      /*
       * .once('main-container')
       */
      $('#article-node-form', context).once(function() {
//      $('.main-container', context).once(function() {

        $image.each(function() {
//          console.log('hit it');
          var $this = $(this);
          var $button = $this.find('.insert-button');
          var $widget = $this.find('.image-widget-data');
          var $textEl = $widget.children('.form-type-textfield');
          $textEl.hide();
          var $cap = $this.find('.text-format-wrapper').find('.form-textarea');
          // on image insert, copy captions text to title & alt
          $button.click(function() {
            $widget.children('.form-type-textfield').each(function() {
              var s = $cap.val();
              var asd;
              $(this).children('input').val($cap.val());
            })
          });
        });

        // on submit, clean up html in title & alt fields
        $submit.click(function() {
          $image.each(function() {
            var $this = $(this);
            var $widget = $this.find('.image-widget-data');
            var $cap = $this.find('.text-format-wrapper').find('.form-textarea');
            $widget.children('.form-type-textfield').each(function() {
              var html = $(this).children('input').val();
              if (
                html.length === 0
                  && $cap.val().length > 0
                ) {
                html = $cap.val();
              }
              var text = html.replace(/(<([^>]+)>)/ig," ").replace(/\s+/g, ' ');
              $(this).children('input').val(text)
            })
          });
        });

        $('.main-container').ajaxComplete(function() {
          $image.each(function() {
            $(this).find('.image-widget-data').children('.form-type-textfield').hide();
//            var $widget = $this.find('.image-widget-data');
          });
        });

      });
      /* end .once('main-container') */




    }
  };
})(jQuery);