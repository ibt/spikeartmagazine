<?php
/**
 * @file
 * spike_article.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function spike_article_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|article|basic_info';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'article';
  $ds_fieldsetting->view_mode = 'basic_info';
  $ds_fieldsetting->settings = array(
    'basic_node_info' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|article|basic_info'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|article|full';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'article';
  $ds_fieldsetting->view_mode = 'full';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '2',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'auth_by_line' => array(
      'weight' => '4',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|article|full'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|article|mini_teaser';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'article';
  $ds_fieldsetting->view_mode = 'mini_teaser';
  $ds_fieldsetting->settings = array(
    'post_date' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'ds_post_date_time_ago',
    ),
    'title_link' => array(
      'weight' => '2',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|article|mini_teaser'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|article|related_content';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'article';
  $ds_fieldsetting->view_mode = 'related_content';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '1',
        'wrapper' => 'h4',
        'class' => '',
        'exclude node title settings' => '1',
      ),
    ),
  );
  $export['node|article|related_content'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|article|sidebar';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'article';
  $ds_fieldsetting->view_mode = 'sidebar';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '4',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '0',
        'wrapper' => '',
        'class' => '',
      ),
    ),
    'auth_by_line' => array(
      'weight' => '5',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'corr_article_language_version' => array(
      'weight' => '6',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'dynamic_date_issue' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|article|sidebar'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|article|teaser';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'article';
  $ds_fieldsetting->view_mode = 'teaser';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '3',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '1',
        'wrapper' => 'h4',
        'class' => 'article-title',
      ),
    ),
    'compound_info' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'print_article_extra' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|article|teaser'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function spike_article_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|article|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'article';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'first_img_cap',
        1 => 'auth_by_line',
        2 => 'title',
        3 => 'group_article_top',
        4 => 'field_ads',
        5 => 'field_title_sub',
        6 => 'field_image',
        7 => 'field_blurb',
        8 => 'body',
        9 => 'group_tags',
        10 => 'field_category',
        11 => 'field_tags',
      ),
    ),
    'fields' => array(
      'first_img_cap' => 'ds_content',
      'auth_by_line' => 'ds_content',
      'title' => 'ds_content',
      'group_article_top' => 'ds_content',
      'field_ads' => 'ds_content',
      'field_title_sub' => 'ds_content',
      'field_image' => 'ds_content',
      'field_blurb' => 'ds_content',
      'body' => 'ds_content',
      'group_tags' => 'ds_content',
      'field_category' => 'ds_content',
      'field_tags' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['node|article|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|article|form';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'article';
  $ds_layout->view_mode = 'form';
  $ds_layout->layout = 'ds_2col_stacked';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'title',
        1 => 'path',
        2 => 'group_blurb',
        3 => 'field_blurb',
        4 => 'field_title_sub',
        5 => 'field_blurb_visibility',
        6 => 'field_article_ref_issue',
        7 => 'field_display_extra_ref',
        8 => 'field_ads',
        9 => 'body',
      ),
      'right' => array(
        10 => 'language',
        11 => 'field_online_bool',
        12 => 'field_author',
        13 => 'field_category',
        14 => 'field_tags',
        15 => 'group_images',
        16 => 'field_image',
      ),
      'hidden' => array(
        17 => 'field_article_lang_version_ref',
        18 => 'rabbit_hole',
        19 => 'field_article_ref_article',
        20 => 'flag',
        21 => 'field_term_language',
        22 => 'group_extra',
        23 => 'metatags',
        24 => '_add_existing_field',
      ),
    ),
    'fields' => array(
      'title' => 'left',
      'path' => 'left',
      'group_blurb' => 'left',
      'field_blurb' => 'left',
      'field_title_sub' => 'left',
      'field_blurb_visibility' => 'left',
      'field_article_ref_issue' => 'left',
      'field_display_extra_ref' => 'left',
      'field_ads' => 'left',
      'body' => 'left',
      'language' => 'right',
      'field_online_bool' => 'right',
      'field_author' => 'right',
      'field_category' => 'right',
      'field_tags' => 'right',
      'group_images' => 'right',
      'field_image' => 'right',
      'field_article_lang_version_ref' => 'hidden',
      'rabbit_hole' => 'hidden',
      'field_article_ref_article' => 'hidden',
      'flag' => 'hidden',
      'field_term_language' => 'hidden',
      'group_extra' => 'hidden',
      'metatags' => 'hidden',
      '_add_existing_field' => 'hidden',
    ),
    'classes' => array(),
    'wrappers' => array(
      'header' => 'div',
      'left' => 'div',
      'right' => 'div',
      'footer' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => 0,
  );
  $export['node|article|form'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|article|full';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'article';
  $ds_layout->view_mode = 'full';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'group_article_top',
        1 => 'field_category',
        2 => 'field_image',
        3 => 'title',
        4 => 'field_title_sub',
        5 => 'field_blurb',
        6 => 'auth_by_line',
        7 => 'body',
        8 => 'field_ads',
        9 => 'group_tags',
        10 => 'field_tags',
      ),
    ),
    'fields' => array(
      'group_article_top' => 'ds_content',
      'field_category' => 'ds_content',
      'field_image' => 'ds_content',
      'title' => 'ds_content',
      'field_title_sub' => 'ds_content',
      'field_blurb' => 'ds_content',
      'auth_by_line' => 'ds_content',
      'body' => 'ds_content',
      'field_ads' => 'ds_content',
      'group_tags' => 'ds_content',
      'field_tags' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
    'hide_page_title' => '1',
    'page_option_title' => '',
  );
  $export['node|article|full'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|article|mini_teaser';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'article';
  $ds_layout->view_mode = 'mini_teaser';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_category',
        1 => 'post_date',
        2 => 'title_link',
        3 => 'field_image',
      ),
    ),
    'fields' => array(
      'field_category' => 'ds_content',
      'post_date' => 'ds_content',
      'title_link' => 'ds_content',
      'field_image' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|article|mini_teaser'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|article|related_content';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'article';
  $ds_layout->view_mode = 'related_content';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_image',
        1 => 'title',
      ),
    ),
    'fields' => array(
      'field_image' => 'ds_content',
      'title' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|article|related_content'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|article|sidebar';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'article';
  $ds_layout->view_mode = 'sidebar';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_category',
        1 => 'dynamic_date_issue',
        2 => 'title',
        3 => 'auth_by_line',
        4 => 'corr_article_language_version',
        5 => 'flag_legacy_captions',
      ),
    ),
    'fields' => array(
      'field_category' => 'ds_content',
      'dynamic_date_issue' => 'ds_content',
      'title' => 'ds_content',
      'auth_by_line' => 'ds_content',
      'corr_article_language_version' => 'ds_content',
      'flag_legacy_captions' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|article|sidebar'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|article|teaser';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'article';
  $ds_layout->view_mode = 'teaser';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'print_article_extra',
        1 => 'field_display_extra_ref',
        2 => 'compound_info',
        3 => 'field_image',
        4 => 'title',
        5 => 'field_blurb',
      ),
    ),
    'fields' => array(
      'print_article_extra' => 'ds_content',
      'field_display_extra_ref' => 'ds_content',
      'compound_info' => 'ds_content',
      'field_image' => 'ds_content',
      'title' => 'ds_content',
      'field_blurb' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['node|article|teaser'] = $ds_layout;

  return $export;
}

/**
 * Implements hook_ds_view_modes_info().
 */
function spike_article_ds_view_modes_info() {
  $export = array();

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'mini_teaser';
  $ds_view_mode->label = 'mini teaser';
  $ds_view_mode->entities = array(
    'node' => 'node',
  );
  $export['mini_teaser'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'related_content';
  $ds_view_mode->label = 'Related Content';
  $ds_view_mode->entities = array(
    'node' => 'node',
  );
  $export['related_content'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'sidebar';
  $ds_view_mode->label = 'sidebar';
  $ds_view_mode->entities = array(
    'node' => 'node',
  );
  $export['sidebar'] = $ds_view_mode;

  return $export;
}
