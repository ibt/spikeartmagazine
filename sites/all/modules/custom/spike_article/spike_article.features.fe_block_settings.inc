<?php
/**
 * @file
 * spike_article.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function spike_article_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['service_links-service_links'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'service_links',
    'module' => 'service_links',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bootstrap' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bootstrap',
        'weight' => 0,
      ),
      'ember' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'ember',
        'weight' => 0,
      ),
      'spike' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'spike',
        'weight' => -2,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['spike_caption_insert-caption_sidebar'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'caption_sidebar',
    'module' => 'spike_caption_insert',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bootstrap' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bootstrap',
        'weight' => 0,
      ),
      'ember' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'ember',
        'weight' => 0,
      ),
      'spike' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'spike',
        'weight' => -27,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views-160a9d9bc28a683d3d9371fec44bac4d'] = array(
    'cache' => 4,
    'custom' => 0,
    'delta' => '160a9d9bc28a683d3d9371fec44bac4d',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bootstrap' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bootstrap',
        'weight' => 0,
      ),
      'ember' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'ember',
        'weight' => 0,
      ),
      'spike' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'spike',
        'weight' => 14,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  return $export;
}
