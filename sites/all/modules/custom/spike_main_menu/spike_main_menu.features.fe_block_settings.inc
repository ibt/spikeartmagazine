<?php
/**
 * @file
 * spike_main_menu.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function spike_main_menu_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['spike_layout-spike_main_menu_block'] = array(
    'cache' => 8,
    'custom' => 0,
    'delta' => 'spike_main_menu_block',
    'module' => 'spike_layout',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bootstrap' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bootstrap',
        'weight' => 0,
      ),
      'ember' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'ember',
        'weight' => 0,
      ),
      'spike' => array(
        'region' => 'sidebar_first',
        'status' => 1,
        'theme' => 'spike',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  return $export;
}
