<?php
/**
 * @file
 * spike_main_menu.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function spike_main_menu_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_archive:<front>.
  $menu_links['main-menu_archive:<front>'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Archive',
    'options' => array(
      'attributes' => array(),
      'identifier' => 'main-menu_archive:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => -49,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: main-menu_content:<front>.
  $menu_links['main-menu_content:<front>'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Content',
    'options' => array(
      'identifier' => 'main-menu_content:<front>',
      'alter' => TRUE,
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => -50,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: main-menu_issues:issues.
  $menu_links['main-menu_issues:issues'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'issues',
    'router_path' => 'issues',
    'link_title' => 'Issues',
    'options' => array(
      'identifier' => 'main-menu_issues:issues',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
    'parent_identifier' => 'main-menu_:archive',
  );
  // Exported menu link: main-menu_shop:shop.
  $menu_links['main-menu_shop:shop'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'shop',
    'router_path' => 'shop',
    'link_title' => 'Shop',
    'options' => array(
      'identifier' => 'main-menu_shop:shop',
      'attributes' => array(),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: main-menu_subjects:subjects.
  $menu_links['main-menu_subjects:subjects'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'subjects',
    'router_path' => 'subjects',
    'link_title' => 'Subjects',
    'options' => array(
      'attributes' => array(),
      'identifier' => 'main-menu_subjects:subjects',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
    'parent_identifier' => 'main-menu_archive:<front>',
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Archive');
  t('Content');
  t('Issues');
  t('Shop');
  t('Subjects');

  return $menu_links;
}
