
(function($) {

  Drupal.behaviors.spikeShopTheme = {

    attach: function (context, settings) {

      /*
      shop - editions & external
      * make sure image & headlines line up
       */
      function nodeAlignBottom(nodes) {
        // find tallest element
        var t=0; // the height of the highest element (after the function runs)
        var t_elem;  // the highest element (after the function runs)
        var array = [];
        nodes.find('.field-name-title').each(function () {
          var $this = $(this);
          array.push($this.outerHeight());
          if ( $this.outerHeight() > t ) {
            t_elem=this;
            t=$this.outerHeight();
          }
        });
        nodes.find('.node').each(function(i, e) {
          var offset = t - array[i];
          $(this).css({
            'bottom': offset
          });
        });
      }
      $('.view-editions, .view-external-products', context).once(function() {
//        nodeAlignBottom($(this));
      });

      /*
      shop page - title manipulation
      * make the 'shop' headline appear next to each panel headline
       */
      var pageTitle = $('.page-header').clone();
      $('.page-header').css({
        'height': 0,
        'width': 0,
        'opacity': 0
      });

      $('.page-shop .panel-display', context).once(function() {
        var $wrapper = $(this);
        var headerTitle = $('title').text();
        // modify html title
//        $('title').text('Spike Shop '+ headerTitle);
        /* insert page header ("SHOP") before all panel titles but last */
        var title = $(this).find('.pane-title').not(":last");
        pageTitle.clone().addClass('page-title-clone').insertBefore(title);
        /* clone pane titles - we'll use these for smoothing in the scroll effect */
        $wrapper.find('.panel-pane').not(":last").each(function(i) {
          var pane = $(this);
          var paneTitle = $(this).find('.pane-title');
          paneTitle.clone().addClass('title-clone').insertBefore(paneTitle);
        });

        $(window).scroll(function() {
          var scrT = $(window).scrollTop();
          $wrapper.find('.panel-pane').not(":last").each(function(i) {
            var pane = $(this);
            var paneTitle = $(this).find('.pane-title').not('.title-clone');
            if (
              pane.isOnScreen()
              && pane.offset().top < scrT - 80
              && $('.sticky-footer-seperator').offset().top
                > $('.sticky-footer .pane-title').offset().top
              ) {
              pane.addClass('onScreen')
              paneTitle.addClass('sticky-panel-title');
            } else {
              paneTitle.removeClass('sticky-panel-title');
              pane.removeClass('onScreen');
  //                titleClone.remove();
            }
          });
        });

      });


      $.fn.isOnScreen = function(){

        var win = $(window);

        var viewport = {
          top : win.scrollTop(),
          left : win.scrollLeft()
        };
        viewport.right = viewport.left + win.width();
        viewport.bottom = viewport.top + win.height();

        var bounds = this.offset();
        bounds.right = bounds.left + this.outerWidth();
        bounds.bottom = bounds.top + this.outerHeight();

        return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));

      };


    }
  };
})(jQuery);