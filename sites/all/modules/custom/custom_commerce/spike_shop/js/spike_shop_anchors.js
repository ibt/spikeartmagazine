(function($) {

  Drupal.behaviors.spikeShopAnchors = {

    attach: function (context, settings) {



      /*
       * define anchors on the page and do nice scroll
       *
       * .once('main-container')
       */
      $('.main-container', context).once(function() {
        // get the anchor
        var segment_str = window.location.pathname;
        var segment_array = segment_str.split( '/' );
        var target = segment_array.pop();
        var target2nd = segment_array.pop();
        var position  = $('html').offset().top;
        var element = 'html';

        // positions
        switch(target) {

          case 'issues':
            element = '.pane-spike-issues-panel-pane-1';
//              position = $(".pane-spike-subscriptions-panel-pane-1").offset().top;
            break;
          case 'editions':
            element = '.pane-editions-panel-pane-1';
//              position = $(".pane-spike-subscriptions-panel-pane-1").offset().top;
            break;
          case 'external-products':
            element = '.pane-external-products-panel-pane-1';
//              position = $(".pane-spike-subscriptions-panel-pane-1").offset().top;
            break;
          case 'subscriptions':
            element = '.pane-spike-subscriptions-panel-pane-1';
//              position = $(".pane-spike-subscriptions-panel-pane-1").offset().top;
            break;

          default:
        }

        position = $(element).offset().top;

        if (target != 'shop') {
          // scroll to position named above
          $('html, body').animate({ scrollTop: position }, 1000);
        }


          $('.sidebar-subscribe').click(function(e) {
            $('html, body').animate({
              scrollTop: $(document).height()
            }, 1000);
            return false;
          });



      });
      /* end .once('main-container') */

    }
  };
})(jQuery);