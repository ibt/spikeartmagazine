(function($) {

  Drupal.behaviors.spikeShopCheckout = {

    attach: function (context, settings) {

      $('.page-cart', context).once(function() {

        $('.commerce-line-item-views-form').find('a').attr('target', '_blank');
      })


      var href = window.location.origin;
      var throb = 'throbber';
      var el = '.' + throb;
      var img_path = '/sites/all/themes/spike/images/throb_trans.gif';
      var img_wrap = '<div class="throb-wrapper"></div>'
      var img = '<img src='+ href + img_path + '>';
      var insert = '<div class=" ' + throb + '">';

      function shop_throbber() {

        $(document).ajaxStart(function(){
          $(el).remove();
          var winH = $(window).outerHeight();
          var winW = $(window).outerWidth();
          var w = 200;
          var h = w;
          $('body').append(insert);
          $(el).append(img_wrap);
          var $img = $(el + ' .throb-wrapper');
//          $('.throb-wrapper').append(img);
          $(el).css({
            'position': 'fixed',
            'width': winW,
            'height': winH,
            'background': 'rgba(255, 255, 255, 0.4)',
            'z-index': '999'
          });
          $img.css({
            'position': 'fixed',
            'top': (winH / 2) - (h/2),
            'left': (winW / 2) - (w/2),
            'width': w,
            'height': h,
            'background-image': 'url(" ' + href + img_path + '")',
            'background-size': 'contain',
            'background-repeat': 'no-repeat',
            'z-index': '999'
          });
        });

        $(document).ajaxSuccess(function(){
          $(el).fadeOut().remove();
        });
      }


      /*
       * .once('main-container')
       */
      $('.main-container', context).once(function() {
        var array = [
          $('#customer-profile-billing-commerce-customer-address-add-more-wrapper ' +
            '.form-type-select select.country'),
          $('#customer-profile-shipping-commerce-customer-address-add-more-wrapper ' +
            '.form-type-select select.country')
        ];
        $.each(array, function() {
          this.click(function() {
            shop_throbber();
          })
        })

      });
      /* end .once('main-container') */


      $('.form-checkbox').on('checkbox-shipping', function(event,  $parent) {
        console.log('find this');
        var $box = $(this);
        var $company = $parent.siblings('.field-type-addressfield').find('.organisation-name').parent();
        var $vat = $parent.siblings('.field-name-field-vat-nr');
        var checked = function($el) {
          return $el.prop('checked');
        };
        var countryState = function() {
          var test = checked($box);
          if (test == false) {
            $company.hide();
            $vat.hide();
          }
        }
        var stateClick = function() {
          var test = checked($box);
          if (test) {
//          console.log('unhide this');
            $company.slideDown();
            $vat.slideDown();
          } else {
//          console.log('hide this');
            $company.slideUp().children().val('');
            $vat.slideUp().find('input').val('');
          }
        }
        countryState();
        $box.click(function() {
          stateClick();
        })
        // if company or vat fields have a value, then check the box
        var compID = 'edit-customer-profile-billing-commerce-customer-address-und-0-organisation-name';
        var vatID = 'edit-customer-profile-billing-field-vat-nr-und-0-value';
        var compInput = document.getElementById(compID);
        var vatInput = document.getElementById(vatID);
        if (
          (compInput && compInput.value)
          || (vatInput && vatInput.value)
          ) {
          $box.prop('checked', true);
          $company.show()
          $vat.show()
        }
      });

      /*
       checkout page modification
       */
      var $checkboxes = $('.form-checkbox');
      var billCheck = 'form-item-customer-profile-billing-company-check';

      var selectItems = function() {
        $checkboxes.each(function() {
          var $parent = $(this).parents();
          if ( $parent.hasClass(billCheck) ) {
            $(this).trigger('checkbox-shipping', [$parent]);
          }
        });
      }

      $('.page-checkout', context).once(function() {

        selectItems();
        $(document).ajaxComplete(function() {
          selectItems()
        })


      });


    }
  };
})(jQuery);
