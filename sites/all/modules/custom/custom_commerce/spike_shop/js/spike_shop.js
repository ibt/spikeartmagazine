(function($) {

  Drupal.behaviors.spikeShopSlideshow = {

    attach: function (context, settings) {

      var slideW = 300; // plus padding
      var padding = 80;
      var hover = false;

//      var $slideshow = $('.pane-products-recommended-panel-pane-1').find('.view-products-recommended .view-content');
      var $slideshow = $('.pane-products-recommended-panel-pane-1').find('.view-products-recommended');



      /* MAKE SLIDER OBJECT */
      function slideShow(slides) {
        var maxDisplay = 5; // display number
        this.timeout = 10000;
        slideCountController(slides, maxDisplay);
        this.index = 0;
        this.slides = slides;
      }

      /* MAKE SURE THERE ARE ENOUGH SLIDES */
      function slideCountController(slides, maxDisplay) {
        var v = maxDisplay; // visible elements
        if (slides.length == 0) return false;
        if (slides.length < v) {
          var dif = v - slides.length;
          for(var i = 0; dif > i; i++) {
            slides.push(slides[i].clone());
          }
        }
      }

      $slideshow.on('center-slider', function(event, slides) {

        var $slider = $(this).children('#slider');
        var $inner = $slider.children('.inner');
        // center slides
        var posLeft = $inner.position().left;
        var innerW = posLeft - padding;
        var dif = $inner.width() - innerW;
        var coordinates = [];
        $inner.find('.views-row').each(function() {
          if (dif <= 0) { // elements larger than
            var dist = ($(this).position().left) + (dif / 2);
            $(this).css({ left: dist });
          } else {
            // center
            // @ TODO
//            var dist = ($(this).position().left) + (dif / 2);
            var dist = ($(this).position().left) - (dif);

//            $(this).css({ left: dist });
            var asdf;
          }
          // assemble animation coordinates
          coordinates.push($(this).position().left);
        });
        slides.coordinates = coordinates.reverse();
        var pop = slides.coordinates.pop();
        slides.coordinates.unshift(pop);


      });

      /*
       * SLIDE SHOW SETTER
       */
      $slideshow.on('set-slider', function(event, slides) {

        var $slider = $(this).children('#slider');
        var $inner = $slider.children('.inner');
        // positioning
        var posTop = $inner.position().top;
        var posLeft = $inner.position().left;
        // place slide
        $inner.hide();
        for (var i = 0; i <  slides.slides.length; i++, posLeft = posLeft + slideW + padding) {
          $inner.append(slides.slides[i]);
          slides.slides[i].css({ top: posTop, left: posLeft, position: 'absolute'});
//          slides.slides[i].css({ top: posTop - 30, left: posLeft, position: 'absolute'});
        }
        $inner.fadeIn('50');
        $('.view-products-recommended').trigger('center-slider', [slides]);

        var innerW = posLeft - padding;
        var dif = $inner.width() - innerW;
        var coordinates = [];
        $inner.find('.views-row').each(function() {
          var asdf;
          if (dif <= 0) { // elements larger than
            var dist = ($(this).position().left) + (dif / 2);
            $(this).css({ left: dist });
//            if(!$(this).isOnScreen()) {
//              // @TODO fade elements at the edge
//              //              $(this).css({opacity: '.5'});
//            }
          } else {
            // center
            // @ TODO
            var dist = ($(this).position().left) - (dif / 2);
          }
          // get variable height of slide title,
          // use this to achieve flush bottom row of images
//          var titleH = $(this).outerHeight();
          var titleH = $(this).find('.field-name-title').outerHeight();
          var elTop = $(this).css('top').replace('px', '');
          $(this).css({
            top: parseInt(elTop) + titleH - 60
          });

//          console.log(elTop + titleH);
          // assemble animation coordinates
          coordinates.push($(this).position().left);
        });
        slides.coordinates = coordinates.reverse();
        var pop = slides.coordinates.pop();
        slides.coordinates.unshift(pop);


        /*
         * SLIDE MOVE TRIGGER LOOP
         */

//        var timeout = 40000000;
        var timeout = 4000;
        var action = function() {
          var asdf;
          if(hover) return;
          var coord = slides.coordinates;
          for (var e = 0; e < slides.slides.length; e++) {
            var pos = coord[e];
            var eSlide = slides.slides[e];
            if (e == slides.index) {
              eSlide.css({ left: pos - slideW - padding});
              eSlide.animate({ left: pos }, '50');
            } else {
              eSlide.animate({ left: pos }, '300' );
            }
          }

          var pop = coord.pop();
          coord.unshift(pop);


          slides.index++;
          if (slides.index === slides.slides.length) slides.index = 0;
        };

        $slider.hover(
          function() {
            hover = true;
          },
          function() {
            hover = false;
          }
        );

        setInterval(action, timeout);
        action();


      });




      /*
       * .once('main-container')
       */
      $('.main-container', context).once(function() {

        var $view = $('.view-products-recommended');
        var slider = '<div id="slider"><div class="inner"></div></div>';

//        $slideshow.webTicker(settings);


        $slideshow.prepend(slider).children('.view-content').hide();
        var slides = [];
        $slideshow.find('.views-row').each(function() {
          slides.push($(this));
        });
        var mySlides = new slideShow(slides);

        $slideshow.trigger('set-slider', [mySlides]);

//        window.onresize = function(event) {
        $(window).resize(function() {
            var asdf;
//            $('.view-
// s-recommended').trigger('set-slider', [mySlides]);
  //          $('.main-container').trigger('stacker-center-elements', [myStack]);

        });

//        var rtime = new Date(1, 1, 2000, 12,00,00);
//        var timeout = false;
//        var delta = 200;
//
//        function resizeend() {
//          if (new Date() - rtime < delta) {
//            setTimeout(resizeend, delta);
//          } else {
//            timeout = false;
//            $('.view-products-recommended').trigger('set-slider', [mySlides]);
//          }
//        }


//        };

      });
      /* end .once('main-container') */





//      /*
//       * .once FLIPPER
//       */
//      $('.node', context).once(function() {
//        var $node = $(this);
//        $(this).find('#flip-content').each(function() {
//        var $el = $(this);
//        var $front = $el.children('#side-1');
//        var $back = $el.children('#side-2');
//        $back.hide().css('left', 0);
//          $node.hover(function () {
//            $el.find('div').stop().rotate3Di('flip', 250, {direction: 'clockwise', sideChange: mySideChange});
//            $el.parents('.node').find('.field-name-field-display-extra-ref').fadeOut();
//          },
//          function () {
//            $el.find('div').stop().rotate3Di('unflip', 500, {sideChange: mySideChange});
//            $el.parents('.node').find('.field-name-field-display-extra-ref').fadeIn();
//          });
//        });
//      });




    }
  };
})(jQuery);