<?php
/**
 * @file
 * spike_shop.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function spike_shop_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'shop';
  $page->task = 'page';
  $page->admin_title = 'Shop';
  $page->admin_description = 'Shop page composed of various product groups';
  $page->path = 'shop';
  $page->access = array(
    'type' => 'none',
    'settings' => NULL,
  );
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_shop_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'shop';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => 'shop-page',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Shop';
  $display->uuid = 'f1928441-69e3-4ce1-9d90-ebad8e2a3e80';
  $display->storage_type = 'page_manager';
  $display->storage_id = 'page_shop_panel_context';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-944a27e1-8fef-47e2-ac00-36c7043b9cfb';
  $pane->panel = 'middle';
  $pane->type = 'views_panes';
  $pane->subtype = 'products_recommended-panel_pane_1';
  $pane->shown = FALSE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '944a27e1-8fef-47e2-ac00-36c7043b9cfb';
  $display->content['new-944a27e1-8fef-47e2-ac00-36c7043b9cfb'] = $pane;
  $display->panels['middle'][0] = 'new-944a27e1-8fef-47e2-ac00-36c7043b9cfb';
  $pane = new stdClass();
  $pane->pid = 'new-23dc4e3e-260b-46cc-936f-f09522153565';
  $pane->panel = 'middle';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = FALSE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => 'Spikes',
    'title' => '',
    'body' => '<div id="sticky-cover"></div><div id="sticky-shadow"></div>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => 'spikes-recommend',
    'css_class' => '',
  );
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '23dc4e3e-260b-46cc-936f-f09522153565';
  $display->content['new-23dc4e3e-260b-46cc-936f-f09522153565'] = $pane;
  $display->panels['middle'][1] = 'new-23dc4e3e-260b-46cc-936f-f09522153565';
  $pane = new stdClass();
  $pane->pid = 'new-8c090516-695d-4202-8267-beb3f6db5470';
  $pane->panel = 'middle';
  $pane->type = 'views_panes';
  $pane->subtype = 'spike_issues-panel_pane_1';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = '8c090516-695d-4202-8267-beb3f6db5470';
  $display->content['new-8c090516-695d-4202-8267-beb3f6db5470'] = $pane;
  $display->panels['middle'][2] = 'new-8c090516-695d-4202-8267-beb3f6db5470';
  $pane = new stdClass();
  $pane->pid = 'new-7e2be7b8-9d20-4005-90ec-f01df57f907c';
  $pane->panel = 'middle';
  $pane->type = 'views_panes';
  $pane->subtype = 'editions-panel_pane_1';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 3;
  $pane->locks = array();
  $pane->uuid = '7e2be7b8-9d20-4005-90ec-f01df57f907c';
  $display->content['new-7e2be7b8-9d20-4005-90ec-f01df57f907c'] = $pane;
  $display->panels['middle'][3] = 'new-7e2be7b8-9d20-4005-90ec-f01df57f907c';
  $pane = new stdClass();
  $pane->pid = 'new-0438abf2-3d33-4724-bf68-a747f8966554';
  $pane->panel = 'middle';
  $pane->type = 'views_panes';
  $pane->subtype = 'external_products-panel_pane_1';
  $pane->shown = FALSE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 4;
  $pane->locks = array();
  $pane->uuid = '0438abf2-3d33-4724-bf68-a747f8966554';
  $display->content['new-0438abf2-3d33-4724-bf68-a747f8966554'] = $pane;
  $display->panels['middle'][4] = 'new-0438abf2-3d33-4724-bf68-a747f8966554';
  $pane = new stdClass();
  $pane->pid = 'new-34ef2ba2-f019-417f-9329-b6277e62e818';
  $pane->panel = 'middle';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => 'Spike down',
    'title' => '',
    'body' => '<div class="spiked-band spiked-band-top invert"></div>',
    'format' => 'filtered_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 5;
  $pane->locks = array();
  $pane->uuid = '34ef2ba2-f019-417f-9329-b6277e62e818';
  $display->content['new-34ef2ba2-f019-417f-9329-b6277e62e818'] = $pane;
  $display->panels['middle'][5] = 'new-34ef2ba2-f019-417f-9329-b6277e62e818';
  $pane = new stdClass();
  $pane->pid = 'new-c4177ee2-eb68-42a5-bef5-7ba6e76ad397';
  $pane->panel = 'middle';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => 'Sticky footer seperator',
    'title' => '',
    'body' => '<!-- container for sticky footer seperator, tooth  whatever -->
<a name="subscriptions"></a>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'sticky-footer-seperator',
  );
  $pane->extras = array();
  $pane->position = 6;
  $pane->locks = array();
  $pane->uuid = 'c4177ee2-eb68-42a5-bef5-7ba6e76ad397';
  $display->content['new-c4177ee2-eb68-42a5-bef5-7ba6e76ad397'] = $pane;
  $display->panels['middle'][6] = 'new-c4177ee2-eb68-42a5-bef5-7ba6e76ad397';
  $pane = new stdClass();
  $pane->pid = 'new-d2dbf500-d511-4f5a-aab8-3458969bac05';
  $pane->panel = 'middle';
  $pane->type = 'views_panes';
  $pane->subtype = 'spike_subscriptions-panel_pane_1';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => 'subscriptions',
    'css_class' => 'sticky-footer',
  );
  $pane->extras = array();
  $pane->position = 7;
  $pane->locks = array();
  $pane->uuid = 'd2dbf500-d511-4f5a-aab8-3458969bac05';
  $display->content['new-d2dbf500-d511-4f5a-aab8-3458969bac05'] = $pane;
  $display->panels['middle'][7] = 'new-d2dbf500-d511-4f5a-aab8-3458969bac05';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-8c090516-695d-4202-8267-beb3f6db5470';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['shop'] = $page;

  return $pages;

}
