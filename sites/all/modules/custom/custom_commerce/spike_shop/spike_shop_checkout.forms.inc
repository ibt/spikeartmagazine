<?php

/*
 * @file spike_shop_checkout.forms.inc
 */


/**
 * custom formatting for shipping pane form
 *
 * @param $form
 * @param $form_state
 * @param $form_id
 */
function spike_shop_checkout_form_alter_shipping(&$form, &$form_state, $form_id) {
  /*
   * basic arrange / unset
   */
  if ($form['commerce_shipping']['shipping_rates']['#value']['custom_shipping_service']->order_id) {
    $order_id = $form['commerce_shipping']['shipping_rates']['#value']['custom_shipping_service']->order_id;
  }
  $order_id = !$order_id ? arg(1) : $order_id;
  $order = commerce_order_load($order_id);
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);

  $form_el = $form['commerce_shipping'];
  // only one shipping method, so disable radio selectors
  // & reconstruct shipping title & total
  unset($form_el['#prefix']); unset($form_el['#suffix']);
  $form_el['shipping_service']['#attributes']['class'][]  = 'hidden';
  $form_el['#title'] = t('Shipping Total');


  /*
   * Custom Element: Total
   */
  $shipping_total = spike_shop_checkout_shipping_cost_getter($form_el, $order_wrapper);
  $shipping_total_output = '<div class="shipping-total-wrapper">'
//  $shipping_total_output = ''
    . '<span class="text">' . t('Total shipping: ') . '</span>'
    . '<span class="price">' . $shipping_total . '</span>'
//    ;
    . '</div>';

  /*
   * Custom Element: Items
   */
  $items_formatted = spike_shop_checkout_shipping_items_getter($order);

  /*
   * Custom Element: Address
   */
  $address = $order_wrapper->commerce_customer_shipping->commerce_customer_address->value();
  $address_output = '<div class="shipping-address"><p>' . t('Shipping to') . ':</p>'
    . spike_layout_theme_address($address)
    . '</div>';

  /*
   * add custom elements to the form
   */
  $form_el['custom_output'] = array(
    '#type' => 'markup',
    'items' => array(
      '#markup' => $items_formatted
    ),
    'total' => array(
      '#markup' => $shipping_total_output
    ),
    'address' => array(
      '#markup' => $address_output
    ),
  );
  $form['commerce_shipping'] = $form_el;
  $t=1;


}



function spike_shop_checkout_form_alter_review_page(&$form, &$form_state, $form_id) {
  $str = 'extra_pane__node__';
  foreach ($form as $id => $row) {
    if (strpos($id, $str) !== FALSE) {
      GLOBAL $language;
      $nid = intval(str_replace($str, '', $id));
      $node = node_load($nid);
      $tr_set = translation_node_get_translations($node->tnid);
      $node = $tr_set[$language->language];
      $el_output = '<div class="output">'
        . l($node->title, 'node/' . $nid, array(
            'attributes' => array(
              'target' => '_blank'
            )
          )
        ) . '</div>';
      $form[$id][$id]['entity']['#markup'] = '';
      if ($nid == 852 || $nid == 851) {
        $form[$id]['termsofservice']['#title'] = t('I accept the ') . $node->title;
        if ($language->language == 'de') {
          $form[$id]['termsofservice']['#title'] .= ' ' . t('zur Kenntnis.');
        }
      } else if ($nid == 854 || $nid == 853) {
        $form[$id]['termsofservice']['#title'] = t('I agree with the ') . $node->title;
      }
      $form[$id]['termsofservice']['#suffix'] = l($node->title, 'node/' . $nid, array( 'attributes' => array( 'target' => '_blank' )));
    }
  }
  $form['buttons']['continue']['#value'] = t('Continue to payment');
}

function spike_shop_checkout_form_alter_completion_page(&$form, &$form_state, $form_id) {
  GLOBAL $language;
  $msg = $language->language == 'de' ? '<div class="checkout-completion-message"> <p>Danke für Ihre Bestellung!<br />Eine Bestätigung per E-Mail wurde an Sie gesandt, sollte Sie ausbleiben, kontaktieren Sie uns bitte unter <a href="mailto:abo@spikeartmagazine.com">abo@spikeartmagazine.com</a>.<br /><a href="http://www.spikeartmagazine.com/de">Zurück zur Startseite.</a></p> </div>'
    : $form['checkout_completion_message']['message']['#markup'];
  $order_info = spike_checkout_messages_page_callback($form_state['order']->order_id);
  $form['checkout_completion_message']['message']['#markup'] = $msg . $order_info;
}


function spike_shop_checkout_form_alter_info_page(&$form, &$form_state, $form_id) {
  /*
 * add COMPANY conditional checkbox
 */
  $company_check = array(
    '#type' => 'checkbox',
    '#title' => t('Bill as a company'),
    '#weight' => -19,
  );

  /* Override back button on checkout page for registered users */
  GLOBAL $user;
  if ($user->uid > 0) {
    $form_state['status_update'] = 'cart';
    $form['buttons']['back']['#submit'][0] = 'spike_checkout_form_redirect';
  }

  $form['customer_profile_billing']['company_check'] = $company_check;
  $form['customer_profile_billing']['commerce_customer_address']['und'][0]
    ['locality_block']['locality']['#title'] = t('Town/City');
  $form['customer_profile_shipping']['commerce_customer_address']['und'][0]
    ['locality_block']['locality']['#title'] = t('Town/City');
}