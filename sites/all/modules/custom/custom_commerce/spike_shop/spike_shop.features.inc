<?php
/**
 * @file
 * spike_shop.features.inc
 */

/**
 * Implements hook_commerce_product_default_types().
 */
function spike_shop_commerce_product_default_types() {
  $items = array(
    'product_edition' => array(
      'type' => 'product_edition',
      'name' => 'product_edition',
      'description' => '',
      'help' => '',
      'revision' => 0,
    ),
    'product_issue' => array(
      'type' => 'product_issue',
      'name' => 'product_issue',
      'description' => '',
      'help' => '',
      'revision' => 0,
    ),
    'product_subscription' => array(
      'type' => 'product_subscription',
      'name' => 'product_subscription',
      'description' => '',
      'help' => '',
      'revision' => 1,
    ),
  );
  return $items;
}

/**
 * Implements hook_commerce_tax_default_rates().
 */
function spike_shop_commerce_tax_default_rates() {
  $items = array(
    'spike_tax_vat' => array(
      'name' => 'spike_tax_vat',
      'display_title' => 'incl. 10% VAT, excl. shipping costs',
      'description' => '',
      'rate' => '.1',
      'type' => 'vat',
      'rules_component' => 'commerce_tax_rate_spike_tax_vat',
      'default_rules_component' => 1,
      'price_component' => 'tax|spike_tax_vat',
      'calculation_callback' => 'commerce_tax_rate_calculate',
      'module' => 'commerce_tax_ui',
      'title' => 'VAT 10%',
      'admin_list' => TRUE,
    ),
  );
  return $items;
}

/**
 * Implements hook_commerce_tax_default_types().
 */
function spike_shop_commerce_tax_default_types() {
  $items = array(
    'sales_tax' => array(
      'name' => 'sales_tax',
      'display_title' => 'Sales tax',
      'description' => 'A basic type for taxes that do not display inclusive with product prices.',
      'display_inclusive' => 0,
      'round_mode' => 0,
      'rule' => 'commerce_tax_type_sales_tax',
      'module' => 'commerce_tax_ui',
      'title' => 'Sales tax',
      'admin_list' => TRUE,
    ),
    'vat' => array(
      'name' => 'vat',
      'display_title' => 'VAT 10%',
      'description' => 'A basic type for taxes that display inclusive with product prices.',
      'display_inclusive' => 1,
      'round_mode' => 1,
      'rule' => 'commerce_tax_type_vat',
      'module' => 'commerce_tax_ui',
      'title' => 'VAT 10%',
      'admin_list' => TRUE,
    ),
  );
  return $items;
}

/**
 * Implements hook_ctools_plugin_api().
 */
function spike_shop_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "commerce_autosku" && $api == "autosku_pattern") {
    return array("version" => "1");
  }
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function spike_shop_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
