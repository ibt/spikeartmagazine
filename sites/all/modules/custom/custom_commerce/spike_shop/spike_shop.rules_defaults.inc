<?php
/**
 * @file
 * spike_shop.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function spike_shop_default_rules_configuration() {
  $items = array();
  $items['commerce_tax_rate_spike_tax_vat'] = entity_import('rules_config', '{ "commerce_tax_rate_spike_tax_vat" : {
      "LABEL" : "Calculate spike_tax_vat",
      "PLUGIN" : "rule",
      "OWNER" : "rules",
      "TAGS" : [ "Commerce Tax", "vat" ],
      "REQUIRES" : [ "spike_shop_checkout", "commerce_tax" ],
      "USES VARIABLES" : { "commerce_line_item" : { "label" : "Line item", "type" : "commerce_line_item" } },
      "IF" : [
        { "NOT spike_shop_checkout_condition_vat_bool" : { "order" : [ "commerce-line-item:order" ] } }
      ],
      "DO" : [
        { "commerce_tax_rate_apply" : {
            "USING" : {
              "commerce_line_item" : [ "commerce-line-item" ],
              "tax_rate_name" : "spike_tax_vat"
            },
            "PROVIDE" : { "applied_tax" : { "applied_tax" : "Applied tax" } }
          }
        }
      ]
    }
  }');
  return $items;
}
