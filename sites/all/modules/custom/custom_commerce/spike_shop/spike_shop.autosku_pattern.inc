<?php
/**
 * @file
 * spike_shop.autosku_pattern.inc
 */

/**
 * Implements hook_default_commerce_autosku_patterns().
 */
function spike_shop_default_commerce_autosku_patterns() {
  $export = array();

  $commerce_autosku_patterns = new stdClass();
  $commerce_autosku_patterns->disabled = FALSE; /* Edit this to true to make a default commerce_autosku_patterns disabled initially */
  $commerce_autosku_patterns->api_version = 1;
  $commerce_autosku_patterns->product_type = 'product_edition';
  $commerce_autosku_patterns->pattern = '';
  $commerce_autosku_patterns->advanced = '';
  $export['product_edition'] = $commerce_autosku_patterns;

  $commerce_autosku_patterns = new stdClass();
  $commerce_autosku_patterns->disabled = FALSE; /* Edit this to true to make a default commerce_autosku_patterns disabled initially */
  $commerce_autosku_patterns->api_version = 1;
  $commerce_autosku_patterns->product_type = 'product_issue';
  $commerce_autosku_patterns->pattern = '';
  $commerce_autosku_patterns->advanced = '';
  $export['product_issue'] = $commerce_autosku_patterns;

  $commerce_autosku_patterns = new stdClass();
  $commerce_autosku_patterns->disabled = FALSE; /* Edit this to true to make a default commerce_autosku_patterns disabled initially */
  $commerce_autosku_patterns->api_version = 1;
  $commerce_autosku_patterns->product_type = 'product_subscription';
  $commerce_autosku_patterns->pattern = '';
  $commerce_autosku_patterns->advanced = '';
  $export['product_subscription'] = $commerce_autosku_patterns;

  return $export;
}
