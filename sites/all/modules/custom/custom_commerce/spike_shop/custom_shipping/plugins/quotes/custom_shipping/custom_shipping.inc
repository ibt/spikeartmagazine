

<?php

$plugin = array(
  'title' => t('Custom shipping'),
  'description' => t('Descriptive text.'),
  'handler' => array(
    'class' => 'CustomShipping',
    'parent' => 'quote_base'
  ),
  'price_component' => array(
   'title' => t('Shipping'),
   'display_title' => t('Shipping'),
   'weight' => -40,
  ),
);
