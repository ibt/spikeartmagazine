<?php
class CustomShipping extends CommerceShippingQuote {
  public function calculate_quote($currency_code, $form_values = array(), $order = NULL, $pane_form = NULL, $pane_values = NULL) {
    $order = $order ? $order : $this->order;
    $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
    $price = 0;
    // Loop through the products, and add costs.
    foreach ($order_wrapper->commerce_line_items as $line_item_wrapper) {
      switch ($line_item_wrapper->commerce_product->field_shipping_type->value()) {
        case 'free':
          break;
        case 'flat_rate':
          $price += $line_item_wrapper->commerce_product->field_flat_rate->amount->value();
          break;
        case 'call_for_details':
          // Call for details
          break;
      }
    }
    return array(array(
      'amount' => $price,
      'currency_code' => $currency_code,
      'label' => t('Custom shipping method line item label'),
      'quantity' => 1,
    ));
  }
}
