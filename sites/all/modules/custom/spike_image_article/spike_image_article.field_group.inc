<?php
/**
 * @file
 * spike_image_article.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function spike_image_article_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_extra|node|article|form';
  $field_group->group_name = 'group_extra';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'article';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Extra display elements',
    'weight' => '16',
    'children' => array(),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Extra display elements',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-extra field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'none',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_extra|node|article|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_extra|node|image_article|form';
  $field_group->group_name = 'group_extra';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'image_article';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Extra display elements',
    'weight' => '8',
    'children' => array(
      0 => 'field_display_extra_ref',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Extra display elements',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-extra field-group-div',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'none',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_extra|node|image_article|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_image_info|node|image_article|full';
  $field_group->group_name = 'group_image_info';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'image_article';
  $field_group->mode = 'full';
  $field_group->parent_name = 'group_upper';
  $field_group->data = array(
    'label' => 'Image Info',
    'weight' => '4',
    'children' => array(
      0 => 'image_caption',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Image Info',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-image-info field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'none',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_image_info|node|image_article|full'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_lower|node|image_article|full';
  $field_group->group_name = 'group_lower';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'image_article';
  $field_group->mode = 'full';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Lower Group',
    'weight' => '1',
    'children' => array(
      0 => 'body',
      1 => 'field_image',
      2 => 'group_tags',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Lower Group',
      'instance_settings' => array(
        'id' => 'article-thumbnails',
        'classes' => 'group-lower field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'none',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_lower|node|image_article|full'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_tags|node|image_article|full';
  $field_group->group_name = 'group_tags';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'image_article';
  $field_group->mode = 'full';
  $field_group->parent_name = 'group_lower';
  $field_group->data = array(
    'label' => 'Tags',
    'weight' => '6',
    'children' => array(
      0 => 'field_tags',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Tags',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-tags field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'none',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_tags|node|image_article|full'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_upper|node|image_article|full';
  $field_group->group_name = 'group_upper';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'image_article';
  $field_group->mode = 'full';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Upper Group',
    'weight' => '0',
    'children' => array(
      0 => 'image_info',
      1 => 'group_image_info',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Upper Group',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-upper field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'none',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_upper|node|image_article|full'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Extra display elements');
  t('Image Info');
  t('Lower Group');
  t('Tags');
  t('Upper Group');

  return $field_groups;
}
