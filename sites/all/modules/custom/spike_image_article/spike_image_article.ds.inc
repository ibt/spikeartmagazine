<?php
/**
 * @file
 * spike_image_article.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function spike_image_article_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|image_article|basic_info';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'image_article';
  $ds_fieldsetting->view_mode = 'basic_info';
  $ds_fieldsetting->settings = array(
    'basic_node_info' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|image_article|basic_info'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|image_article|full';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'image_article';
  $ds_fieldsetting->view_mode = 'full';
  $ds_fieldsetting->settings = array(
    'image_info' => array(
      'weight' => '3',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'image_caption' => array(
      'weight' => '6',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|image_article|full'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|image_article|mini_teaser';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'image_article';
  $ds_fieldsetting->view_mode = 'mini_teaser';
  $ds_fieldsetting->settings = array(
    'post_date' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'ds_post_date_time_ago',
    ),
    'title_link' => array(
      'weight' => '16',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|image_article|mini_teaser'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|image_article|sidebar';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'image_article';
  $ds_fieldsetting->view_mode = 'sidebar';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '2',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '0',
        'wrapper' => '',
        'class' => '',
      ),
    ),
    'post_date' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'ds_post_date_short_euro',
    ),
    'image_counter' => array(
      'weight' => '4',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'thumb_toggle' => array(
      'weight' => '5',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'image_caption' => array(
      'weight' => '3',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|image_article|sidebar'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|image_article|teaser';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'image_article';
  $ds_fieldsetting->view_mode = 'teaser';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '3',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '1',
        'wrapper' => 'h4',
        'class' => 'article-title',
        'exclude node title settings' => '1',
      ),
    ),
    'compound_info' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|image_article|teaser'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function spike_image_article_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|image_article|basic_info';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'image_article';
  $ds_layout->view_mode = 'basic_info';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'basic_node_info',
      ),
    ),
    'fields' => array(
      'basic_node_info' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|image_article|basic_info'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|image_article|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'image_article';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'body',
        1 => 'field_image',
        2 => 'field_display_extra_ref',
        3 => 'field_title_sub',
        4 => 'field_category',
        5 => 'field_tags',
        6 => 'field_article_ref_article',
        7 => 'field_blurb',
      ),
    ),
    'fields' => array(
      'body' => 'ds_content',
      'field_image' => 'ds_content',
      'field_display_extra_ref' => 'ds_content',
      'field_title_sub' => 'ds_content',
      'field_category' => 'ds_content',
      'field_tags' => 'ds_content',
      'field_article_ref_article' => 'ds_content',
      'field_blurb' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|image_article|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|image_article|form';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'image_article';
  $ds_layout->view_mode = 'form';
  $ds_layout->layout = 'ds_2col';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'title',
        1 => 'field_title_sub',
        2 => 'field_subtitle_display',
        3 => 'field_category',
        4 => 'field_tags',
        5 => 'field_blurb',
        6 => 'field_blurb_display',
        7 => 'body',
        8 => 'group_extra',
        9 => 'field_display_extra_ref',
        11 => 'path',
      ),
      'right' => array(
        10 => 'field_image',
        12 => 'group_images',
        13 => 'field_online_bool',
      ),
      'hidden' => array(
        14 => 'language',
        15 => 'field_article_ref_article',
        16 => 'flag',
        17 => 'metatags',
        18 => '_add_existing_field',
      ),
    ),
    'fields' => array(
      'title' => 'left',
      'field_title_sub' => 'left',
      'field_subtitle_display' => 'left',
      'field_category' => 'left',
      'field_tags' => 'left',
      'field_blurb' => 'left',
      'field_blurb_display' => 'left',
      'body' => 'left',
      'group_extra' => 'left',
      'field_display_extra_ref' => 'left',
      'field_image' => 'right',
      'path' => 'left',
      'group_images' => 'right',
      'field_online_bool' => 'right',
      'language' => 'hidden',
      'field_article_ref_article' => 'hidden',
      'flag' => 'hidden',
      'metatags' => 'hidden',
      '_add_existing_field' => 'hidden',
    ),
    'classes' => array(),
    'wrappers' => array(
      'left' => 'div',
      'right' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => 0,
  );
  $export['node|image_article|form'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|image_article|full';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'image_article';
  $ds_layout->view_mode = 'full';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'group_upper',
        1 => 'group_lower',
        2 => 'field_image',
        3 => 'image_info',
        4 => 'group_image_info',
        5 => 'field_tags',
        6 => 'body',
        7 => 'group_tags',
        8 => 'image_caption',
      ),
    ),
    'fields' => array(
      'group_upper' => 'ds_content',
      'group_lower' => 'ds_content',
      'field_image' => 'ds_content',
      'image_info' => 'ds_content',
      'group_image_info' => 'ds_content',
      'field_tags' => 'ds_content',
      'body' => 'ds_content',
      'group_tags' => 'ds_content',
      'image_caption' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
    'hide_page_title' => '0',
    'page_option_title' => '',
  );
  $export['node|image_article|full'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|image_article|mini_teaser';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'image_article';
  $ds_layout->view_mode = 'mini_teaser';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_category',
        1 => 'post_date',
        2 => 'title_link',
        3 => 'field_image',
      ),
    ),
    'fields' => array(
      'field_category' => 'ds_content',
      'post_date' => 'ds_content',
      'title_link' => 'ds_content',
      'field_image' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|image_article|mini_teaser'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|image_article|sidebar';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'image_article';
  $ds_layout->view_mode = 'sidebar';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_category',
        1 => 'post_date',
        2 => 'title',
        3 => 'image_caption',
        4 => 'image_counter',
        5 => 'thumb_toggle',
      ),
    ),
    'fields' => array(
      'field_category' => 'ds_content',
      'post_date' => 'ds_content',
      'title' => 'ds_content',
      'image_caption' => 'ds_content',
      'image_counter' => 'ds_content',
      'thumb_toggle' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['node|image_article|sidebar'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|image_article|teaser';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'image_article';
  $ds_layout->view_mode = 'teaser';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_display_extra_ref',
        1 => 'compound_info',
        2 => 'field_image',
        3 => 'title',
        4 => 'field_title_sub',
        5 => 'field_blurb',
      ),
    ),
    'fields' => array(
      'field_display_extra_ref' => 'ds_content',
      'compound_info' => 'ds_content',
      'field_image' => 'ds_content',
      'title' => 'ds_content',
      'field_title_sub' => 'ds_content',
      'field_blurb' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['node|image_article|teaser'] = $ds_layout;

  return $export;
}
