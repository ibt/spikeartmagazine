<?php
/**
 * @file
 * spike_image_article.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function spike_image_article_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_image_default_styles().
 */
function spike_image_article_image_default_styles() {
  $styles = array();

  // Exported image style: 250_w.
  $styles['250_w'] = array(
    'label' => '250 W',
    'effects' => array(
      23 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 250,
          'height' => 250,
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: image_gallery_thumb.
  $styles['image_gallery_thumb'] = array(
    'label' => 'image gallery thumb 250 w 180 h',
    'effects' => array(
      24 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 250,
          'height' => 180,
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: spike_medium.
  $styles['spike_medium'] = array(
    'label' => 'Spike Medium (w 300)',
    'effects' => array(
      26 => array(
        'name' => 'coloractions_convert',
        'data' => array(
          'format' => 'image/gif',
          'quality' => 75,
        ),
        'weight' => -10,
      ),
      25 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 300,
          'height' => '',
          'upscale' => 1,
          'retinafy' => 1,
        ),
        'weight' => -9,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function spike_image_article_node_info() {
  $items = array(
    'image_article' => array(
      'name' => t('Image Article'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
