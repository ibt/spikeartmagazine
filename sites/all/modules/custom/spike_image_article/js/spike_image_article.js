(function($) {

  Drupal.behaviors.spikeImageArticle = {

    attach: function (context, settings) {

      var $imageFlipper = $('.image-flipper');
      var $imgCounter = $('#image-counter');
      var $main = $('.main-container');
      var $thumbs = $('#article-thumbnails');
      var $thumbToggle = $('.thumb-toggle');

      function isInt(n){
        return typeof n== "number" && isFinite(n) && n%1===0;
      }

      function printCounter(index,total) {
        return '<span class="active">'+(index)+'</span>/<span class="total">'+total+'</span>';
      }


      function getSlideSize($imageFlipper, index) {
        var $el = $imageFlipper.find('#article-thumbnails').eq(index).children('img');
        return [$el.height(), $el.width()];
      }


      /*
      INIT CAPTIONS
      position captions in the sidebar, they will be updated on pagination
      , or possibly initiated by the pager, to which it would always be synced
       */
      $main.on('initCaptions', function() {
        // find captions
        $main.find('.field-name-image-info .caption').each(function() {
          // $(this).appendTo('#article-captions-container').hide();
          $(this).hide();
        });

        if($(window).width() < 1024){
          // console.log("sanity two");
          $('.field-name-image-info').append("<div class='mobile-nav-wrapper'><div class='prev-mobile'></div><div class='next-mobile'></div></div>");

        }else{
          // console.log("insanity");
        }

      });

      $main.on('setImageCounter', function(event, current, total) {
        var counter = printCounter(current,total);
        $imgCounter.html(counter);
        $main.trigger('setImageCaption', current);
      });


      $main.on('setImageCaption', function(event, index) {
        index--;
        var $captionContainer = $('.sb-2 .field-name-image-caption');
        var $caption = $('.caption[caption-index="' + index + '"]').clone();
        // Clear old content and add newly selected caption.
        $captionContainer.children().fadeOut().remove();
        $captionContainer.append($caption.addClass('caption-index-' + index));
        $caption.fadeIn();
      });

      $thumbs.on('thumbPosInit', function() {
        $(this).css({
          top: $(window).height()
        });
      });



      $thumbs.on('checkThumbPos', function() {
        var thumbP = $thumbs.position().top;
        var scrollTop = $(window).scrollTop();
        var dir;
        if (scrollTop < thumbP / 1.5) {
          $('html,body').animate({
            scrollTop: thumbP
          }, 500);
          // do smooth scroll

        } else {
          $('html,body').animate({
            scrollTop: 0
          }, 500);

        }
      });

      $thumbs.on('setThumbTrigger', function(event, dir) {
        if (dir == 0) {
          $('.thumb-toggle-el-top').show();
          $('.thumb-toggle-el-bot').hide();
        } else {
          $('.thumb-toggle-el-top').hide();
          $('.thumb-toggle-el-bot').show();
        }
      });

      /* action on thumbnail click - page to that image */
      $main.on('thumbClick', function(event, i) {
        var $flipper = $main.find('.image-flipper');
        i++;
        $flipper.turn('page', i);
      });

      $imageFlipper.on('flipperResize', function(event, setH, setW, page) {
        var index = page - 1;
        // based on a set height, calculate dynamic width
        // h/w ratio
        var $el = $imageFlipper.find('#article-thumbnails').eq(index).children('img');
        var w = $el.width();
        var h = $el.height();
        var ratio = h/w;
        var newH = h;

        $el.parents('.turn-page-wrapper').css({height: newH});

        $imageFlipper.turn('size', setW, newH);

      });

      function getColor() {
        return '#'+Math.floor(Math.random()*16777215).toString(16)
      }


      $imageFlipper.on('initFlipBg', function() {
        var asfd;
        $(this).find('.turn-page').each(function() {
          $(this).css({
            'background': 'white'
          });
        })
      });

      function titlePos(h) {
        return ($(window).height() / 2) - (h / 2)
      }

      $imageFlipper.on('initTitle', function() {
        var $title = $(this).parents('.field').siblings('.group-image-info');
        $title.find('h1').clone().addClass('title-clone').insertBefore($title);
        $('.title-clone').wrap('<div class="title-clone-wrapper">');

        var $tClone = $('.title-clone-wrapper');
        var h = $tClone.find('h1').outerHeight();
        $tClone.css({
          opacity: 0,
          position: 'absolute',
          top: titlePos(h)
        });
        $tClone.animate({
          opacity: 1
        }, 300).delay(4000).fadeOut('slow');

        $imageFlipper.click(function() {
          $tClone.clearQueue().fadeOut('fast');
        })

      });

      /*
      IMAGE ARTICLE initFlipper
      make an image display with turn.js library
       */
      $imageFlipper.on('initFlipper', function() {
        // center our element based on with of side columns
        var margin = parseInt($(this).parents('.field').css('margin-left').replace('px', ''));
        var flpW = $(window).width() - (margin * 2);
        var flpH = $(window).height() - 120;
        $imageFlipper.css({
          margin: 0,
          width: flpW,
          height: flpH
        });

        var imgDim = getSlideSize($imageFlipper, 1);
        var ratio = imgDim[0] / imgDim[1];
        var elH = ratio * flpW;

        // init turn js object
        var $mag = $(this);
        $mag.turn({
          display: 'single',
//          display: 'double',
          duration: 1200,
          width: flpW,
          height: flpH,
          autoCenter: true,
//          acceleration: true,
          acceleration: $.isTouch,
//            gradients: true,
          gradients: !$.isTouch,
          turnCorners: 'bl,tr',
          elevation: 350,
          easeFunction: 'ease-in',
//            max: 3,
          when: {
            start: function(event, page, view) {
              $main.trigger('setImageCounter', [$mag.turn('page'), $mag.turn('pages')]);
            },
            turned: function(event, page, pageObject) {
              $mag.trigger('initFlipBg');
              // force remove of title attr introduced by new images
              $('[title]').removeAttr('title');
            }
          }
        });

        /*
        click control - simplify clicks on the image
         */
        $mag.click(function(e) {
          var offset = $(this).offset();
          var relativeX = (e.pageX - offset.left);
          if (relativeX < flpW / 2) { $mag.turn('previous') }
          else { $mag.turn('next') };
        });
        $('.prev-mobile').click(function(){
            $mag.turn('previous');
        });

        $('.next-mobile').click(function(){
          $mag.turn('next');
        });


        // additional init functions

        // set pager values
        var cur = $mag.turn('page');
        var total = $mag.turn('pages')
        var asdf;
        $main.trigger('setImageCounter', [$mag.turn('page'), $mag.turn('pages')]);


      });


      /*
       * .once('main-container')
       */
      $('.main-container', context).once(function() {
        $main.hide();

        /*
        move page title into body - this is a BUG fix - dunno why field wouldn't work
         */
        $('.page-header').insertAfter('.group-upper .field-name-image-caption');


        /*
        init captions
         */
        $(this).trigger('initCaptions');

        $imageFlipper.trigger('initFlipper');
        $imageFlipper.trigger('initFlipBg');
        $(window).on('load', function(){
          $imageFlipper.trigger('initTitle');

        });



        /*
        turn.js keyboard controls
         */
        $(window).bind('keydown', function(e){
          if (e.keyCode==37)
            $imageFlipper.turn('previous');
          else if (e.keyCode==39)
            $imageFlipper.turn('next');
            $('.title-clone-wrapper').clearQueue().fadeOut('fast');
        });




        /*
         hover control - give visual feedback about clicking when hover
         */


        $('#next').click('keydown', function(e){
          if (e.keyCode==37)
            $imageFlipper.turn('previous');
          else if (e.keyCode==39)
            $imageFlipper.turn('next');
          $('.title-clone-wrapper').clearQueue().fadeOut('fast');
        });




        $(window).resize(function() {
//          $imageFlipper.trigger('initFlipper');

          var margin = parseInt($imageFlipper.parents('.field').css('margin-left').replace('px', ''));
          var flpW = $(window).width() - (margin * 2);
          var flpH = $(window).height() - 120;
          var size = $(window).width() - (margin * 2);

          $imageFlipper.turn('size', flpW, flpH);
          $('.title-clone-wrapper').css({
            top: titlePos($('.title-clone-wrapper').height())
          });


        });





        /* position the thumbnail display just below viewport
        * this position is toggled via $('#thumb-toggle') in right sidebar
        * */
        $thumbs.trigger('checkThumbPos');


        /* trigger to toggle position of thumbnail display
        * the trigger is in the right sidebar
        * */
        $('.thumb-toggle a').click(function() {
          // suppress link click
          $thumbs.trigger('checkThumbPos');
          return false;
        });

        /* click trigger on thumbnail to jump back to main img */
        $thumbs.find('img').click(function() {
          var i = $(this).parent().index();
          $main.trigger('thumbClick', [i]);
          // change slide
          $thumbs.trigger('checkThumbPos');

        });


        // on scroll
        $(window).scroll(function() {
          var scrollT = $(window).scrollTop();
          var thumbPos = $thumbs.offset().top;
          var breakP = $thumbToggle.offset().top + ($thumbToggle.outerHeight()/2);
          var dir;
          if (breakP >= thumbPos) {
            // back to the top
            dir = 1;
            $('#article-caption-display').fadeOut();
          } else {
            // overview
            dir = 0;
            $('#article-caption-display').fadeIn();
          }
          $thumbs.trigger('setThumbTrigger', [dir]);
        });

        $main.show();

      });
      /* end .once('main-container') */


    }
  };
})(jQuery);