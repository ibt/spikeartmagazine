<?php

/**
 * @file
 * spike_overlays.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function spike_overlays_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function spike_overlays_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_flag_default_flags().
 */
function spike_overlays_flag_default_flags() {
  $flags = array();
  // Exported flag: "Overlay".
  $flags['overlay'] = array(
    'entity_type' => 'node',
    'title' => 'Overlay',
    'global' => 0,
    'types' => array(
      0 => 'overlay',
    ),
    'flag_short' => 'X',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'X',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'weight' => 0,
    'expire_type' => 'flag_period',
    'expire_period' => 120,
    'expire_date' => NULL,
    'show_in_links' => array(
      'full' => 'full',
      'teaser' => 0,
      'rss' => 0,
      'search_index' => 0,
      'search_result' => 0,
      'checkout_pane' => 0,
      'token' => 0,
      'basic_info' => 0,
      'mini_teaser' => 0,
      'sidebar' => 0,
      'related_content' => 0,
      'mini_basic' => 0,
      'teaser_product_front' => 0,
      'revision' => 0,
    ),
    'show_as_field' => 1,
    'show_on_form' => 0,
    'access_author' => '',
    'show_contextual_link' => 0,
    'i18n' => 0,
    'api_version' => 3,
    'module' => 'spike_overlays',
    'locked' => array(
      0 => 'name',
    ),
  );
  return $flags;

}

/**
 * Implements hook_node_info().
 */
function spike_overlays_node_info() {
  $items = array(
    'overlay' => array(
      'name' => t('Advert Overlay'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
