<?php

/**
 * @file
 * spike_overlays.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function spike_overlays_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_wrapper|node|overlay|default';
  $field_group->group_name = 'group_wrapper';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'overlay';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Wrapper',
    'weight' => '2',
    'children' => array(
      0 => 'field_image_overlay',
      1 => 'field_link',
      2 => 'field_image_overlay_aux',
      3 => 'flag_overlay',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Wrapper',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-wrapper field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_wrapper|node|overlay|default'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Wrapper');

  return $field_groups;
}
