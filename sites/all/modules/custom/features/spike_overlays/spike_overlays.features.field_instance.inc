<?php

/**
 * @file
 * spike_overlays.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function spike_overlays_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-overlay-field_image_overlay'.
  $field_instances['node-overlay-field_image_overlay'] = array(
    'bundle' => 'overlay',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'checkout_pane' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'default' => array(
        'label' => 'hidden',
        'module' => 'image_link_formatter',
        'settings' => array(
          'field_multiple_limit' => -1,
          'field_multiple_limit_offset' => 0,
          'field_multiple_limit_order' => 0,
          'image_link' => 'field_link',
          'image_style' => 'spike_extra_large_600',
        ),
        'type' => 'image_link_formatter',
        'weight' => 11,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_image_overlay',
    'label' => 'Image Overlay',
    'required' => 1,
    'settings' => array(
      'alt_field' => 0,
      'default_image' => 0,
      'entity_translation_sync' => FALSE,
      'file_directory' => 'images/overlay',
      'file_extensions' => 'png gif jpg jpeg',
      'focus' => 0,
      'focus_lock_ratio' => 0,
      'focus_min_size' => '',
      'image_field_caption' => array(
        'enabled' => 0,
      ),
      'image_field_caption_wrapper' => array(
        'image_field_caption_default' => array(
          'format' => 'filtered_html',
          'value' => '',
        ),
      ),
      'max_filesize' => '2 MB',
      'max_resolution' => '',
      'min_resolution' => '',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'allowed_schemes' => array(
          'private' => 0,
          'public' => 'public',
        ),
        'allowed_types' => array(
          'audio' => 0,
          'document' => 0,
          'image' => 'image',
          'video' => 0,
        ),
        'browser_plugins' => array(
          'media_default--media_browser_1' => 'media_default--media_browser_1',
          'media_default--media_browser_my_files' => 'media_default--media_browser_my_files',
          'upload' => 'upload',
        ),
        'filefield_sources' => array(
          'filefield_sources' => array(),
          'source_attach' => array(
            'absolute' => 0,
            'attach_mode' => 'move',
            'path' => 'file_attach',
          ),
          'source_imce' => array(
            'imce_mode' => 0,
          ),
          'source_reference' => array(
            'autocomplete' => 0,
          ),
        ),
        'insert' => 0,
        'insert_absolute' => FALSE,
        'insert_class' => '',
        'insert_default' => array(
          0 => 'auto',
        ),
        'insert_styles' => array(
          0 => 'auto',
        ),
        'insert_width' => '',
        'preview_image_style' => 'thumbnail',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => 4,
    ),
  );

  // Exported field_instance: 'node-overlay-field_image_overlay_aux'.
  $field_instances['node-overlay-field_image_overlay_aux'] = array(
    'bundle' => 'overlay',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'checkout_pane' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'default' => array(
        'label' => 'hidden',
        'module' => 'image_link_formatter',
        'settings' => array(
          'field_multiple_limit' => -1,
          'field_multiple_limit_offset' => 0,
          'field_multiple_limit_order' => 0,
          'image_link' => 'field_link',
          'image_style' => 'large_retina',
        ),
        'type' => 'image_link_formatter',
        'weight' => 12,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_image_overlay_aux',
    'label' => 'Mobile-Image Overlay',
    'required' => 0,
    'settings' => array(
      'alt_field' => 0,
      'default_image' => 0,
      'entity_translation_sync' => FALSE,
      'file_directory' => 'images/overlay',
      'file_extensions' => 'png gif jpg jpeg',
      'focus' => 0,
      'focus_lock_ratio' => 0,
      'focus_min_size' => '',
      'image_field_caption' => array(
        'enabled' => 0,
      ),
      'image_field_caption_wrapper' => array(
        'image_field_caption_default' => array(
          'format' => 'filtered_html',
          'value' => '',
        ),
      ),
      'max_filesize' => '2 MB',
      'max_resolution' => '',
      'min_resolution' => '',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'filefield_sources' => array(
          'filefield_sources' => array(
            'attach' => 0,
            'clipboard' => 0,
            'reference' => 0,
            'remote' => 0,
            'upload' => 'upload',
          ),
          'source_attach' => array(
            'absolute' => 0,
            'attach_mode' => 'move',
            'path' => 'file_attach',
          ),
          'source_imce' => array(
            'imce_mode' => 0,
          ),
          'source_reference' => array(
            'autocomplete' => 0,
            'search_all_fields' => 0,
          ),
        ),
        'insert' => 0,
        'insert_absolute' => 0,
        'insert_class' => '',
        'insert_default' => 'auto',
        'insert_styles' => array(
          'auto' => 'auto',
          'colorbox__250_w' => 0,
          'colorbox__300_w' => 0,
          'colorbox__advert_teaser' => 0,
          'colorbox__blur_test' => 0,
          'colorbox__full_width' => 0,
          'colorbox__full_width_blur' => 0,
          'colorbox__full_width_blur_lighten' => 0,
          'colorbox__full_width_fix' => 0,
          'colorbox__iek_photo' => 0,
          'colorbox__image_gallery_thumb' => 0,
          'colorbox__large' => 0,
          'colorbox__large_plus' => 0,
          'colorbox__large_retina' => 0,
          'colorbox__media_thumbnail' => 0,
          'colorbox__medium' => 0,
          'colorbox__medium_400_x_420' => 0,
          'colorbox__medium_crop_300_x_373' => 0,
          'colorbox__medium_retina' => 0,
          'colorbox__medium_square_400' => 0,
          'colorbox__special_teaser' => 0,
          'colorbox__spike_extra_large_600' => 0,
          'colorbox__spike_large' => 0,
          'colorbox__spike_medium' => 0,
          'colorbox__spike_medium_300_w_bg' => 0,
          'colorbox__square_thumbnail' => 0,
          'colorbox__thumbnail' => 0,
          'colorbox__thumbnail_retina' => 0,
          'icon_link' => 0,
          'image' => 0,
          'image_250_w' => 0,
          'image_300_w' => 0,
          'image_advert_teaser' => 0,
          'image_blur_test' => 0,
          'image_full_width' => 0,
          'image_full_width_blur' => 0,
          'image_full_width_blur_lighten' => 0,
          'image_full_width_fix' => 0,
          'image_iek_photo' => 0,
          'image_image_gallery_thumb' => 0,
          'image_large' => 0,
          'image_large_plus' => 0,
          'image_large_retina' => 0,
          'image_media_thumbnail' => 0,
          'image_medium' => 0,
          'image_medium_400_x_420' => 0,
          'image_medium_crop_300_x_373' => 0,
          'image_medium_retina' => 0,
          'image_medium_square_400' => 0,
          'image_special_teaser' => 0,
          'image_spike_extra_large_600' => 0,
          'image_spike_large' => 0,
          'image_spike_medium' => 0,
          'image_spike_medium_300_w_bg' => 0,
          'image_square_thumbnail' => 0,
          'image_thumbnail' => 0,
          'image_thumbnail_retina' => 0,
          'link' => 0,
        ),
        'insert_width' => '',
        'preview_image_style' => 'thumbnail',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => 5,
    ),
  );

  // Exported field_instance: 'node-overlay-field_link'.
  $field_instances['node-overlay-field_link'] = array(
    'bundle' => 'overlay',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'checkout_pane' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'default' => array(
        'label' => 'hidden',
        'module' => 'link',
        'settings' => array(
          'field_multiple_limit' => -1,
          'field_multiple_limit_offset' => 0,
          'field_multiple_limit_order' => 0,
        ),
        'type' => 'link_default',
        'weight' => 13,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_link',
    'label' => 'Link',
    'required' => 0,
    'settings' => array(
      'absolute_url' => 1,
      'attributes' => array(
        'class' => '',
        'configurable_class' => 0,
        'configurable_title' => 0,
        'rel' => '',
        'target' => 'user',
        'title' => '',
      ),
      'display' => array(
        'url_cutoff' => 80,
      ),
      'enable_tokens' => 0,
      'entity_translation_sync' => FALSE,
      'rel_remove' => 'default',
      'title' => 'optional',
      'title_allowed_values' => '',
      'title_label_use_field_label' => 0,
      'title_maxlength' => 128,
      'title_value' => '',
      'url' => 0,
      'user_register_form' => FALSE,
      'validate_url' => 1,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'link',
      'settings' => array(),
      'type' => 'link_field',
      'weight' => 6,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Image Overlay');
  t('Link');
  t('Mobile-Image Overlay');

  return $field_instances;
}
