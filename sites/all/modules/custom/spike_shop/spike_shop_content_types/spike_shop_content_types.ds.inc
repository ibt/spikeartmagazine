<?php
/**
 * @file
 * spike_shop_content_types.ds.inc
 */

/**
 * Implements hook_ds_layout_settings_info().
 */
function spike_shop_content_types_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|edition|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'edition';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_2col_stacked';
  $ds_layout->settings = array(
    'regions' => array(
      'header' => array(
        0 => 'field_display_extra_ref',
        1 => 'field_title_sub',
      ),
      'left' => array(
        2 => 'body',
      ),
      'right' => array(
        3 => 'field_image',
      ),
      'footer' => array(
        4 => 'field_product',
        5 => 'field_tags',
      ),
    ),
    'fields' => array(
      'field_display_extra_ref' => 'header',
      'field_title_sub' => 'header',
      'body' => 'left',
      'field_image' => 'right',
      'field_product' => 'footer',
      'field_tags' => 'footer',
    ),
    'classes' => array(),
    'wrappers' => array(
      'header' => 'div',
      'left' => 'div',
      'right' => 'div',
      'footer' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|edition|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|edition|full';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'edition';
  $ds_layout->view_mode = 'full';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'image_caption',
        1 => 'image_info',
        2 => 'group_upper',
        3 => 'group_lower',
        4 => 'field_tags',
        5 => 'group_image_info',
        6 => 'body',
        7 => 'group_tags',
      ),
    ),
    'fields' => array(
      'image_caption' => 'ds_content',
      'image_info' => 'ds_content',
      'group_upper' => 'ds_content',
      'group_lower' => 'ds_content',
      'field_tags' => 'ds_content',
      'group_image_info' => 'ds_content',
      'body' => 'ds_content',
      'group_tags' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'hide_page_title' => '1',
    'page_option_title' => '',
  );
  $export['node|edition|full'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|edition|teaser';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'edition';
  $ds_layout->view_mode = 'teaser';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'flag_recommended',
        1 => 'field_display_extra_ref',
        2 => 'field_image',
        3 => 'title',
        4 => 'field_product',
      ),
    ),
    'fields' => array(
      'flag_recommended' => 'ds_content',
      'field_display_extra_ref' => 'ds_content',
      'field_image' => 'ds_content',
      'title' => 'ds_content',
      'field_product' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['node|edition|teaser'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|subscription|teaser';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'subscription';
  $ds_layout->view_mode = 'teaser';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'title',
        1 => 'body',
        2 => 'field_product_ref_multiple',
      ),
    ),
    'fields' => array(
      'title' => 'ds_content',
      'body' => 'ds_content',
      'field_product_ref_multiple' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|subscription|teaser'] = $ds_layout;

  return $export;
}

/**
 * Implements hook_ds_view_modes_info().
 */
function spike_shop_content_types_ds_view_modes_info() {
  $export = array();

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'teaser_product_front';
  $ds_view_mode->label = 'teaser_product_front';
  $ds_view_mode->entities = array(
    'node' => 'node',
  );
  $export['teaser_product_front'] = $ds_view_mode;

  return $export;
}
