(function ($) {
  Drupal.behaviors.spikeIssueDisplay = {
    attach: function (context, settings) {
      var $main = $(".main-container");
      var $thumbs = $("#article-thumbnails");

      /*
      build masonry for referenced articles
       */
      function initMasonryEl($el) {
        var array = [];
        $el.children().each(function () {
          array.push($(this));
        });
        $.each(array, function () {
          $el.prepend($(this));
        });
        var ar = $el.children();
        ar.sort(function (a, b) {
          // Get a random number between 0 and 10
          var temp = parseInt(Math.random() * 10);
          // Get 1 or 0, whether temp is odd or even
          var isOddOrEven = temp % 2;
          // Get +1 or -1, whether temp greater or smaller than 5
          var isPosOrNeg = temp > 5 ? 1 : -1;
          // Return -1, 0, or +1
          return isOddOrEven * isPosOrNeg;
        });
        // Masonry on article Ref in Issue
        $el.masonry({
          columnWidth: 300,
          isAnimated: true,
          //          gutterWidth: 60,
          isFitWidth: true
        });
      }

      var $imageFlipper = $(".field-name-flip-book");

      $thumbs.on("checkThumbPos", function () {
        var thumbP = $thumbs.position().top;
        var scrollTop = $(window).scrollTop();
        var dir;
        if (scrollTop < thumbP / 1.5) {
          $("html,body").animate(
            {
              scrollTop: thumbP
            },
            500
          );
          // do smooth scroll
          dir = 1;
        } else {
          $("html,body").animate(
            {
              scrollTop: 0
            },
            500
          );

          dir = 0;
        }
      });

      // function printCounter(index, total) {
      //   return (
      //     '<span class="active">' +
      //     index +
      //     '</span>/<span class="total">' +
      //     total +
      //     "</span>"
      //   );
      // }

      // var $imgCounter = $("#image-counter");
      // $main.on("setImageCounter", function(event, current, total) {
      //   var counter = printCounter(current, total);
      //   $imgCounter.html(counter);
      //   $main.trigger("setImageCaption", current);
      // });

      /* action on thumbnail click - page to that image */
      $main.on("thumbClick", function (event, i) {
        var $flipper = $main.find(".field-name-flip-book");
        i++;
        $flipper.turn("page", i);
      });

      /*
       IMAGE ARTICLE initFlipper
       make an image display with turn.js library
       */
      $imageFlipper.on("initFlipBg", function () {
        $(this)
          .find(".turn-page")
          .each(function () {
            $(this).css({
              background: "white"
            });
          });
      });

      /*
      when placing product buttons into flipper,
      we must dynamically resize neighbor items to make it all fit
       */
      $imageFlipper.on("resizeElForProductFit", function (event, page) {
        var $buttons = $(".group-product");
        var bH = $buttons.outerHeight() + 60;
        // on first page, insert into the more inner element,
        // this is the diptych page, text & image
        if (page == "first") {
          var el =
            ".flip-page .group-left-inner .field-name-body .field-item p";
          var h_wrapper = $(".flip-page .group-left-inner").outerHeight() - bH;
          var h = 0;
          var margin;
          $(el).each(function (e) {
            h = h + $(this).height();
          });
          if (h > h_wrapper) {
            margin = $(".flip-page .group-left-inner").outerHeight() - bH;
            //            margin = h - bH;
          } else {
            margin = h + 30;
          }
        } else {
          // on subsequent pages just image - one panel
          $(".flip-page .page-inner")
            .not(":eq(0)")
            .css({
              height: $(".flip-page .group-left-inner").outerHeight() - bH
            });
        }
      });

      /*
      insert product buttons into every page
       */
      $(".main-container").on("productButtonsInit", function () {
        var $buttons = $(".group-product");
        $(this)
          .find(".flip-page .group-left-inner")
          .first()
          .append($buttons.clone().addClass("group-product-inner"));
        $imageFlipper.trigger("resizeElForProductFit", ["first"]);
        $buttons.css({ opacity: 0 });
      });

      // function curImage(curPage) {
      //   var afd;
      //   return $(".turn-page-wrapper")
      //     .attr("page", curPage)
      //     .find("img");
      // }

      /*
      initFlipper
      initialize the primary flipper element
       */
      $imageFlipper.on("initFlipper", function () {
        var $mag = $(this);
        $mag.imagesLoaded(function () {
          $mag.addClass("image-flipper");
          $mag.children().each(function () {
            $(this)
              .children()
              .wrapAll('<div class="page-inner"></div>');
          });

          // center our element based on with of side columns
          var flpW = $imageFlipper.width();
          // var flpH = $(window).height() - 220;
          var flpH = $imageFlipper.find("img").height();
          $mag.css({
            margin: 0,
            width: flpW,
            height: flpH
          });

          $mag.turn({
            display: "single",
            duration: 1200,
            acceleration: $.isTouch,
            gradients: !$.isTouch,
            width: flpW,
            height: flpH,
            autoCenter: true,
            elevation: 350,
            max: 3,
            when: {
              start: function (event, page, pageObject) {
                var buttons = $(".node.node-issue.view-mode-full").children(
                  ".group-product"
                );
                if ($(this).turn("view") == 1) {
                  buttons.animate({ opacity: 0 });
                } else {
                  buttons.animate({ opacity: 1 });
                }
              },
              turning: function (event, page, pageObject) {
                $mag.find("img").addClass("loaded");
                $mag.find(".next-wrapper").addClass("loaded");
                //              fitPageElements(page);
              }
            }
          });

          //        fitPageElements($mag.turn('page'));

          /*
           click control - simplify clicks on the image
           */
          $mag.click(function (e) {
            // not for editions or products or those missing blurbs
            $(this)
              .find("#side-2")
              .each(function () {
                var txt = $(this).text();
                //            console.log(txt + 'right rgh8t');
              });
            if (
              !$(this)
                .parents("body")
                .hasClass("node-type-edition")
              //            ||
            ) {
              var offset = $(this).offset();
              var relativeX = e.pageX - offset.left;
              if (relativeX < flpW / 2) {
                $mag.turn("previous");
              } else {
                $mag.turn("next");
              }
            }
          });

          $(window).resize(function () {
            var flpW = $(window).width() - 440;
            //          var flpW = $imageFlipper.width();
            // var flpH = $(window).height() - 220;
            var flpH = $mag
              .find("img")
              .parent()
              .outerHeight();
            //          var size = $(window).width() - (220 * 2);
            //          console.log('resize');
            $mag.turn("size", flpW, flpH);
            // resize product button sibling size
            $imageFlipper.trigger("resizeElForProductFit", ["first"]);
          });

          $main.trigger("setImageCounter", [
            $mag.turn("page"),
            $mag.turn("pages")
          ]);
          $mag.find(".turn-page-wrapper img").addClass("loaded");
          setTimeout(function () {
            $mag.find(".turn-page-wrapper .group-product").addClass("loaded");
            $(".next-wrapper").addClass("loaded");
          }, 200);
          // add left/right panels
          // let markup = "<div data='left'></div><div data='right'></div>";
          // $mag.find(".turn-page-wrapper").prepend(markup);
        });
      });

      /*
        MAIN ONCE
        ** init article reference area, which displays articles for this issue in
        *  color full width spread followed by sticky footer
         */
      $(".main-container", context).once(function () {
        $(window).resize(function () {
          //          console.log($('.group-left-inner').height());
        });

        /*
         image flipper, assemble & initiate
         */
        var $book = $(".group-center .field-name-flip-book");

        // empty flip book of needless markup
        // note - no longer necessary, b/c there is content we need now
        //        $book.children().remove();
        // make first page of flipbook from the 2 page text & image elements
        $book.siblings().wrapAll('<div class="flip-page"></div>');

        // insert first page into flipbook
        $book.append($book.siblings());
        // wrap & insert subsequent page elements
        $(".field-name-field-issue-page-image .field-item").each(function () {
          $book.append($(this).addClass("flip-page measure"));
        });

        // move last page w/ with link to next item to end
        $book.find(".next-wrapper").each(function () {
          var $next_item = $(this).closest(".field-items");
          $book.append($next_item);
        });

        /*
         for mobile, rearrange for simple display, for computer
         rearrange for flipper display
          */
        if ($(window).width() <= 414) {
          // $(".field-name-flip-book .field-name-body").insertBefore(
          //   $(".field-name-field-issue-ref-article")
          // );
          $(".group-product.field-group-div").insertAfter(
            $(".group-left-inner.field-group-div .field-name-body .field-item")
          );
          $('.group-right-inner .field-name-field-image').insertBefore('.group-left-inner.field-group-div .field-name-body');
          $('.group-right-inner').remove();
        } else {
          $book.trigger("initFlipper");
          $book.trigger("initFlipBg");

          /*
           trigger custom layout for gradient / scroll on text
           */
          //          $imageFlipper.trigger('initFirstPageFlipCustom');
          $(this).trigger("productButtonsInit");

          /*
           fit all elements for height in the page el
           */
          //        $imageFlipper.trigger('fitPageElements');

          /*
           turn.js keyboard controls
           */
          $(window).bind("keydown", function (e) {
            if (e.keyCode == 37) $imageFlipper.turn("previous");
            else if (e.keyCode == 39) $imageFlipper.turn("next");
          });

          // function pagerVisibility() {
          //   if ($imageFlipper.turn("page") == 1) {
          //     $("#prev").fadeOut();
          //     $("#next").fadeIn();
          //   } else if (
          //     $imageFlipper.turn("page") == $imageFlipper.turn("pages")
          //   ) {
          //     $("#next").fadeOut();
          //     $("#prev").fadeIn();
          //   } else {
          //     $("#next").fadeIn();
          //     $("#prev").fadeIn();
          //   }
          // }

          /*
           pager controls
           */
          // pagerVisibility();
          // $("#next").click(function(e) {
          //   e.preventDefault();
          //   $imageFlipper.turn("next");
          //   pagerVisibility();
          // });
          // $("#prev").click(function(e) {
          //   e.preventDefault();
          //   $imageFlipper.turn("previous");
          //   pagerVisibility();
          // });
          /*
           move pager
           */
          $(".field-name-custom-pagination").insertBefore(
            "#block-spike-caption-insert-caption-sidebar"
          );

          /* click trigger on thumbnail to jump back to main img */

          $thumbs.find("img").click(function () {
            var i = $(this)
              .parents(".field-item")
              .index();
            var asfd;
            $thumbs.trigger("checkThumbPos");
            $main.trigger("thumbClick", [i]);

            // change slide
            //          $('a[data-slidesjs-item="' + i + '"]').trigger('click');
            $main.trigger("setImageCounter", [
              $imageFlipper.turn("page"),
              $imageFlipper.turn("pages")
            ]);
          });

          // article ref
          var $pageOut = $(this).find(".field-name-field-issue-ref-article");
          //        var $pageOut = $(this).find('.field-name-field-article-ref-issue');
          var $masonry = $pageOut.children(".field-items");
          var $wrapper = $('<div id="mas-wrapper"></div>');
          $masonry.wrap($wrapper);
          initMasonryEl($masonry);

          //        console.log()

          // if no articles referenced, change to white spike below
          if ($pageOut.size() == 0) {
            $("body").addClass("no-referenced-articles");
            var asdf;
            //          $('#sticky-cover').css({
            //            'background-image': 'url("/sites/all/themes/spike/images/white-up.svg") !important'
            //          });
          }
        }
      });
      /* end .once('main-container') */
    }
  };
})(jQuery);
