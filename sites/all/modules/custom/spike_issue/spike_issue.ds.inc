<?php
/**
 * @file
 * spike_issue.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function spike_issue_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|issue|full';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'issue';
  $ds_fieldsetting->view_mode = 'full';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'subscribe_button' => array(
      'weight' => '7',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'flip_book' => array(
      'weight' => '2',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|issue|full'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|issue|teaser';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'issue';
  $ds_fieldsetting->view_mode = 'teaser';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '3',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '1',
        'wrapper' => 'h4',
        'class' => '',
      ),
    ),
    'issue_image_compound' => array(
      'weight' => '2',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|issue|teaser'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function spike_issue_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|issue|form';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'issue';
  $ds_layout->view_mode = 'form';
  $ds_layout->layout = 'ds_2col';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'title',
        1 => 'body',
        2 => 'field_blurb',
        3 => 'field_display_extra_ref',
        4 => 'path',
      ),
      'right' => array(
        5 => 'language',
        6 => 'field_current',
        7 => 'field_image',
        8 => 'field_issue_page_image',
        9 => 'field_issue_ref_article',
      ),
      'hidden' => array(
        10 => 'field_online_bool',
        11 => 'field_season_term',
        12 => 'field_article_ref_issue',
        13 => 'field_article_lang_version_ref',
        14 => 'field_product',
        15 => 'metatags',
        16 => '_add_existing_field',
      ),
    ),
    'fields' => array(
      'title' => 'left',
      'body' => 'left',
      'field_blurb' => 'left',
      'field_display_extra_ref' => 'left',
      'path' => 'left',
      'language' => 'right',
      'field_current' => 'right',
      'field_image' => 'right',
      'field_issue_page_image' => 'right',
      'field_issue_ref_article' => 'right',
      'field_online_bool' => 'hidden',
      'field_season_term' => 'hidden',
      'field_article_ref_issue' => 'hidden',
      'field_article_lang_version_ref' => 'hidden',
      'field_product' => 'hidden',
      'metatags' => 'hidden',
      '_add_existing_field' => 'hidden',
    ),
    'classes' => array(),
    'wrappers' => array(
      'left' => 'div',
      'right' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => 0,
  );
  $export['node|issue|form'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|issue|full';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'issue';
  $ds_layout->view_mode = 'full';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'title',
        1 => 'group_center',
        2 => 'group_product',
        3 => 'flip_book',
        4 => 'group_left_inner',
        5 => 'field_issue_page_image',
        6 => 'field_issue_ref_article',
        7 => 'group_right_inner',
        8 => 'field_image',
        9 => 'body',
        10 => 'field_product',
        11 => 'subscribe_button',
      ),
    ),
    'fields' => array(
      'title' => 'ds_content',
      'group_center' => 'ds_content',
      'group_product' => 'ds_content',
      'flip_book' => 'ds_content',
      'group_left_inner' => 'ds_content',
      'field_issue_page_image' => 'ds_content',
      'field_issue_ref_article' => 'ds_content',
      'group_right_inner' => 'ds_content',
      'field_image' => 'ds_content',
      'body' => 'ds_content',
      'field_product' => 'ds_content',
      'subscribe_button' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
    'hide_page_title' => '1',
    'page_option_title' => '',
  );
  $export['node|issue|full'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|issue|mini_teaser';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'issue';
  $ds_layout->view_mode = 'mini_teaser';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_image',
      ),
    ),
    'fields' => array(
      'field_image' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|issue|mini_teaser'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|issue|teaser';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'issue';
  $ds_layout->view_mode = 'teaser';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_display_extra_ref',
        1 => 'issue_image_compound',
        2 => 'title',
        3 => 'field_product',
      ),
    ),
    'fields' => array(
      'field_display_extra_ref' => 'ds_content',
      'issue_image_compound' => 'ds_content',
      'title' => 'ds_content',
      'field_product' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['node|issue|teaser'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|issue|teaser_image';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'issue';
  $ds_layout->view_mode = 'teaser_image';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_image',
      ),
    ),
    'fields' => array(
      'field_image' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|issue|teaser_image'] = $ds_layout;

  return $export;
}

/**
 * Implements hook_ds_view_modes_info().
 */
function spike_issue_ds_view_modes_info() {
  $export = array();

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'mini_basic';
  $ds_view_mode->label = 'mini_basic';
  $ds_view_mode->entities = array(
    'node' => 'node',
  );
  $export['mini_basic'] = $ds_view_mode;

  return $export;
}
