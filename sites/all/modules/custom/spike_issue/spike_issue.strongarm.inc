<?php
/**
 * @file
 * spike_issue.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function spike_issue_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_node__issue';
  $strongarm->value = array(
    'view_modes' => array(
      'teaser' => array(
        'custom_settings' => TRUE,
      ),
      'full' => array(
        'custom_settings' => TRUE,
      ),
      'rss' => array(
        'custom_settings' => FALSE,
      ),
      'search_index' => array(
        'custom_settings' => FALSE,
      ),
      'search_result' => array(
        'custom_settings' => FALSE,
      ),
      'token' => array(
        'custom_settings' => FALSE,
      ),
      'revision' => array(
        'custom_settings' => FALSE,
      ),
      'mini_teaser' => array(
        'custom_settings' => TRUE,
      ),
      'sidebar' => array(
        'custom_settings' => FALSE,
      ),
      'teaser_shop' => array(
        'custom_settings' => TRUE,
      ),
      'gallery' => array(
        'custom_settings' => FALSE,
      ),
      'basic_info' => array(
        'custom_settings' => FALSE,
      ),
      'reference' => array(
        'custom_settings' => FALSE,
      ),
      'related_content' => array(
        'custom_settings' => TRUE,
      ),
      'teaser_image' => array(
        'custom_settings' => TRUE,
      ),
      'mini_basic' => array(
        'custom_settings' => TRUE,
      ),
      'image_gallery' => array(
        'custom_settings' => FALSE,
      ),
      'teaser_square' => array(
        'custom_settings' => FALSE,
      ),
      'checkout_pane' => array(
        'custom_settings' => TRUE,
      ),
      'print' => array(
        'custom_settings' => FALSE,
      ),
      'teaser_product_front' => array(
        'custom_settings' => FALSE,
      ),
    ),
    'extra_fields' => array(
      'form' => array(
        'title' => array(
          'weight' => '0',
        ),
        'path' => array(
          'weight' => '4',
        ),
        'language' => array(
          'weight' => '5',
        ),
        'flag' => array(
          'weight' => '11',
        ),
        'metatags' => array(
          'weight' => '15',
        ),
      ),
      'display' => array(
        'product:sku' => array(
          'teaser' => array(
            'weight' => '20',
            'visible' => FALSE,
          ),
          'full' => array(
            'weight' => '38',
            'visible' => FALSE,
          ),
          'default' => array(
            'weight' => '-10',
            'visible' => FALSE,
          ),
          'teaser_shop' => array(
            'weight' => '27',
            'visible' => FALSE,
          ),
          'basic_info' => array(
            'weight' => '-10',
            'visible' => FALSE,
          ),
          'related_content' => array(
            'weight' => '8',
            'visible' => FALSE,
          ),
          'teaser_image' => array(
            'weight' => '20',
            'visible' => FALSE,
          ),
          'mini_basic' => array(
            'weight' => '-10',
            'visible' => FALSE,
          ),
        ),
        'product:title' => array(
          'teaser' => array(
            'weight' => '21',
            'visible' => FALSE,
          ),
          'full' => array(
            'weight' => '39',
            'visible' => FALSE,
          ),
          'default' => array(
            'weight' => '-5',
            'visible' => FALSE,
          ),
          'teaser_shop' => array(
            'weight' => '28',
            'visible' => FALSE,
          ),
          'basic_info' => array(
            'weight' => '-5',
            'visible' => FALSE,
          ),
          'related_content' => array(
            'weight' => '9',
            'visible' => FALSE,
          ),
          'teaser_image' => array(
            'weight' => '21',
            'visible' => FALSE,
          ),
          'mini_basic' => array(
            'weight' => '-5',
            'visible' => FALSE,
          ),
        ),
        'product:status' => array(
          'teaser' => array(
            'weight' => '22',
            'visible' => FALSE,
          ),
          'full' => array(
            'weight' => '41',
            'visible' => FALSE,
          ),
          'default' => array(
            'weight' => '5',
            'visible' => FALSE,
          ),
          'teaser_shop' => array(
            'weight' => '29',
            'visible' => FALSE,
          ),
          'basic_info' => array(
            'weight' => '5',
            'visible' => FALSE,
          ),
          'related_content' => array(
            'weight' => '38',
            'visible' => FALSE,
          ),
          'teaser_image' => array(
            'weight' => '22',
            'visible' => FALSE,
          ),
          'mini_basic' => array(
            'weight' => '5',
            'visible' => FALSE,
          ),
        ),
        'product:commerce_price' => array(
          'teaser' => array(
            'weight' => '6',
            'visible' => FALSE,
          ),
          'full' => array(
            'weight' => '36',
            'visible' => FALSE,
          ),
          'default' => array(
            'weight' => '0',
            'visible' => TRUE,
          ),
          'teaser_shop' => array(
            'weight' => '6',
            'visible' => TRUE,
          ),
          'basic_info' => array(
            'weight' => '0',
            'visible' => TRUE,
          ),
          'related_content' => array(
            'weight' => '29',
            'visible' => FALSE,
          ),
          'teaser_image' => array(
            'weight' => '6',
            'visible' => FALSE,
          ),
          'mini_basic' => array(
            'weight' => '0',
            'visible' => FALSE,
          ),
        ),
        'product:commerce_stock' => array(
          'teaser' => array(
            'weight' => '27',
            'visible' => FALSE,
          ),
          'full' => array(
            'weight' => '42',
            'visible' => FALSE,
          ),
          'default' => array(
            'weight' => '36',
            'visible' => TRUE,
          ),
          'teaser_shop' => array(
            'weight' => '34',
            'visible' => FALSE,
          ),
          'basic_info' => array(
            'weight' => '36',
            'visible' => TRUE,
          ),
          'related_content' => array(
            'weight' => '7',
            'visible' => FALSE,
          ),
          'teaser_image' => array(
            'weight' => '27',
            'visible' => FALSE,
          ),
          'mini_basic' => array(
            'weight' => '36',
            'visible' => FALSE,
          ),
        ),
        'language' => array(
          'full' => array(
            'weight' => '34',
            'visible' => FALSE,
          ),
          'default' => array(
            'weight' => '0',
            'visible' => TRUE,
          ),
          'mini_basic' => array(
            'weight' => '0',
            'visible' => FALSE,
          ),
          'basic_info' => array(
            'weight' => '0',
            'visible' => TRUE,
          ),
        ),
      ),
    ),
  );
  $export['field_bundle_settings_node__issue'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'i18n_node_extended_issue';
  $strongarm->value = '1';
  $export['i18n_node_extended_issue'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'i18n_node_options_issue';
  $strongarm->value = array(
    0 => 'current',
    1 => 'required',
  );
  $export['i18n_node_options_issue'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'language_content_type_issue';
  $strongarm->value = '2';
  $export['language_content_type_issue'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_issue';
  $strongarm->value = array(
    0 => 'main-menu',
  );
  $export['menu_options_issue'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_issue';
  $strongarm->value = 'main-menu:0';
  $export['menu_parent_issue'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_issue';
  $strongarm->value = array(
    0 => 'status',
  );
  $export['node_options_issue'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_issue';
  $strongarm->value = '0';
  $export['node_preview_issue'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_issue';
  $strongarm->value = 0;
  $export['node_submitted_issue'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_node_issue_de_pattern';
  $strongarm->value = '';
  $export['pathauto_node_issue_de_pattern'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_node_issue_en_pattern';
  $strongarm->value = '';
  $export['pathauto_node_issue_en_pattern'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_node_issue_pattern';
  $strongarm->value = '';
  $export['pathauto_node_issue_pattern'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_node_issue_und_pattern';
  $strongarm->value = '';
  $export['pathauto_node_issue_und_pattern'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_taxonomy_term_season_pattern';
  $strongarm->value = '';
  $export['pathauto_taxonomy_term_season_pattern'] = $strongarm;

  return $export;
}
