<?php
/**
 * @file
 * spike_issue.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function spike_issue_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_center|node|issue|full';
  $field_group->group_name = 'group_center';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'issue';
  $field_group->mode = 'full';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Center',
    'weight' => '1',
    'children' => array(
      0 => 'flip_book',
      1 => 'group_left_inner',
      2 => 'group_right_inner',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Center',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-center field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'none',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_center|node|issue|full'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_left_inner|node|issue|full';
  $field_group->group_name = 'group_left_inner';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'issue';
  $field_group->mode = 'full';
  $field_group->parent_name = 'group_center';
  $field_group->data = array(
    'label' => 'Left',
    'weight' => '3',
    'children' => array(
      0 => 'body',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Left',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-left-inner field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'none',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_left_inner|node|issue|full'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_product|node|issue|full';
  $field_group->group_name = 'group_product';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'issue';
  $field_group->mode = 'full';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Product grouping',
    'weight' => '2',
    'children' => array(
      0 => 'field_product',
      1 => 'subscribe_button',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Product grouping',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-product field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'none',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_product|node|issue|full'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_right_inner|node|issue|full';
  $field_group->group_name = 'group_right_inner';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'issue';
  $field_group->mode = 'full';
  $field_group->parent_name = 'group_center';
  $field_group->data = array(
    'label' => 'Right',
    'weight' => '4',
    'children' => array(
      0 => 'field_image',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Right',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-right-inner field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'none',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_right_inner|node|issue|full'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Center');
  t('Left');
  t('Product grouping');
  t('Right');

  return $field_groups;
}
