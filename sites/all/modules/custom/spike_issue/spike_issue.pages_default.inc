<?php
/**
 * @file
 * spike_issue.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function spike_issue_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'issues';
  $page->task = 'page';
  $page->admin_title = 'Issues';
  $page->admin_description = '';
  $page->path = 'issues';
  $page->access = array();
  $page->menu = array(
    'type' => 'normal',
    'title' => 'Issues',
    'name' => 'main-menu',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_issues_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'issues';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Magazines';
  $display->uuid = '770ddf2a-7360-4966-bb73-f3f43e159947';
  $display->storage_type = 'page_manager';
  $display->storage_id = 'page_issues_panel_context';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-ff73d10d-ca4d-4425-95d3-4bc5ea4c3e0d';
  $pane->panel = 'middle';
  $pane->type = 'views_panes';
  $pane->subtype = 'spike_issues-panel_pane_1';
  $pane->shown = FALSE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'ff73d10d-ca4d-4425-95d3-4bc5ea4c3e0d';
  $display->content['new-ff73d10d-ca4d-4425-95d3-4bc5ea4c3e0d'] = $pane;
  $display->panels['middle'][0] = 'new-ff73d10d-ca4d-4425-95d3-4bc5ea4c3e0d';
  $pane = new stdClass();
  $pane->pid = 'new-9f48da63-c5a0-4cdb-8dae-5423f254c7b6';
  $pane->panel = 'middle';
  $pane->type = 'views_panes';
  $pane->subtype = 'spike_issues-panel_pane_2';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '9f48da63-c5a0-4cdb-8dae-5423f254c7b6';
  $display->content['new-9f48da63-c5a0-4cdb-8dae-5423f254c7b6'] = $pane;
  $display->panels['middle'][1] = 'new-9f48da63-c5a0-4cdb-8dae-5423f254c7b6';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-ff73d10d-ca4d-4425-95d3-4bc5ea4c3e0d';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['issues'] = $page;

  return $pages;

}
