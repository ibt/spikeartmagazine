<?php
/**
 * @file
 * spike_issue.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function spike_issue_taxonomy_default_vocabularies() {
  return array(
    'season' => array(
      'name' => 'Season',
      'machine_name' => 'season',
      'description' => 'Seasons of the year for a quarterly magazine',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'language' => 'und',
      'i18n_mode' => 0,
      'base_i18n_mode' => 0,
      'base_language' => 'und',
    ),
  );
}
