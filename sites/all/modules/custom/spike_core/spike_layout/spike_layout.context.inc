<?php
/**
 * @file
 * spike_layout.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function spike_layout_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'admin_node_add';
  $context->description = 'add admin page classes for id';
  $context->tag = 'admin';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'node/add' => 'node/add',
      ),
    ),
    'user' => array(
      'values' => array(
        'administrator' => 'administrator',
        'dev_op_test' => 'dev_op_test',
        'operator' => 'operator',
      ),
    ),
  );
  $context->reactions = array(
    'theme_html' => array(
      'class' => 'admin-node-add',
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('add admin page classes for id');
  t('admin');
  $export['admin_node_add'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'every_page';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'spike_lang-language_control' => array(
          'module' => 'spike_lang',
          'delta' => 'language_control',
          'region' => 'navigation',
          'weight' => '-10',
        ),
        'spike_search-spike_search_form' => array(
          'module' => 'spike_search',
          'delta' => 'spike_search_form',
          'region' => 'navigation',
          'weight' => '-9',
        ),
        'spike_newsletter-spike_subscribe_block_navbar' => array(
          'module' => 'spike_newsletter',
          'delta' => 'spike_subscribe_block_navbar',
          'region' => 'navigation',
          'weight' => '-8',
        ),
        'spike_layout-spike_main_menu_block' => array(
          'module' => 'spike_layout',
          'delta' => 'spike_main_menu_block',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
        'views-spike_issues-block_2' => array(
          'module' => 'views',
          'delta' => 'spike_issues-block_2',
          'region' => 'sidebar_first',
          'weight' => '-9',
        ),
        'spike_layout-spike_subscribe_block' => array(
          'module' => 'spike_layout',
          'delta' => 'spike_subscribe_block',
          'region' => 'sidebar_first',
          'weight' => '-8',
        ),
        'views--exp-search-page' => array(
          'module' => 'views',
          'delta' => '-exp-search-page',
          'region' => 'sidebar_first',
          'weight' => '-7',
        ),
        'spike_newsletter-spike_subscribe_block' => array(
          'module' => 'spike_newsletter',
          'delta' => 'spike_subscribe_block',
          'region' => 'sidebar_first',
          'weight' => '-6',
        ),
        'spike_layout-themeBlock_social' => array(
          'module' => 'spike_layout',
          'delta' => 'themeBlock_social',
          'region' => 'sidebar_first',
          'weight' => '-5',
        ),
        'spike_layout-themeBlock_copyright' => array(
          'module' => 'spike_layout',
          'delta' => 'themeBlock_copyright',
          'region' => 'sidebar_first',
          'weight' => '-4',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;
  $export['every_page'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'logo_header';
  $context->description = 'determines when to display the logo header image';
  $context->tag = '';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
    'user' => array(
      'values' => array(
        'anonymous user' => 'anonymous user',
        'editor' => 'editor',
        'operator' => 'operator',
      ),
    ),
  );
  $context->reactions = array();
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('determines when to display the logo header image');
  $export['logo_header'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'non_front';
  $context->description = 'context for every page w/o the splash page - non-front';
  $context->tag = '';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '~<front>' => '~<front>',
        '~subjects' => '~subjects',
        '~contributors' => '~contributors',
      ),
    ),
    'theme' => array(
      'values' => array(
        'spike' => 'spike',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'spike_lang-language_control' => array(
          'module' => 'spike_lang',
          'delta' => 'language_control',
          'region' => 'header',
          'weight' => '-10',
        ),
      ),
    ),
    'theme_html' => array(
      'class' => 'non-front-lang',
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('context for every page w/o the splash page - non-front');
  $export['non_front'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'operator';
  $context->description = 'controls & context for the operator';
  $context->tag = '';
  $context->conditions = array(
    'user' => array(
      'values' => array(
        'editor' => 'editor',
        'operator' => 'operator',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'system-user-menu' => array(
          'module' => 'system',
          'delta' => 'user-menu',
          'region' => 'footer',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('controls & context for the operator');
  $export['operator'] = $context;

  $context = new stdClass();
  $context->disabled = TRUE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'sidebars';
  $context->description = 'Sidebars should be active';
  $context->tag = '';
  $context->conditions = array(
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'spike_layout-spike_main_menu_block' => array(
          'module' => 'spike_layout',
          'delta' => 'spike_main_menu_block',
          'region' => 'sidebar_first',
          'weight' => '-9',
        ),
        'spike_layout-spike_feed_block' => array(
          'module' => 'spike_layout',
          'delta' => 'spike_feed_block',
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Sidebars should be active');
  $export['sidebars'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'sidebars_article';
  $context->description = 'Sidebars on node article';
  $context->tag = 'sidebars';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'article' => 'article',
        'edition' => 'edition',
        'external_product' => 'external_product',
        'image_article' => 'image_article',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
    'path' => array(
      'values' => array(
        'node/*' => 'node/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-160a9d9bc28a683d3d9371fec44bac4d' => array(
          'module' => 'views',
          'delta' => '160a9d9bc28a683d3d9371fec44bac4d',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'spike_layout-spike_share' => array(
          'module' => 'spike_layout',
          'delta' => 'spike_share',
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
        'views-article_sidebar_captions-block' => array(
          'module' => 'views',
          'delta' => 'article_sidebar_captions-block',
          'region' => 'sidebar_second',
          'weight' => '-7',
        ),
      ),
    ),
    'theme_html' => array(
      'class' => 'sidebar-articles',
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Sidebars on node article');
  t('sidebars');
  $export['sidebars_article'] = $context;

  $context = new stdClass();
  $context->disabled = TRUE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'sidebars_front';
  $context->description = 'Sidebars front style with menu & recent content';
  $context->tag = 'sidebars';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'spike_layout-spike_main_menu_block' => array(
          'module' => 'spike_layout',
          'delta' => 'spike_main_menu_block',
          'region' => 'sidebar_first',
          'weight' => '-9',
        ),
        'spike_layout-spike_feed_block' => array(
          'module' => 'spike_layout',
          'delta' => 'spike_feed_block',
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Sidebars front style with menu & recent content');
  t('sidebars');
  $export['sidebars_front'] = $context;

  $context = new stdClass();
  $context->disabled = TRUE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'sidebars_standard';
  $context->description = 'front page 2nd col sidebar recent content';
  $context->tag = 'sidebars';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
        'news' => 'news',
        'glish' => 'glish',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'spike_layout-spike_feed_block' => array(
          'module' => 'spike_layout',
          'delta' => 'spike_feed_block',
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
      ),
    ),
    'theme_html' => array(
      'class' => 'sidebar-standard',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('front page 2nd col sidebar recent content');
  t('sidebars');
  $export['sidebars_standard'] = $context;

  return $export;
}
