<!--@NOTE ~ THIS MARKUP IS LIFTED FROM :-->
<!--ds-1col--node-quotation.tpl.php IN SPIKE THEME templates FOLDER-->
<div class="spike-banded-element"><div class="inside">
    <div class="breakpoint-spike bp-spike-before breakpoint-el"></div>
    <div class="breakpoint-shadow shadow-top breakpoint-el"></div>
    <div<?php print drupal_attributes($style_attributes); ?>>
      <?php
      $breakpoint=TRUE;
      print '<h2 class="pane-title">' . render($content->title) . '</h2>';
      print render($content->content);
      ?>
    </div>
    <div class="breakpoint-spike  bp-spike-after"></div><div class="breakpoint-shadow shadow-bottom"></div>
  </div></div>