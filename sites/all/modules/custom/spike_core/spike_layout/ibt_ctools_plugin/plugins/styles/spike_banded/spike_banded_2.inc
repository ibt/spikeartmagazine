<?php

$plugin = array(
  // Title and description of the plugin
  'title' => t('Spike Banded Stripe 2'),
  'description' => t('Spike banded stripe formatter - BETTER'),
  // Define a theme function for this plugin
  'render pane' => 'spike_banded_style_2_render_pane',
  // We'll be using a template for rendering
  'hook theme' => array(
    'spike_banded_style_2' => array(
      'variables' => array(
        'content' => NULL,
        'style_attributes' => array(),
      ),
      'path' => drupal_get_path('module', 'ibt_ctools_plugin') . '/plugins/styles/spike_banded',
      'template' => 'spike-banded-2',
    ),
  ),
  // This defines the settings form for the plugin
  'settings form' => 'spike_banded_style_settings_form',
);


/**
 * Render callback.
 */
function spike_banded_style_2_render_pane($vars) {
  $settings = $vars['settings'];
  $content = $vars['content'];
  return theme('spike_banded_style', array('content' => $content, 'settings' => $settings));
}
