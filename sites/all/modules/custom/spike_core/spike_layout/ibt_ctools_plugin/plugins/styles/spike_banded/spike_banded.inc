<?php
// Plugin definition
$plugin = array(
  // Title and description of the plugin
  'title' => t('Spike Banded Stripe'),
  'description' => t('Spike banded stripe formatter'),
  // Define a theme function for this plugin
  'render pane' => 'spike_banded_style_render_pane',
  // We'll be using a template for rendering
  'hook theme' => array(
    'spike_banded_style' => array(
      'variables' => array(
        'content' => NULL,
        'style_attributes' => array(),
      ),
      'path' => drupal_get_path('module', 'ibt_ctools_plugin') . '/plugins/styles/spike_banded',
      'template' => 'spike-banded',
    ),
  ),
  // This defines the settings form for the plugin
  'settings form' => 'spike_banded_style_settings_form',
);



/**
 * Settings form callback.
 */
function spike_banded_style_settings_form($form, $form_state) {
  $options = array(
    'pane-nostyle' => t('Plain / no styles / transparent background'),
    'pane-patterned' => t('Patterned'),
    'pane-reverse' => t('Reverse color values'),
    'callout-orange' => t('Callout: Orange'),
    'callout-blue' => t('Callout: Blue')
  );
  $headings = array(
    'pane-heading-default' => t('Describe Default Heading Style Here'),
    'pane-heading-secondary' => t('Describe Secondary Heading Style Here'),
    'pane-heading-tertiary' => t('Describe Tertiary Heading Style Here')
  );
  $form['css_classes'] = array(
    '#type' => 'radios',
    '#title' => t('Custom Pane and Block Styles'),
    '#description' => t('Choose a style for your pane or block.'),
    '#options' => $options,
    '#required' => FALSE,
    '#default_value' => (isset($style_settings['css_classes'])) ? $style_settings['css_classes'] : 'pane-nostyle',
  );
  $form['heading_classes'] = array(
    '#type' => 'radios',
    '#title' => t('Header Styles'),
    '#description' => t('Choose a header style for your pane or block.'),
    '#options' => $headings,
    '#required' => FALSE,
    '#default_value' => (isset($style_settings['heading_classes'])) ? $style_settings['heading_classes'] : '',
  );

  return $form;
}


/**
 * Render callback.
 */
function theme_spike_banded_style_render_pane($vars) {
  $settings = $vars['settings'];
  $content = $vars['content'];
  return theme('spike_banded_style', array('content' => $content, 'settings' => $settings));
}
