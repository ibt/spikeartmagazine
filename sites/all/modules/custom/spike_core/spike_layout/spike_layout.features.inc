<?php
/**
 * @file
 * spike_layout.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function spike_layout_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function spike_layout_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function spike_layout_image_default_styles() {
  $styles = array();

  // Exported image style: advert_teaser.
  $styles['advert_teaser'] = array(
    'label' => 'advert_teaser',
    'effects' => array(
      28 => array(
        'name' => 'coloractions_convert',
        'data' => array(
          'format' => 'image/png',
          'quality' => 75,
        ),
        'weight' => -10,
      ),
      27 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 300,
          'height' => '',
          'upscale' => 1,
          'retinafy' => 1,
        ),
        'weight' => -9,
      ),
    ),
  );

  // Exported image style: blur_test.
  $styles['blur_test'] = array(
    'label' => 'blur_test',
    'effects' => array(
      17 => array(
        'name' => 'iek_image_filter',
        'data' => array(
          'filter_name' => 7,
          'repeat' => 60,
          'arg1' => 10,
          'arg2' => 0,
          'arg3' => 0,
          'arg4' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: full_width_blur.
  $styles['full_width_blur'] = array(
    'label' => 'full_width_blur',
    'effects' => array(
      17 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 2200,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => -10,
      ),
      18 => array(
        'name' => 'iek_image_filter',
        'data' => array(
          'filter_name' => 7,
          'repeat' => 30,
          'arg1' => 10,
          'arg2' => 0,
          'arg3' => 0,
          'arg4' => 0,
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: large_plus.
  $styles['large_plus'] = array(
    'label' => 'large plus',
    'effects' => array(),
  );

  // Exported image style: medium_400_x_420.
  $styles['medium_400_x_420'] = array(
    'label' => 'medium 400 x 420',
    'effects' => array(
      15 => array(
        'name' => 'imagefield_focus_scale_and_crop',
        'data' => array(
          'width' => 400,
          'height' => 420,
          'strength' => 'high',
          'fallback' => 'image',
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: medium_crop_300_x_373.
  $styles['medium_crop_300_x_373'] = array(
    'label' => 'medium crop 300 x 373',
    'effects' => array(
      14 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 300,
          'height' => 373,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: medium_square_400.
  $styles['medium_square_400'] = array(
    'label' => 'medium square 400',
    'effects' => array(
      24 => array(
        'name' => 'smartcrop_scale_and_crop',
        'data' => array(
          'width' => 400,
          'height' => 400,
          'upscale' => 1,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: spike_medium_300_w_bg.
  $styles['spike_medium_300_w_bg'] = array(
    'label' => 'spike medium 300 w and bg',
    'effects' => array(
      29 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 300,
          'height' => 373,
          'upscale' => 1,
          'retinafy' => 0,
        ),
        'weight' => -10,
      ),
      31 => array(
        'name' => 'coloractions_convert',
        'data' => array(
          'format' => 'image/png',
          'quality' => 75,
        ),
        'weight' => -9,
      ),
      32 => array(
        'name' => 'canvasactions_definecanvas',
        'data' => array(
          'RGB' => array(
            'HEX' => '',
          ),
          'under' => 1,
          'exact' => array(
            'width' => 300,
            'height' => 373,
            'xpos' => 'center',
            'ypos' => 'bottom',
          ),
          'relative' => array(
            'leftdiff' => '',
            'rightdiff' => '',
            'topdiff' => '',
            'bottomdiff' => '',
          ),
        ),
        'weight' => 3,
      ),
    ),
  );

  return $styles;
}
