<?php
/**
 * @file
 * spike_layout.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function spike_layout_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['spike_issue-spike_issue_sidebar'] = array(
    'cache' => 4,
    'custom' => 0,
    'delta' => 'spike_issue_sidebar',
    'module' => 'spike_issue',
    'node_types' => array(
      0 => 'issue',
    ),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bootstrap' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bootstrap',
        'weight' => 0,
      ),
      'ember' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'ember',
        'weight' => 0,
      ),
      'spike' => array(
        'region' => 'sidebar_second',
        'status' => 1,
        'theme' => 'spike',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  return $export;
}
