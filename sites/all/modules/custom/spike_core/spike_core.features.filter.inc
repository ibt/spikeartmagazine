<?php
/**
 * @file
 * spike_core.features.filter.inc
 */

/**
 * Implements hook_filter_default_formats().
 */
function spike_core_filter_default_formats() {
  $formats = array();

  // Exported format: caption_text.
  $formats['caption_text'] = array(
    'format' => 'caption_text',
    'name' => 'caption_text',
    'cache' => 1,
    'status' => 1,
    'weight' => -9,
    'filters' => array(
      'float_filter' => array(
        'weight' => -49,
        'status' => 1,
        'settings' => array(
          'img_tag' => 'span',
          'table_tag' => 'div',
          'figure_tag' => 'div',
        ),
      ),
      'filter_url' => array(
        'weight' => -48,
        'status' => 1,
        'settings' => array(
          'filter_url_length' => 72,
        ),
      ),
      'filter_autop' => array(
        'weight' => -46,
        'status' => 1,
        'settings' => array(),
      ),
      'filter_html' => array(
        'weight' => -45,
        'status' => 1,
        'settings' => array(
          'allowed_html' => '<a> <em> <strong> <cite> <blockquote> <code> <ul> <ol> <li> <dl> <dt> <dd> <img> <div>',
          'filter_html_help' => 1,
          'filter_html_nofollow' => 0,
        ),
      ),
      'filter_htmlcorrector' => array(
        'weight' => -39,
        'status' => 1,
        'settings' => array(),
      ),
      'pathologic' => array(
        'weight' => -38,
        'status' => 1,
        'settings' => array(
          'local_paths' => '',
          'protocol_style' => 'full',
        ),
      ),
    ),
  );

  // Exported format: Filtered HTML.
  $formats['filtered_html'] = array(
    'format' => 'filtered_html',
    'name' => 'Filtered HTML',
    'cache' => 1,
    'status' => 1,
    'weight' => -10,
    'filters' => array(
      'video_filter' => array(
        'weight' => -1,
        'status' => 1,
        'settings' => array(
          'video_filter_width' => '',
          'video_filter_height' => '',
          'video_filter_autoplay' => 0,
          'video_filter_related' => 0,
          'video_filter_html5' => 1,
          'video_filter_codecs' => array(
            'dailymotion' => 'dailymotion',
            'flickr_video' => 'flickr_video',
            'google' => 'google',
            'instagram' => 'instagram',
            'metacafe' => 'metacafe',
            'myspace' => 'myspace',
            'vimeo' => 'vimeo',
            'vine' => 'vine',
            'youtube' => 'youtube',
            'youtube_playlist' => 'youtube_playlist',
            'archive' => 0,
            'bliptv' => 0,
            'candidcareer' => 0,
            'capped' => 0,
            'collegehumor' => 0,
            'coub' => 0,
            'democracynow_fullshow' => 0,
            'democracynow_story' => 0,
            'flickr_slideshows' => 0,
            'foxnews' => 0,
            'gametrailers' => 0,
            'gamevideos' => 0,
            'giphy' => 0,
            'godtube' => 0,
            'mailru' => 0,
            'myvideo' => 0,
            'picasa_slideshows' => 0,
            'rutube' => 0,
            'slideshare' => 0,
            'streamhoster' => 0,
            'teachertube' => 0,
            'ted' => 0,
            'twitch' => 0,
            'ustream' => 0,
            'vbox' => 0,
            'whatchado' => 0,
            'wistia' => 0,
            'youku' => 0,
          ),
          'video_filter_multiple_sources' => 1,
        ),
      ),
      'filter_autop' => array(
        'weight' => 0,
        'status' => 1,
        'settings' => array(),
      ),
      'filter_url' => array(
        'weight' => 0,
        'status' => 1,
        'settings' => array(
          'filter_url_length' => 72,
        ),
      ),
      'float_filter' => array(
        'weight' => 0,
        'status' => 1,
        'settings' => array(
          'img_tag' => 'span',
          'table_tag' => 'div',
          'figure_tag' => 'div',
        ),
      ),
      'filter_html' => array(
        'weight' => 1,
        'status' => 1,
        'settings' => array(
          'allowed_html' => '<a> <em> <strong> <cite> <blockquote> <code> <ul> <ol> <li> <dl> <dt> <dd> <br> <p> @[class|style] <img> <pre> <h2>@[class|style] <div>@[class|style] <iframe> @[class|style] <table> @[class|style] <th> <tr> <td>  @[class|style] <h1>@[class|style] <h3>@[class|style] <h4> @[class|style]<h5>@[class|style] <h6>@[class|style]',
          'filter_html_help' => 1,
          'filter_html_nofollow' => 0,
        ),
      ),
      'filter_htmlcorrector' => array(
        'weight' => 10,
        'status' => 1,
        'settings' => array(),
      ),
      'pathologic' => array(
        'weight' => 50,
        'status' => 1,
        'settings' => array(
          'local_paths' => '',
          'protocol_style' => 'full',
        ),
      ),
    ),
  );

  // Exported format: Full HTML.
  $formats['full_html'] = array(
    'format' => 'full_html',
    'name' => 'Full HTML',
    'cache' => 1,
    'status' => 1,
    'weight' => -8,
    'filters' => array(
      'filter_htmlcorrector' => array(
        'weight' => 10,
        'status' => 1,
        'settings' => array(),
      ),
    ),
  );

  return $formats;
}
