<?php
/**
 * @file
 * spike_core.features.inc
 */

/**
 * Implements hook_default_cer().
 */
function spike_core_default_cer() {
  $items = array();
  $items['node:article:field_article_lang_version_ref*node:article:field_article_lang_version_ref'] = entity_import('cer', '{
    "cer_bidirectional" : { "und" : [ { "value" : "1" } ] },
    "cer_enabled" : { "und" : [ { "value" : "1" } ] },
    "cer_left" : { "und" : [ { "path" : "node:article:field_article_lang_version_ref" } ] },
    "cer_right" : { "und" : [ { "path" : "node:article:field_article_lang_version_ref" } ] },
    "cer_weight" : { "und" : [ { "value" : "0" } ] },
    "identifier" : "node:article:field_article_lang_version_ref*node:article:field_article_lang_version_ref"
  }');
  $items['node:article:field_article_ref_article*node:article:field_article_ref_article'] = entity_import('cer', '{
    "cer_bidirectional" : { "und" : [ { "value" : "1" } ] },
    "cer_enabled" : { "und" : [ { "value" : "1" } ] },
    "cer_left" : { "und" : [ { "path" : "node:article:field_article_ref_article" } ] },
    "cer_right" : { "und" : [ { "path" : "node:article:field_article_ref_article" } ] },
    "cer_weight" : { "und" : [ { "value" : "0" } ] },
    "identifier" : "node:article:field_article_ref_article*node:article:field_article_ref_article"
  }');
  $items['node:article:field_article_ref_issue*node:issue:field_issue_ref_article'] = entity_import('cer', '{
    "cer_bidirectional" : { "und" : [ { "value" : "1" } ] },
    "cer_enabled" : { "und" : [ { "value" : "1" } ] },
    "cer_left" : { "und" : [ { "path" : "node:article:field_article_ref_issue" } ] },
    "cer_right" : { "und" : [ { "path" : "node:issue:field_issue_ref_article" } ] },
    "cer_weight" : { "und" : [ { "value" : "0" } ] },
    "identifier" : "node:article:field_article_ref_issue*node:issue:field_issue_ref_article"
  }');
  $items['node:issue:field_article_lang_version_ref*node:issue:field_article_lang_version_ref'] = entity_import('cer', '{
    "cer_bidirectional" : { "und" : [ { "value" : "1" } ] },
    "cer_enabled" : { "und" : [ { "value" : "1" } ] },
    "cer_left" : { "und" : [ { "path" : "node:issue:field_article_lang_version_ref" } ] },
    "cer_right" : { "und" : [ { "path" : "node:issue:field_article_lang_version_ref" } ] },
    "cer_weight" : { "und" : [ { "value" : "0" } ] },
    "identifier" : "node:issue:field_article_lang_version_ref*node:issue:field_article_lang_version_ref"
  }');
  return $items;
}

/**
 * Implements hook_ctools_plugin_api().
 */
function spike_core_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function spike_core_node_info() {
  $items = array(
    'page' => array(
      'name' => t('Basic page'),
      'base' => 'node_content',
      'description' => t('Use <em>basic pages</em> for your static content, such as an \'About us\' page.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
