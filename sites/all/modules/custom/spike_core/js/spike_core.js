// http://docs.ckeditor.com/#!/api/CKEDITOR.config

// Uses 2 spaces instead of a tab for indentation.
CKEDITOR.config.dataIndentationChars = '  ';
CKEDITOR.config.tabSpaces = 2;


// Resizes editor for specific fields when initially loaded.
(function(){
  var instanceNames = {
    'edit-field-my-field-und-0-value' : 200,
    'edit-field-my-other-field-und-0-value': 200,
    'edit-body-und-0-value': 350
  };

  CKEDITOR.on('instanceCreated', function(e){
    var instance = e.editor;

    if (instanceNames[instance.name] !== "undefined") {
      instance.on('instanceReady', function(e){
        var height  = instanceNames[instance.name];
        e.editor.resize('100%', height);
      });
    }
  });
})();