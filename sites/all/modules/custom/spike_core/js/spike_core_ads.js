(function ($) {
  Drupal.behaviors.articleAdInsertsForm = {
    attach: function (context, settings) {

      $(document).bind('focus', function(e){
        e.preventDefault();
      });

      // Add insert submit buttons behind all advert autocomplete fields.
      $('form .field-name-field-ads > * tbody tr td:nth-child(2)').once('articleAdInsertsForm', function (i) {
        let id = i;
        let insertMarkup = '<input id="' + id + '">';
        $(this).append(insertMarkup);
        $('#' + id)
          .attr('value', 'Insert')
          .attr('type', 'submit')
          .attr('inline-ad-index', i)
          .addClass('form-submit advert-inline-insert-submit');
        // Prevent enter key submission on input.
        var $selection = $(this).find('input').first();
        $selection.keypress(function(event) {
          if (event.keyCode == 13) {
            event.preventDefault();
          }
        });

        // On click (of insert submit button), insert advert into WYSIWYG using our custom insert.js behavior.
        // (the same one used to insert image placeholders.)
        var $insertAdButton = $(this).find('.advert-inline-insert-submit');
        // Prevent enter key submission on input.
        $insertAdButton.keypress(function(event) {
          if (event.keyCode == 13) {
            event.preventDefault();
          }
        });
        $insertAdButton.click(function(e){
          e.preventDefault();
          // Make sure input has a value to insert.
          if (!$selection.val()) {
            // alert('Before inserting, you must select a valid advertisement.')
          }
          else {
            let markup =
              '<p advert-id="' + id + '" inline-advert>____ADVERTISEMENT____<span>' + $selection.val() + '</span></p>';
            Drupal.insert.insertIntoActiveEditor(markup);

          }
        });
      });


    }
  };


  Drupal.behaviors.articleAdInserts = {
    attach: function (context, settings) {
      // Replace inline placeholders with the actual banner ad nodes.
      $('.node-article .node-advert-article-divider').once('articleAdInserts', function (i) {
        var $advert = $(this);
        $('.node-article').find('p[inline-advert]').eq(i).each(function(){
          $(this).html($advert);
        });
      });
    }
  };
}(jQuery));
