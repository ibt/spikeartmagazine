<?php
/**
 * @file
 * spike_display_extra.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function spike_display_extra_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-highlight_element-field_image_highlight'.
  $field_instances['node-highlight_element-field_image_highlight'] = array(
    'bundle' => 'highlight_element',
    'deleted' => 0,
    'description' => 'Mascot or other weird extra element to place on content for highlighting purposes',
    'display' => array(
      'checkout_pane' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'default' => array(
        'label' => 'hidden',
        'module' => 'image',
        'settings' => array(
          'field_multiple_limit' => -1,
          'field_multiple_limit_offset' => 0,
          'field_multiple_limit_order' => 0,
          'image_link' => '',
          'image_style' => '',
        ),
        'type' => 'image',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_image_highlight',
    'label' => 'Image highlight',
    'required' => 1,
    'settings' => array(
      'alt_field' => 0,
      'default_image' => 0,
      'entity_translation_sync' => FALSE,
      'file_directory' => 'users/shared/images-highlight/images',
      'file_extensions' => 'png gif jpg jpeg',
      'focus' => 0,
      'focus_lock_ratio' => 0,
      'focus_min_size' => '',
      'image_field_caption' => array(
        'enabled' => 0,
        'image_field_caption_wrapper' => array(
          'image_field_caption_default' => array(
            'format' => 'plain_text',
            'value' => '',
          ),
        ),
      ),
      'image_field_caption_wrapper' => array(
        'image_field_caption_default' => array(
          'format' => 'filtered_html',
          'value' => '',
        ),
      ),
      'max_filesize' => '',
      'max_resolution' => '',
      'min_resolution' => '',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'filefield_sources' => array(
          'filefield_sources' => array(
            'attach' => 0,
            'clipboard' => 0,
            'imce' => 0,
            'reference' => 0,
            'remote' => 0,
            'upload' => 'upload',
          ),
          'source_attach' => array(
            'absolute' => 0,
            'attach_mode' => 'move',
            'path' => 'file_attach',
          ),
          'source_imce' => array(
            'imce_mode' => 0,
          ),
          'source_reference' => array(
            'autocomplete' => 0,
          ),
        ),
        'imce_filefield_on' => 0,
        'insert' => 0,
        'insert_absolute' => 0,
        'insert_class' => '',
        'insert_default' => 'auto',
        'insert_styles' => array(
          'auto' => 'auto',
          'colorbox__full_width' => 0,
          'colorbox__large' => 0,
          'colorbox__medium' => 0,
          'colorbox__menu_icon' => 0,
          'colorbox__spike_large' => 0,
          'colorbox__spike_medium' => 0,
          'colorbox__square_thumbnail' => 0,
          'colorbox__thumbnail' => 0,
          'icon_link' => 0,
          'image' => 0,
          'image_full_width' => 0,
          'image_large' => 0,
          'image_medium' => 0,
          'image_menu_icon' => 0,
          'image_spike_large' => 0,
          'image_spike_medium' => 0,
          'image_square_thumbnail' => 0,
          'image_thumbnail' => 0,
          'link' => 0,
        ),
        'insert_width' => '',
        'preview_image_style' => 'thumbnail',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => 1,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Image highlight');
  t('Mascot or other weird extra element to place on content for highlighting purposes');

  return $field_instances;
}
