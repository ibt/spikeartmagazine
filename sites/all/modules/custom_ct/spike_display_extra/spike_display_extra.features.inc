<?php
/**
 * @file
 * spike_display_extra.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function spike_display_extra_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function spike_display_extra_node_info() {
  $items = array(
    'highlight_element' => array(
      'name' => t('Highlight element'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
