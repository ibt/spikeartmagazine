<?php
/**
 * @file
 * spike_editions.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function spike_editions_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_image_info|node|edition|full';
  $field_group->group_name = 'group_image_info';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'edition';
  $field_group->mode = 'full';
  $field_group->parent_name = 'group_upper';
  $field_group->data = array(
    'label' => 'Image Info',
    'weight' => '4',
    'children' => array(
      0 => 'image_caption',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Image Info',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-image-info field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $field_groups['group_image_info|node|edition|full'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_tags|node|edition|full';
  $field_group->group_name = 'group_tags';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'edition';
  $field_group->mode = 'full';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Tags',
    'weight' => '34',
    'children' => array(
      0 => 'field_tags',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Tags',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-tags field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'none',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_tags|node|edition|full'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_upper|node|edition|full';
  $field_group->group_name = 'group_upper';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'edition';
  $field_group->mode = 'full';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Upper',
    'weight' => '0',
    'children' => array(
      0 => 'image_info',
      1 => 'group_image_info',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Upper',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-upper field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'none',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_upper|node|edition|full'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Image Info');
  t('Tags');
  t('Upper');

  return $field_groups;
}
