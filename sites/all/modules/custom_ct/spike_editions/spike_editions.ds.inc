<?php
/**
 * @file
 * spike_editions.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function spike_editions_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|edition|full';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'edition';
  $ds_fieldsetting->view_mode = 'full';
  $ds_fieldsetting->settings = array(
    'image_info' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'image_caption' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|edition|full'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|edition|sidebar';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'edition';
  $ds_fieldsetting->view_mode = 'sidebar';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '0',
        'wrapper' => '',
        'class' => '',
        'linked_field' => array(
          'linked' => 0,
          'destination' => '',
          'advanced' => array(
            'title' => '',
            'target' => '',
            'class' => '',
            'rel' => '',
            'text' => '',
          ),
        ),
      ),
    ),
    'image_counter' => array(
      'weight' => '5',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'content_type' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'corr_article_language_version' => array(
      'weight' => '6',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|edition|sidebar'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|edition|teaser';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'edition';
  $ds_fieldsetting->view_mode = 'teaser';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '3',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '1',
        'wrapper' => 'h4',
        'class' => '',
      ),
    ),
    'flag_recommended' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|edition|teaser'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|edition|teaser_product_front';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'edition';
  $ds_fieldsetting->view_mode = 'teaser_product_front';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '5',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '1',
        'wrapper' => 'h4',
        'class' => '',
        'linked_field' => array(
          'linked' => 0,
          'destination' => '',
          'advanced' => array(
            'title' => '',
            'target' => '',
            'class' => '',
            'rel' => '',
            'text' => '',
          ),
        ),
      ),
    ),
    'flag_recommended' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'content_type' => array(
      'weight' => '2',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|edition|teaser_product_front'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function spike_editions_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|edition|form';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'edition';
  $ds_layout->view_mode = 'form';
  $ds_layout->layout = 'ds_2col_stacked';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'title',
        2 => 'field_title_sub',
        3 => 'field_blurb',
        4 => 'path',
        5 => 'field_tags',
        6 => 'field_display_extra_ref',
        7 => 'body',
        13 => 'field_product',
      ),
      'right' => array(
        1 => 'language',
        8 => 'field_image',
        9 => 'field_category',
      ),
      'hidden' => array(
        10 => 'field_online_bool',
        11 => 'flag',
        14 => 'metatags',
        15 => '_add_existing_field',
      ),
      'footer' => array(
        12 => 'field_shipping_rate',
      ),
    ),
    'fields' => array(
      'title' => 'left',
      'language' => 'right',
      'field_title_sub' => 'left',
      'field_blurb' => 'left',
      'path' => 'left',
      'field_tags' => 'left',
      'field_display_extra_ref' => 'left',
      'body' => 'left',
      'field_image' => 'right',
      'field_category' => 'right',
      'field_online_bool' => 'hidden',
      'flag' => 'hidden',
      'field_shipping_rate' => 'footer',
      'field_product' => 'left',
      'metatags' => 'hidden',
      '_add_existing_field' => 'hidden',
    ),
    'classes' => array(),
    'wrappers' => array(
      'header' => 'div',
      'left' => 'div',
      'right' => 'div',
      'footer' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => 0,
  );
  $export['node|edition|form'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|edition|sidebar';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'edition';
  $ds_layout->view_mode = 'sidebar';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'content_type',
        1 => 'title',
        2 => 'field_title_sub',
        3 => 'field_product',
        4 => 'image_counter',
        5 => 'corr_article_language_version',
      ),
    ),
    'fields' => array(
      'content_type' => 'ds_content',
      'title' => 'ds_content',
      'field_title_sub' => 'ds_content',
      'field_product' => 'ds_content',
      'image_counter' => 'ds_content',
      'corr_article_language_version' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|edition|sidebar'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|edition|teaser_product_front';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'edition';
  $ds_layout->view_mode = 'teaser_product_front';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'flag_recommended',
        1 => 'field_display_extra_ref',
        2 => 'content_type',
        3 => 'field_image',
        4 => 'title',
      ),
    ),
    'fields' => array(
      'flag_recommended' => 'ds_content',
      'field_display_extra_ref' => 'ds_content',
      'content_type' => 'ds_content',
      'field_image' => 'ds_content',
      'title' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|edition|teaser_product_front'] = $ds_layout;

  return $export;
}
