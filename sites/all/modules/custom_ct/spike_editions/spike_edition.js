(function ($) {

  Drupal.behaviors.spikeEditions = {

    attach: function (context, settings) {

      var $imageFlipper = $('.image-flipper');
      var $imgCounter = $('#image-counter');
      var $main = $('.main-container');
      var $thumbs = $('#article-thumbnails');
      var $thumbToggle = $('.thumb-toggle');
      //      var hex = '#'+Math.floor(Math.random()*16777215).toString(16);
      //      var lum = Math.random() * 0.75;
      //      var color = ColorLuminance(hex, lum);
      var asfd;
      //      var thumbH = $thumbs.outerHeight(true);
      //      var slideIndex = new pageLoad(0);
      //      var abadfs;



      function isInt(n) {
        return typeof n == "number" && isFinite(n) && n % 1 === 0;
      }

      function printCounter(index, total) {
        return '<span class="active">' + (index) + '</span>/<span class="total">' + total + '</span>';
      }


      function getSlideSize($imageFlipper, index) {
        var $el = $imageFlipper.find('.item').eq(index).children('img');
        return [$el.height(), $el.width()];
      }


      /*
       INIT CAPTIONS
       position captions in the sidebar, they will be updated on pagination
       , or possibly initiated by the pager, to which it would always be synced
       */
      $main.on('initCaptions', function () {
        // find captions
        var asdf;
        $main.find('.field-name-image-info .caption').each(function () {
          $(this).insertBefore('.group-lower .field-name-body');
        });
      });

      $main.on('setImageCounter', function (event, current, total) {
        var counter = printCounter(current, total);
        $imgCounter.html(counter);
        $main.trigger('setImageCaption', current);
      });


      $main.on('setImageCaption', function (event, index) {
        index--;
        var $container = $('#article-captions-container')
        var $display = $('#article-caption-display');
        var $caption = $container.children().eq(index).clone();
        var asdf;
        $display.children().fadeOut().remove();
        $display.append($caption);
        $caption.fadeIn();
      });

      $thumbs.on('thumbPosInit', function () {
        $(this).css({
          top: $(window).height()
        });
      });



      $thumbs.on('checkThumbPos', function () {
        var thumbP = $thumbs.position().top;
        var scrollTop = $(window).scrollTop();
        var dir;
        if (scrollTop < thumbP / 1.5) {
          //          window.scrollTo(0,thumbP + 60);
          $('html,body').animate({
            scrollTop: thumbP
          }, 500);
          // do smooth scroll

          dir = 1;
        } else {
          //          window.scrollTo(0,0);
          $('html,body').animate({
            scrollTop: 0
          }, 500);

          dir = 0;
        }
        var asdf;
        //        $thumbs.trigger('setThumbTrigger', [dir]);
      })

      $thumbs.on('setThumbTrigger', function (event, dir) {
        if (dir == 0) {
          $('.thumb-toggle-el-top').show();
          $('.thumb-toggle-el-bot').hide();
        } else {
          $('.thumb-toggle-el-top').hide();
          $('.thumb-toggle-el-bot').show();
        }
        //        $thumbToggle.html(dir === 0 ? 'Overview' : 'Back to top');
      });

      /* action on thumbnail click - page to that image */
      $main.on('thumbClick', function (event, i) {
        var $flipper = $main.find('.image-flipper');
        i++;
        $flipper.turn('page', i);
      });





      $imageFlipper.on('flipperResize', function (event, page) {
        $turn = $(this);
        let index = page - 1;
        let margin = parseInt($turn.parents('.field').css('margin-left').replace('px', ''));
        // console.log('margin = ' + margin);
        let height = 0;
        // let $item = $turn.find('img').parent();
        // let $img = $item.find('img');
        let string = '.item.turn-page.p' + page;
        let $item = $(string);
        let $img = $item.find('img');
        let imgH = $img.attr('height');
        let imgW = $img.attr('width');
        let flpW = $(window).width() - (margin * 2);
        let flpH = ((imgH / imgW) * flpW);
        $imageFlipper.turn('size', flpW, flpH);
        $('.group-lower .caption').hide();
        let $caption = $('.group-lower .caption')
        $caption.eq(index).show();
      });


      function getColor() {
        return '#' + Math.floor(Math.random() * 16777215).toString(16)
      }


      $imageFlipper.on('initFlipBg', function () {
        var color = getColor();
        var asfd;
        $(this).find('.turn-page').each(function () {
          $(this).css({
            'background': 'white'
            //            'background': color
          });
          var asdf;
        })
      });

      $imageFlipper.on('titlePos', function () {

      });

      function titlePos(h) {
        //        return ($(window).height() / 2)
        return ($(window).height() / 2) - (h / 2)
      }

      $imageFlipper.on('initTitle', function () {
        var $title = $(this).parents('.field').siblings('.group-image-info');
        $title.find('h1').clone().addClass('title-clone').insertBefore($title);
        $('.title-clone').wrap('<div class="title-clone-wrapper">');

        var $tClone = $('.title-clone-wrapper');
        var h = $tClone.find('h1').outerHeight();
        var asdf;
        //        console.log(h);
        $tClone.css({
          opacity: 0,
          position: 'absolute',
          top: titlePos(h)
        });
        $tClone.animate({
          opacity: 1
        }, 300).delay(4000).fadeOut('slow');
        //        });

        $imageFlipper.click(function () {
          //          console.log('click');
          $tClone.clearQueue().fadeOut('fast');
        })

      });

      /*
       IMAGE ARTICLE initFlipper
       make an image display with turn.js library
       */
      $imageFlipper.on('initFlipper', function () {

        // center our element based on with of side columns
        var margin = parseInt($(this).parents('.field').css('margin-left').replace('px', ''));
        var flpW = $(window).width() - (margin * 2);
        $imageFlipper.css({
          margin: 0,
          width: flpW,
          // height: flpH
        });

        var imgDim = getSlideSize($imageFlipper, 0);
        var ratio = imgDim[0] / imgDim[1];
        var elH = ratio * flpW;

        // init turn js object
        var $mag = $(this);
        $mag.turn({
          display: 'single',
          //          display: 'double',
          duration: 1200,
          width: flpW,
          // height: flpH,
          autoCenter: true,
          acceleration: true,
          // acceleration: $.isTouch,
          gradients: true,
          // gradients: !$.isTouch,
          turnCorners: 'bl,tr',
          elevation: 350,
          easeFunction: 'ease-in',
          //            max: 3,
          when: {
            start: function (event, page, view) {
              $main.trigger('setImageCounter', [$mag.turn('page'), $mag.turn('pages')]);
              $mag.trigger('flipperResize', [$mag.turn('page')]);
            },
            turned: function (event, page, pageObject) {
              $mag.trigger('initFlipBg');
            }
          }
        });

        /*
         click control - simplify clicks on the image
         */
        $mag.click(function (e) {
          var offset = $(this).offset();
          var relativeX = (e.pageX - offset.left);
          var relativeY = (e.pageY - offset.top);
          if (relativeX < flpW / 2) { $mag.turn('previous') }
          else { $mag.turn('next') };
        });

        //        $(window).resize(function() {
        //          var flpW = $(window).width() - (margin * 2);
        //          var flpH = $(window).height() - 100;
        //          $mag.trigger('flipperResize', [flpH, flpW, $mag.turn('page')]);
        //          var size = $(window).width() - (margin * 2);
        //
        //          $mag.turn('size', size, size);
        //        });


        // additional init functions

        // set pager values
        var cur = $mag.turn('page');
        var total = $mag.turn('pages')
        var asdf;
        $main.trigger('setImageCounter', [$mag.turn('page'), $mag.turn('pages')]);


      });


      /*
       * .once('main-container')
       */
      $('.main-container', context).once(function () {
        $main.hide();

        /*
         move page title into body - this is a BUG fix - dunno why field wouldn't work
         */
        $('.page-header').insertAfter('.group-upper .field-name-image-caption');


        /*
         init captions
         */



        if ($(window).outerWidth() >= 768) {
          $('.main-container').trigger('initCaptions');
          $imageFlipper.trigger('initFlipper');
          $imageFlipper.trigger('initFlipBg');
          $imageFlipper.trigger('flipperResize', [1]);
        }
        $(window).on('load', function () {
          $imageFlipper.trigger('initTitle');

          // give all images proper vertical centering
        });


        /*
         @TODO | use thumbPosInit to always
         */
        //        $thumbs.css({ position: 'absolute' }).trigger('thumbPosInit');


        /*
         turn.js keyboard controls
         */
        $(window).bind('keydown', function (e) {
          if (e.keyCode == 37)
            $imageFlipper.turn('previous');
          else if (e.keyCode == 39)
            $imageFlipper.turn('next');
          $('.title-clone-wrapper').clearQueue().fadeOut('fast');
        });


        function pagerVisibility() {
          if ($imageFlipper.turn('page') == 1) {
            $('#prev').fadeOut();
            $('#next').fadeIn();
          } else if ($imageFlipper.turn('page') == $imageFlipper.turn('pages')) {
            $('#next').fadeOut();
            $('#prev').fadeIn();
          } else {
            $('#next').fadeIn();
            $('#prev').fadeIn();
          }
        }

        $('#next').click('keydown', function (e) {
          if (e.keyCode == 37)
            $imageFlipper.turn('previous');
          else if (e.keyCode == 39)
            $imageFlipper.turn('next');
          $('.title-clone-wrapper').clearQueue().fadeOut('fast');
        });


        $(window).resize(function () {
          //          $imageFlipper.trigger('initFlipper');

          var margin = parseInt($imageFlipper.parents('.field').css('margin-left').replace('px', ''));
          var flpW = $(window).width() - (margin * 2);
          var flpH = $(window).height() - 120;
          var size = $(window).width() - (margin * 2);

          // $imageFlipper.turn('size', flpW, flpH);
          // $('.title-clone-wrapper').css({
          //   top: titlePos($('.title-clone-wrapper').height())
          // });
          if ($(window).outerWidth() > 500) {
            $imageFlipper.trigger('flipperResize', [$imageFlipper.turn('page')]);
          }
        });





        /* position the thumbnail display just below viewport
         * this position is toggled via $('#thumb-toggle') in right sidebar
         * */
        //        $thumbs.css({ top: $(window).outerHeight(true) });
        //        $thumbs.css({ background: color });


        //        $main.trigger('setImageCounter',[0]);
        //        $main.trigger('setImageCaption', [0]);
        $thumbs.trigger('checkThumbPos');

        //        $thumbs.trigger('setThumbTrigger', [0]);



        /* trigger to toggle position of thumbnail display
         * the trigger is in the right sidebar
         * */
        $('.thumb-toggle a').click(function () {
          // suppress link click
          $thumbs.trigger('checkThumbPos');
          return false;
        });

        /* click trigger on thumbnail to jump back to main img */
        $thumbs.find('.field-name-field-image img').click(function () {
          var i = $(this).parent().index();
          var asfd;
          $main.trigger('thumbClick', [i]);

          // change slide
          //          $('a[data-slidesjs-item="' + i + '"]').trigger('click');
          $thumbs.trigger('checkThumbPos');

        });




        $main.show();


        var asdf;

      });
      /* end .once('main-container') */


    }

  };
})(jQuery);