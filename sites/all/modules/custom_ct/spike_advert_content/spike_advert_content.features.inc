<?php
/**
 * @file
 * spike_advert_content.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function spike_advert_content_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function spike_advert_content_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function spike_advert_content_node_info() {
  $items = array(
    'advert_article_divider' => array(
      'name' => t('Advert article divider'),
      'base' => 'node_content',
      'description' => t('\'Article divider\' advertisements are placed manually by the editor in individual article texts. They can be given an expiration date, after which they disappear from all texts in to which they had been inserted.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'advert_content' => array(
      'name' => t('Advert content'),
      'base' => 'node_content',
      'description' => t('Fancy display adverts'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'advert_sidebar' => array(
      'name' => t('Advert Sidebar'),
      'base' => 'node_content',
      'description' => t('Fancy display adverts'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
