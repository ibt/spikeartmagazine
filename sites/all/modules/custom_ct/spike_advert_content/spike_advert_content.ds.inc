<?php
/**
 * @file
 * spike_advert_content.ds.inc
 */

/**
 * Implements hook_ds_layout_settings_info().
 */
function spike_advert_content_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|advert_content|teaser';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'advert_content';
  $ds_layout->view_mode = 'teaser';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_display_extra_ref',
        1 => 'field_image_highlight',
      ),
    ),
    'fields' => array(
      'field_display_extra_ref' => 'ds_content',
      'field_image_highlight' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|advert_content|teaser'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|advert_sidebar|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'advert_sidebar';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_image_highlight',
      ),
    ),
    'fields' => array(
      'field_image_highlight' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|advert_sidebar|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|advert_sidebar|teaser';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'advert_sidebar';
  $ds_layout->view_mode = 'teaser';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_image_highlight',
      ),
    ),
    'fields' => array(
      'field_image_highlight' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|advert_sidebar|teaser'] = $ds_layout;

  return $export;
}
